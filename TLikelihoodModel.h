/*
 * TLikelihoodModel.h
 *
 *  Created on: Mar 1, 2018
 *      Author: phaentu
 */

#ifndef TLIKELIHOODMODEL_H_
#define TLIKELIHOODMODEL_H_

#include "TGenoErrorStorage.h"
#include "TPosteriorMean.h"
#include "TRandomGenerator.h"
#include "TLog.h"

#include "TFastLog.h"

//////////////////////////////////////
// Likelihood models
//////////////////////////////////////
class TLikelihoodModel{
protected:
	int numGroups;
	int numSnps;
	int numSamples;
	bool weightsInitialized;

public:
	double*** weights;
	int testNum;

	TLikelihoodModel();
	virtual ~TLikelihoodModel(){};

	virtual void estimateInitialParameters(TGenoErrorStorage & Data){throw "void estimateGenotypeFrequencies(TGenoErrorStorage & Data) not implemented for base class TLikelihoodModel.";};
	virtual double estimateInitialErrorRates(TGenoErrorStorage & Data){throw "void estimateInitialErrorRates(TGenoErrorStorage & Data) not implemented for base class TLikelihoodModel.";};
	virtual void adjustAfterBurnin(){throw "void adjustAfterBurnin() not implemented for base class TLikelihoodModel.";};
	virtual double calculateLL(TGenoErrorStorage & Data, double**** emissionProbs){throw "void calculateLL(TGenoErrorStorage & Data, double**** emissionProbs) not implemented for base class TLikelihoodModel.";};
	virtual double calculatePosteriorProb(TGenoErrorStorage & Data, double**** emissionProbs, TRandomGenerator & randomGenerator){throw "double calculatePosteriorProb(TGenoErrorStorage & Data, double**** emissionProbs) not implemented for base class TLikelihoodModel.";};
	virtual void writePosteriorSurfaceHeader(std::ofstream & out){throw "void writePosteriorSurfaceHeader(std::ofstream & out) not implemented for base class TLikelihoodModel.";};
	virtual void writePosteriorSurface(std::ofstream & out, TGenoErrorStorage & Data, double**** emissionProbs, double postProbError, TRandomGenerator & randomGenerator){throw "void writePosteriorSurface(std::ofstream & out, TGenoErrorStorage & Data, double**** emissionProbs, TRandomGenerator & randomGenerator) not implemented for base class TLikelihoodModel.";};
	virtual void calculateLogHastingsTermErrorUpdate(double** logHastingsTerm, TGenoErrorStorage & Data, double**** emissionProbs){throw "void calculateLogHastingsTermErrorUpdate(double* logHastingsTerm, TGenoErrorStorage & Data, double**** emissionProbs) not implemented for base class TLikelihoodModel.";};
	virtual void calculateWeights(TGenoErrorStorage & Data, double**** emissionProbs){throw "void calculateWeights(TGenoErrorStorage & Data, double**** emissionProbs) not implemented for base class TLikelihoodModel.";};
	virtual void estimateFrequencies(TGenoErrorStorage & Data){throw "void estimateFreq() not implemented for base class TLikelihoodModel.";};
	virtual void updateModelSpecificParametersMCMC(TGenoErrorStorage & Data, double**** emissionProbs, TRandomGenerator & randomGenerator){throw "void updateModelSpecificParametersMCMC(TGenoErrorStorage & Data, double**** emissionProbs, const double & proposalWidth, TRandomGenerator & randomGenerator) not implemented for base class TLikelihoodModel.";};
	virtual void writeAcceptanceRateModelSpecificParameters(TLog* logfile){throw "std::string getAcceptanceRateModelSpecificParameters() not implemented for base class TLikelihoodModel.";};
	virtual void addCurEstimatesToPosteriorMeanStorage(){throw "void addCurEstimatesToPosteriorMeanStorage() not implemented for base class TLikelihoodModel.";};
	virtual void setParametersToPosteriorMean(){throw "void setParametersToPosteriorMean() not implemented for base class TLikelihoodModel.";};
	virtual void fillQ(double** Q, TGenoErrorStorage & Data, double**** emissionProbs){throw "void fillQ(double** Q, TGenoErrorStorage & Data, double**** emissionProbs) not implemented for base class TLikelihoodModel.";};
	virtual void fillQHomoOnly(double** Q, TGenoErrorStorage & Data, double**** emissionProbs){throw "void fillQHomoOnly(double** Q, TGenoErrorStorage & Data, double**** emissionProbs) not implemented for base class TLikelihoodModel.";};
	virtual void fillQHetOnly(double** Q, TGenoErrorStorage & Data, double**** emissionProbs){throw "void fillQHomoOnly(double** Q, TGenoErrorStorage & Data, double**** emissionProbs) not implemented for base class TLikelihoodModel.";};
	virtual void writeModelSpecificParameterEstimates(std::string outname, TGenoErrorStorage & Data, TLog* logfile){throw "void writeModelSpecificParameterEstimates(std::string outname, TGenoErrorStorage & Data, TLog* logfile) not implemented for base class TLikelihoodModel.";};
	virtual void writeHeaderHierarchicalParameters(gz::ogzstream & out){throw "void writeHeaderHierarchicalParameters(std::ofstream & out) not implemented for base class TLikelihoodModel.";};
	virtual void writeCurHierarchicalParameters(gz::ogzstream & out){throw "void writeCurHierarchicalParameters(std::ofstream & out) not implemented for base class TLikelihoodModel.";};
};

class TIndividualReplicatesModel:public TLikelihoodModel{
private:
	double** genotypeFrequencies;
	double** newGenotypeFrequencies;

protected:

	bool frequenciesInitialized;

	void initializeFrequencies();
	void freeFrequencies();
	void initializeWeights(TGenoErrorStorage & Data);
	void freeWeights();

public:

	TIndividualReplicatesModel(TGenoErrorStorage & Data);
	virtual ~TIndividualReplicatesModel(){
		freeWeights();
		freeFrequencies();
	};
	void estimateInitialParameters(TGenoErrorStorage & Data);
	double estimateInitialErrorRates(TGenoErrorStorage & Data);
	double calculateLL(TGenoErrorStorage & Data, double**** emissionProbs);
	void calculateWeights(TGenoErrorStorage & Data, double**** emissionProbs);
	void estimateFrequencies(TGenoErrorStorage & Data);
	void fillQ(double** Q, TGenoErrorStorage & Data, double**** emissionProbs);
	void fillQHomoOnly(double** Q, TGenoErrorStorage & Data, double**** emissionProbs);
	void fillQHetOnly(double** Q, TGenoErrorStorage & Data, double**** emissionProbs);
	void writeModelSpecificParameterEstimates(std::string outname, TGenoErrorStorage & Data, TLog* logfile);
};

class THardyWeinbergModel:public TLikelihoodModel{
private:
	double** alleleFreq;
	double** newAlleleFreq;

	//MCMC storage
	double* alpha;
	double* beta;
	double* logAlpha;
	double* logBeta;
	double** proposalWidthFrequencies;
	double proposalWidthAlphaBeta;
	TPosteriorMean2D alleleFreqPosteriorMean;
	TPosteriorMean logAlphaPosteriorMean;
	TPosteriorMean logBetaPosteriorMean;
	long numAlphaBetaAccepted;
	long** numFreqAccepted;
	long numUpdates;

	//prior on alpha / beta
	double sigma2AlphaBetaPrior;
	double muAlphaBetaPrior;
	double OneOverTwoSigma2;

	//lookup table to calc log quickly
	TFastLog fastlog;

	void fillGenotypeFrequenciesOneLocus(long & locus, double** theseAlleleFrequencies);
	void fillGenotypeFrequencies(double** theseAlleleFrequencies);
	void updateAlphaBeta(double & _this, double & _thisLog, double & _other, double & sumLogFreq, TRandomGenerator & randomGenerator);

protected:
	double*** genotypeFrequencies;

	bool frequenciesInitialized;

	virtual void initializeFrequencies();
	virtual void freeFrequencies();
	void initializeWeights(TGenoErrorStorage & Data);
	void freeWeights();



public:
	THardyWeinbergModel();
	THardyWeinbergModel(TGenoErrorStorage & Data);
	virtual ~THardyWeinbergModel(){
		freeWeights();
		freeFrequencies();
	};
	virtual void estimateInitialParameters(TGenoErrorStorage & Data);
	void readFrequenciesfromFile(std::string & filename, TLog* logfile);
	double estimateInitialErrorRates(TGenoErrorStorage & Data);
	void adjustAfterBurnin();
	double calculateLL(TGenoErrorStorage & Data, double**** emissionProbs);
	double calculatePosteriorProb(TGenoErrorStorage & Data, double**** emissionProbs, TRandomGenerator & randomGenerator);
	void writePosteriorSurfaceHeader(std::ofstream & out);
	void writePosteriorSurface(std::ofstream & out, TGenoErrorStorage & Data, double**** emissionProbs, double postProbError, TRandomGenerator & randomGenerator);
	void calculateLogHastingsTermErrorUpdate(double** logHastingsTerm, TGenoErrorStorage & Data, double**** emissionProbs);
	void calculateWeights(TGenoErrorStorage & Data, double**** emissionProbs);
	virtual void estimateFrequencies(TGenoErrorStorage & Data);
	void updateModelSpecificParametersMCMC(TGenoErrorStorage & Data, double**** emissionProbs, TRandomGenerator & randomGenerator);
	void writeAcceptanceRateModelSpecificParameters(TLog* logfile);
	void addCurEstimatesToPosteriorMeanStorage();
	void setParametersToPosteriorMean();
	void fillQ(double** Q, TGenoErrorStorage & Data, double**** emissionProbs);
	void fillQHomoOnly(double** Q, TGenoErrorStorage & Data, double**** emissionProbs);
	void fillQHetOnly(double** Q, TGenoErrorStorage & Data, double**** emissionProbs);
	void writeModelSpecificParameterEstimates(std::string outname, TGenoErrorStorage & Data, TLog* logfile);
	void writeHeaderHierarchicalParameters(gz::ogzstream & out);
	void writeCurHierarchicalParameters(gz::ogzstream & out);
};

/*
class TPopulationReplicateModel:public THardyWeinbergModel{
private:
	double*** newFreq;

	void initializeFrequencies();
	void freeFrequencies();

public:
	TPopulationReplicateModel(TGenoErrorStorage & Data);
	virtual ~TPopulationReplicateModel(){
		freeWeights();
		freeFrequencies();
	};
	void estimateInitialFrequencies(TGenoErrorStorage & Data);
	void estimateFrequencies(TGenoErrorStorage & Data);
	void writeEstimatedFrequencies(std::string outname, TGenoErrorStorage & Data, TLog* logfile);
};
*/


#endif /* TLIKELIHOODMODEL_H_ */
