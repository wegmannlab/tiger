#include "commonutilities/TMain.h"
#include "allTasks.h"

//---------------------------------------------------------------------------
// Function to add existing tasks
// Use main.addRegularTask to add a regular task (shown in list of available tasks)
// Use main.adddebugTask to add a debug task (not shown in list of available tasks)
//
//---------------------------------------------------------------------------
void addTaks(TMain & main){
	main.addRegularTask("adjustPL", new TTask_adjustPL());
	main.addRegularTask("estimateTruthSet", new TTask_truthSet());
	main.addRegularTask("estimateIndReps", new TTask_sampleReps());
	main.addRegularTask("estimateHardyWeinberg", new TTask_hardyWeinberg());
	main.addRegularTask("simulate", new TTask_simulate());
	main.addRegularTask("truthSetErrors", new TTask_truthSetErrors());

	//and debug tasks
	main.addDebugTask("surface", new TTask_surface());
};

//---------------------------------------------------------------------------
//Main function
//---------------------------------------------------------------------------
int main(int argc, char* argv[]){
	TMain main("Tiger", "1.0", "https://bitbucket.org/wegmannlab/tiger");

	//add existing tasks
	addTaks(main);

	//now run program
	return main.run(argc, argv);
};

