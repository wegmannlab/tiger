/*
 * TGenoErrorStorage.h
 *
 *  Created on: Feb 26, 2018
 *      Author: wegmannd
 */

#ifndef TGENOERRORSTORAGE_H_
#define TGENOERRORSTORAGE_H_


#include <vector>
#include "TParameters.h"
#include "TLog.h"
#include "TSampleGroup.h"

class TGenoErrorOneErrorModel;
class TGenoErrorTwoErrorModel;

class TGenoErrorStorage{
protected:
	std::string vcfFilename;
	bool vcfRead;
	unsigned int* allSamples;
	std::vector<std::string> sampleNames;
	std::vector<std::string> errorClassNames;
	TSampleGroups groups;


	//data on snps
	std::vector<std::string> chromosomeNames;
	std::vector<int> snpChr;
	std::vector<long> snpPosition;

	void openVCF(TVcfFileSingleLine & vcfFile, TLog* logfile);
	void readSampleGroups(TParameters* myParameters, TLog* logfile, TVcfFileSingleLine & vcfFile);
	void readErrorClassOfSamples(TParameters* myParameters, TLog* logfile, TVcfFileSingleLine & vcfFile);
	void fillDepthIndexes();

public:
	//dimension
	int numSamples;
	int* numSamplesPerGroup;
	long numSnps;
	int numGroups;
	int numErrorClasses;
	int maxDepthPlusOne;
	int* numDepthsWithData;

	//data
	std::vector<short*> genotypes; //missing genotype = 3
	std::vector<int*> depths;

	int* groupOfSample;
	int* batchOfSample;
	int** depthsWithData;
	int** depthIndex;
	long** numDataPoints;

	TGenoErrorStorage();
	TGenoErrorStorage(TParameters* myParameters, TLog* logfile);
	void readVCF(TParameters* myParameters, TLog* logfile);

	~TGenoErrorStorage(){
		if(vcfRead){
			for(int c=0; c<numErrorClasses; ++c){
				delete[] depthsWithData[c];
				delete[] depthIndex[c];
				delete[] numDataPoints[c];
			}
			delete[] depthsWithData;
			delete[] depthIndex;
			delete[] numDataPoints;
			delete[] numDepthsWithData;

			for(std::vector<int*>::iterator itD = depths.begin(); itD != depths.end(); ++itD)
				delete[] *itD;
			for(std::vector<short*>::iterator itG = genotypes.begin(); itG != genotypes.end(); ++itG)
				delete[] *itG;

			delete[] batchOfSample;
			delete[] allSamples;
		}
	};

	int getNumGroups(){ return groups.numGroups; };
	int getNumErrorClasses(){ return numErrorClasses; };
	std::string getGroupString(int groupIndex){ return groups.getGroupString(groupIndex); };
	std::string getGroupName(int groupIndex){ return groups.getName(groupIndex); };
	std::string getErrorClassName(int index){ return errorClassNames[index]; };
	int getNumSnps(){ return numSnps; };
	std::string getChrPosString(long snp);
	std::string getVcfFilename(){ return vcfFilename; };
};

#endif /* TGENOERRORSTORAGE_H_ */
