/*
 * TAtlasTest.cpp
 *
 *  Created on: Dec 11, 2017
 *      Author: phaentu
 */


#include "TTest.h"


//------------------------------------------
//TTest
//------------------------------------------
TTest::TTest(TParameters & params, TLog* Logfile){
	logfile = Logfile;
	_name = "empty";
	_testingPrefix = params.getParameterStringWithDefault("prefix", "ATLAS_testing_");
	logfile->list("Will use prefix '" + _testingPrefix + "' for all testing files.");
};

bool TTest::runFromInputfile(std::string task){
	logfile->startIndent("Running task '" + task + "':");

	_testParams.addParameter("task", task);
	_testParams.addParameter("verbose", "");

	bool returnVal = true;
	/*

	//open task switcher and run task
	TaskSwitcher taskSwitcher(&_testParams, logfile);

	try{
		taskSwitcher.runTask();
	}
	catch (std::string & error){
		logfile->conclude(error);
		returnVal = false;
	}
	catch (const char* error){
		logfile->conclude(error);
		returnVal = false;
	}
	catch(std::exception & error){
		logfile->conclude(error.what());
		returnVal = false;
	}
	catch (...){
		logfile->conclude("unhandeled error!");
		returnVal = false;
	}
	logfile->endIndent();
	*/
	return returnVal;
};






