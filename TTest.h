/*
 * TAtlasTest.h
 *
 *  Created on: Dec 11, 2017
 *      Author: phaentu
 */

#ifndef TTEST_H_
#define TTEST_H_

#include <vector>
#include <map>
#include "TParameters.h"

//------------------------------------------
//TTest
//------------------------------------------
//Base class for individual tests.
//Tests can be combined to suites in TATlasTesting

class TTest{
protected:
	TLog* logfile;
	TParameters _testParams;
	std::string _testingPrefix;
	std::string _name;

	bool runFromInputfile(std::string task);

public:
	TTest(TParameters & params, TLog* Logfile);
	virtual ~TTest(){};

	std::string name(){return _name;};

	virtual bool run(){
		return true;
	};
};



#endif /* TTEST_H_ */
