/*
 * TGenoError.cpp
 *
 *  Created on: May 11, 2017
 *      Author: wegmannd
 */

#include "TGenoErrorEstimator.h"


////////////////////////////////////////////////
// TGenoErrorEstimator
////////////////////////////////////////////////

TGenoErrorEstimator::TGenoErrorEstimator(TRandomGenerator* RandomGenerator, TLog* Logfile){
	logfile = Logfile;
	randomGenerator = RandomGenerator;
	likelihoodModel = NULL;
	likelihoodModelInitialized = false;
}

void TGenoErrorEstimator::createLikelihoodModel(std::string model){
	if(model =="indReps"){
		logfile->list("Creating likelihood model for individual replicates.");
		likelihoodModel = new TIndividualReplicatesModel(data);
		defaultInferenceAlgo = "MLE";
	} else if(model == "hardyWeinberg"){
		logfile->list("Creating likelihood model under Hardy-Weinberg.");
		likelihoodModel = new THardyWeinbergModel(data);
		defaultInferenceAlgo = "Bayes";
	/*
	} else if(model == "popRep"){
		logfile->list("Creating likelihood model for population replicates.");
		likelihoodModel = new TPopulationReplicateModel(data);
		defaultInferenceAlgo = "EM";
	*/
	} else
		throw "Model '" + model + "' does not exist! Use either 'indReps', 'hardyWeinberg' or 'truthSet'.";

	likelihoodModelInitialized = true;
};

void TGenoErrorEstimator::inferGenoErrorMLE(TParameters* myParameters){
	//Reading basic parameters
	TGenoErrorEmParams EM_params(myParameters, logfile);

	//run estimation for one error model
	logfile->startIndent("Fitting model with a single error rate:");
	model1.estimateErrorRatesMLE(data, likelihoodModel, EM_params, logfile);
	logfile->endIndent();

	//run estimation for two error model
	logfile->startIndent("Fitting model with two error rates:");
	model2.estimateErrorRatesMLE(data, likelihoodModel, EM_params, logfile);
	logfile->endIndent();
}

void TGenoErrorEstimator::inferGenoErrorBayes(TParameters* myParameters){
	//Reading basic parameters
	TGenoErrorMcmcParams MCMC_params(myParameters, logfile);

	//run estimation for one error model
	logfile->startIndent("Fitting model with a single error rate:");
	model1.estimateErrorRatesBayesian(data, likelihoodModel, MCMC_params, outname, randomGenerator, logfile);
	logfile->endIndent();

	//run estimation for two error model
	logfile->startIndent("Fitting model with two error rates:");
	model2.estimateErrorRatesBayesian(data, likelihoodModel, MCMC_params, outname, randomGenerator, logfile);
	logfile->endIndent();
}

void TGenoErrorEstimator::inferGenoError(TParameters* myParameters, std::string model){
	//read data
	data.readVCF(myParameters, logfile);

	//create likelihood model
	createLikelihoodModel(model);

	//read outname, if given
	outname = myParameters->getParameterStringWithDefault("outname", "");
	if(outname == ""){
		outname = data.getVcfFilename();
		outname = extractBefore(outname, ".vcf");
	}
	logfile->list("Will write output with prefix '" + outname + "'.");
	logfile->endIndent();

	//which inference algorithm is used?
	std::string inferecneAlgo = myParameters->getParameterStringWithDefault("algo", defaultInferenceAlgo);
	if(inferecneAlgo == "MLE"){
		logfile->startIndent("Inferring MLE of error rates using an EM algorithm:");
		inferGenoErrorMLE(myParameters);
	} else if(inferecneAlgo == "Bayes"){
		logfile->startIndent("Inferring posterior means of error rates using an MCMC algorithm:");
		inferGenoErrorBayes(myParameters);
	} else
		throw "Unknown inference algorithm '" + inferecneAlgo +"'. Use either 'MLE' or 'Bayes'.";


	//report estimates
	logfile->startIndent("Writing estimates:");
	writeEstimates();
	likelihoodModel->writeModelSpecificParameterEstimates(outname, data, logfile);
	logfile->endIndent();
}

void TGenoErrorEstimator::calculatePosteriorSurface(TParameters* myParameters){
	//read data
	data.readVCF(myParameters, logfile);

	//create likelihood model
	createLikelihoodModel("hardyWeinberg");

	//read outname, if given
	//TODO: put in function
	outname = myParameters->getParameterStringWithDefault("outname", "");
	if(outname == ""){
		outname = data.getVcfFilename();
		outname = extractBefore(outname, ".vcf");
	}
	logfile->list("Will write output with prefix '" + outname + "'.");

	//name of frequency file
	std::string freqFile = myParameters->getParameterString("freqFile");

	model1.posteriorSurface(data, freqFile, outname, randomGenerator, logfile);
};

void TGenoErrorEstimator::writeEstimates(){
	std::string filename = outname + "_errorRates.txt";
	logfile->listFlush("Writing estimated error rates to '" + filename + "' ...");
	std::ofstream out(filename.c_str());
	if(!out)
		throw "Failed to open file '" + filename + "' for writing!";
	out << "Batch\tminDepth\tmaxDepth\tSize\tErrorRate\tErrorRateHom\tErrorRateHet\n";

	//print estimates, but only those with size > 0
	for(int c=0; c<data.numErrorClasses; ++c){
		for(int d=1; d<data.maxDepthPlusOne; ++d){
			if(data.depthIndex[c][d] > 0){
				out << data.getErrorClassName(c) << "\t" << d << "\t" << d << "\t";
				out << data.numDataPoints[c][data.depthIndex[c][d]]
				    << "\t" << model1.getErrorRate(c, data.depthIndex[c][d])
					<< "\t" << model2.getErrorRateHom(c, data.depthIndex[c][d])
					<< "\t" << model2.getErrorRateHet(c, data.depthIndex[c][d])
					<< "\n";
			}
		}
	}
	out.close();
	logfile->done();
}


