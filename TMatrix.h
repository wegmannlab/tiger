/*
 * TMatrix.h
 *
 *  Created on: Apr 19, 2014
 *      Author: wegmannd
 */

#ifndef TMATRIX_H_
#define TMATRIX_H_

#include <iostream>
#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include <algorithm>
#include "TRandomGenerator.h"
#include "stringFunctions.h"



class TBandMatrixStorage;
class TSquareMatrixStorage;

class TBaseMatrixStorage{
public:
	 int size;
	 double** data;
	 bool initialized;

	 //constructors
	 TBaseMatrixStorage();
	 virtual ~TBaseMatrixStorage(){};

	 //initialize / free
	 virtual void initialize(){throw "Not implemented for TBaseMatrixStorage!";};
	 virtual void free(){throw "Not implemented for TBaseMatrixStorage!";};

	 //access data
	 virtual double& operator() (int row, int col){throw "Not implemented for TBaseMatrixStorage!";};
	 virtual double& at(int row, int col);
	 virtual double& atByRef(int & row, int & col){throw "Not implemented for TBaseMatrixStorage!";};
	 void print();
	 void write(std::ofstream & out);
	 void printDiag();
	 void printRow(int row);
	 void printDiff(TBaseMatrixStorage & other);
	 void printRowSums();
	 int numDiffEntries(TBaseMatrixStorage & other);

	 //modify values
	 virtual void set(double val);
	 virtual void setByRef(double & val){throw "Not implemented for TBaseMatrixStorage!";};
	 virtual void addToDiagonal(double add);
	 virtual void addToDiagonalByRef(double & add){throw "Not implemented for TBaseMatrixStorage!";};

	 //fill values
	 virtual void fillFromMatrix(TSquareMatrixStorage & other){throw "Not implemented for TBaseMatrixStorage!";};
	 virtual void fillFromMatrix(TSquareMatrixStorage & other, double & scale){throw "Not implemented for TBaseMatrixStorage!";};
	 virtual void fillFromMatrix(TBandMatrixStorage & other){throw "Not implemented for TBaseMatrixStorage!";};
	 virtual void fillFromMatrix(TBandMatrixStorage & other, double & scale){throw "Not implemented for TBaseMatrixStorage!";};
	 virtual void fillUnifRandom(TRandomGenerator* Randomgenerator){throw "Not implemented for TBaseMatrixStorage!";};
	 virtual void fillFromProduct(TSquareMatrixStorage & first, TSquareMatrixStorage & second){throw "Not implemented for TBaseMatrixStorage!";};
	 void fillFromSquare(TSquareMatrixStorage & other){fillFromProduct(other, other);}
	 virtual void fillFromProduct(TBandMatrixStorage & first, TBandMatrixStorage & second){throw "Not implemented for TBaseMatrixStorage!";};;
	 void fillFromSquare(TBandMatrixStorage & other){fillFromProduct(other, other);};
	 virtual void fillAsExponential(TBandMatrixStorage & Q){throw "Not implemented for TBaseMatrixStorage!";};
};

class TSquareMatrixStorage:public TBaseMatrixStorage{
public:
	 //constructors
	 TSquareMatrixStorage();
	 TSquareMatrixStorage(int Size);
	 TSquareMatrixStorage(int Size, double Val);
	 TSquareMatrixStorage(TSquareMatrixStorage & other);
	 TSquareMatrixStorage(TSquareMatrixStorage & other, double scale);
	 TSquareMatrixStorage(TBandMatrixStorage & other);
	 ~TSquareMatrixStorage(){free();};

	 //initialize / free
	 void initialize();
	 void resize(int Size);
	 void resize(int Size, double Val);
	 void free();

	 //access data
	 double& operator() (int row, int col);
	 virtual double& atByRef(int & row, int & col);
	 double* operator[] (int row);
	 double* Row(int row){ return data[row]; };

	 //modify values
	 void setByRef(double & val);
	 void addToDiagonalByRef(double & add);

	 //fill values
	 void fillUnifRandom(TRandomGenerator* Randomgenerator);
	 void fillFromMatrix(TSquareMatrixStorage & other);
	 void fillFromMatrix(TSquareMatrixStorage & other, double & Scale);
	 void fillFromMatrix(TBandMatrixStorage & other);
	 void fillFromMatrix(TBandMatrixStorage & other, double & Scale);
	 void fillFromProduct(TSquareMatrixStorage & first, TSquareMatrixStorage & second);
	 void fillFromProduct(TBandMatrixStorage & first, TBandMatrixStorage & second);
	 void fillAsExponential(TBandMatrixStorage & Q);
	 void fillAsCumulative(TSquareMatrixStorage & other);
};

class TBandMatrixStorage: public TBaseMatrixStorage{
public:
	int bandwidth;
	int numDiag;
	double zero;

	//storing diagonals only.
	//The diagonals are numbered 0:(2*bandwidth) with the bandwidth^th diagonal being the longest one
	//Note that this differs form most text books where the diagonals d_tb are numbered -bandwidth:bandwidth
	//Conversion is easy: d_tb = d - bandwidth, d = d_tb + bandwidth
	int* diagLength;

	//constructors
	TBandMatrixStorage();
	TBandMatrixStorage(int Size, int Bandwidth);
	TBandMatrixStorage(int Size, int Bandwidth, double Val);
	TBandMatrixStorage(TBandMatrixStorage & other);
	TBandMatrixStorage(TBandMatrixStorage & other, double Scale);
	~TBandMatrixStorage(){free();}

	//initialize / free
	void initialize();
	void resize(int Size, int Bandwidth);
	void resize(int Size, int Bandwidth, double Val);
	void free();

	//access data
	double& operator() (int row, int col);
	double& atByRef(int & row, int & col);
	double& atNoCheckByRef(int & row, int & col);
	double& atDiag(int diag, int index);
	double& atDiagByRef(int & diag, int & index);

	//modify values
	void setByRef(double & val);
	void addToDiagonalByRef(double & add);

	//fill values
	void fillUnifRandom(TRandomGenerator* Randomgenerator);
	void fillFromMatrix(TBandMatrixStorage & other);
	void fillFromMatrix(TBandMatrixStorage & other, double & Scale);
	void fillFromProduct(TBandMatrixStorage & first, TBandMatrixStorage & second);
	void fillAsExponential(TBandMatrixStorage & Q);
};



#endif /* TMATRIX_H_ */
