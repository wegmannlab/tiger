/*
 * TTigerTests.h
 *
 *  Created on: Feb 16, 2018
 *      Author: phaentu
 */

#ifndef TIGERTESTS_H_
#define TIGERTESTS_H_


#include "TTest.h"
#include "stringFunctions.h"
#include <math.h>


//----------------------------------------
//TTigerTest_truthSetInference
//----------------------------------------
class TTigerTest_truthSetInference:public TTest{
protected:
	int numSamples;
	int numSites;
	double het;
	std::string error, errorHet;
	std::vector<double> errorVec;
	std::vector<double> errorHetVec;
	std::string outname;
	double errorTolerance;

	void parseParameters(TParameters & params, TLog* logfile, std::string testTag);
	void prepareSimulationParameters();

public:
	TTigerTest_truthSetInference(TParameters & params, TLog* logfile);
	TTigerTest_truthSetInference(TParameters & params, TLog* logfile, std::string testTag);
	virtual bool run();
	bool checkErrorRateEstimates(std::string filename);
};


//----------------------------------------
//TTigerTest_replicateInference
//----------------------------------------
class TTigerTest_replicateInference:public TTigerTest_truthSetInference{
protected:
	int numReplicates;

public:
	TTigerTest_replicateInference(TParameters & params, TLog* logfile);
	bool run();
};



#endif /* TIGERTESTS_H_ */
