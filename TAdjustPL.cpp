/*
 * TAdjustPL.cpp
 *
 *  Created on: Feb 5, 2018
 *      Author: phaentu
 */

#include "TAdjustPL.h"

//*********************************************************
// TPLTable
//*********************************************************
void TPLTable::initDirectory(){
	freeDirectory();

	//get max depth
	maxDepth = 0;
	for(it=PLs.begin(); it!=PLs.end(); ++it){
		if(it->depth > maxDepth)
			maxDepth = it->depth;
	}

	//init storage
	_PL_exists = new bool*[3];
	_PL_pointer = new TPLField**[3];
	for(int g=0; g<3; ++g){
		_PL_exists[g] = new bool[maxDepth+1];
		_PL_pointer[g] = new TPLField*[maxDepth+1];

		for(int d=0; d<maxDepth; ++d)
			_PL_exists[g][d] = false;
	}

	//fill storage
	for(it=PLs.begin(); it!=PLs.end(); ++it){
		_PL_exists[it->genotype][it->depth] = true;
		_PL_pointer[it->genotype][it->depth] = &(*it);
	}

	directoryInitialized = true;
};

void TPLTable::freeDirectory(){
	if(directoryInitialized){
		for(int g=0; g<3; ++g){
			delete[] _PL_exists[g];
			delete[] _PL_pointer[g];
		}
		delete[] _PL_exists;
		delete[] _PL_pointer;

		directoryInitialized = false;
	}
};

void TPLTable::addPL(int genotype, int depth, std::string PL){
	//check if it already exists
	for(it=PLs.begin(); it!=PLs.end(); ++it){
		if(it->genotype==genotype && it->depth == depth)
			break;
	}

	//add or update
	if(it==PLs.end())
		PLs.emplace_back(genotype, depth, PL);
	else
		it->PL_string = PL;
};

bool TPLTable::PL_exists(int & genotype, int & depth){
	if(!directoryInitialized)
		initDirectory();

	if(depth > maxDepth) return false;

	return _PL_exists[genotype][depth];
};

std::string TPLTable::updatePL(std::string oldPL, int genotype, int depth){
	//update if it exists
	if(PL_exists(genotype, depth))
		return _PL_pointer[genotype][depth]->PL_string;

	//else return old PL
	return oldPL;
};

std::string TPLTable::getPL(int genotype, int depth){
	if(!PL_exists(genotype, depth))
		throw "PL string for genotype " + toString(genotype) + " and dept " + toString(depth) + " is not in PL table!";

	return _PL_pointer[genotype][depth]->PL_string;
};

void TPLTable::print(){
	std::cout << "PL Table '" << name << "':" << std::endl;
	for(it=PLs.begin(); it!=PLs.end(); ++it){
		it->print();
	}
};

//*********************************************************
// TAdjustPL
//*********************************************************

TAdjustPL::TAdjustPL(TLog* Logfile){
	//store parameters
	logfile = Logfile;
	genotypeStrings[0] = "0/0"; genotypeStrings[1] = "0/1"; genotypeStrings[2] = "1/1";
}

int TAdjustPL::getGenotypeFromString(std::string & genoString){
	for(int i=0; i<3; ++i){
		if(genotypeStrings[i] == genoString)
			return i;
	}
	throw "Unknown genotype string '" + genoString + "'!";
};

void TAdjustPL::openVCF(TParameters* params){
	//open vcf file
	std::string filename = params->getParameterString("vcf");
	if(filename.find(".gz") == std::string::npos){
		logfile->list("Reading vcf from file '" + filename + "'.");
		vcfFile.openStream(filename, false);
	} else {
		logfile->list("Reading vcf from gzipped file '" + filename + "'.");
		vcfFile.openStream(filename, true);
	}

	//enable parsers
	vcfFile.enablePositionParsing();
	vcfFile.enableFormatParsing();
	vcfFile.enableSampleParsing();
	vcfFile.enableVariantParsing();

	//prepare output vcf
	std::string defaultOutName = filename;
	defaultOutName = extractBeforeLast(defaultOutName, ".vcf");
	defaultOutName += "_adjustedPL";
	std::string outname = params->getParameterStringWithDefault("outname", defaultOutName);
	if(readAfterLast(outname, '.') != "gz")
		outname += ".vcf.gz";
	logfile->list("Writing resulting vcf to file '" + outname + "'.");
	vcfFile.openOutputStream(outname, true);
	vcfFile.writeHeaderVCF_4_0();
	vcfFile.enableAutomaticWriting();
}

void TAdjustPL::preparePLTables(TParameters* params){
	//read from file?
	if(params->parameterExists("newPLs")){
		readPLTables(params->getParameterString("newPLs"));
	} else if(params->parameterExists("errorRates")){
		//generate from error rate estimates
		generatePLTablesFromErrorRates(params->getParameterString("errorRates"), params);
	} else
		throw "New PLs have to be provided either via tables (argument 'newPLs') or through error rates (argument 'errorRates')!";
}


void TAdjustPL::setIteratorToErrorClassCreateIfNotExists(std::string & name){
	//check if class exists
	for(plIt=PL_Tables.begin(); plIt!=PL_Tables.end(); ++plIt){
		if(plIt->getName() == name){
			break;
		}
	}

	//create new class, if it does not yet exist
	if(plIt == PL_Tables.end()){
		PL_Tables.emplace_back(name);
		plIt = PL_Tables.end() - 1;
	}
}

void TAdjustPL::readPLTables(std::string inputFileName){
	//read input file
	logfile->listFlush("Reading new PL's from file '" + inputFileName + "' ...");
	std::ifstream file(inputFileName.c_str());
	if(!file) throw "Failed to open file with new PL's' " + inputFileName + "'!";
	std::string paramLine;

	//parse input file
	std::vector<std::string> paramVector;
	std::vector<int> depthInterval;

	//header
	std::getline(file, paramLine);
	fillVectorFromString(paramLine, paramVector, "\t");
	if(paramVector.size() != 7) throw "Wrong number of columns in " + inputFileName + ": " + toString(paramVector.size()) + " instead of 7 columns found!";

	//now read file
	int lineNum = 0;
	int d,g;
	while(file.good() && !file.eof()){
		++lineNum;
		fillVectorFromLineWhiteSpaceSkipEmpty(file, paramVector);
		if(!paramVector.empty()){
			if(paramVector.size() != 7) throw "Wrong number of entries on line " + toString(lineNum) + " in file '" + inputFileName + "'! There should be 7!";

			//which error class?
			setIteratorToErrorClassCreateIfNotExists(paramVector[0]);

			//make entry in newPLs-map for each depth in interval
			for(d = stringToInt(paramVector[1]); d <= stringToInt(paramVector[2]); ++d){
				for(g=0; g<3; ++g){
					if(paramVector[g+4] != "-"){
						plIt->addPL(g, d, paramVector[g+4]);
					}
				}
			}
		}
	}

	logfile->done();
}

void TAdjustPL::generatePLTablesFromErrorRates(TLikelihoodTable* likTable, std::vector<std::string> paramVector, const int lineNum, const std::string inputFileName, const int model){
	if(paramVector.size() != 7)
		throw "Wrong number of entries on line " + toString(lineNum) + " in file '" + inputFileName + "'! There should be 7!";

	//which error class?
	setIteratorToErrorClassCreateIfNotExists(paramVector[0]);

	//check if we have estimates
	if((model == 1 && paramVector[4] != "-")   ||  (model == 2 && paramVector[5] != "-" && paramVector[6] != "-")){
		//turn error rates into PLs -> use TLikelihoodTable class!
		if(model==2)
			likTable->update(stringToDouble(paramVector[5]), stringToDouble(paramVector[6]));
		else
			likTable->update(stringToDouble(paramVector[4]));

		//make entry in newPLs-map for each depth in interval
		for(int d = stringToInt(paramVector[1]); d <= stringToInt(paramVector[2]); ++d){
			for(int g=0; g<3; ++g){
				plIt->addPL(g, d, likTable->getPLstring(g));
			}
		}
	}
}

void TAdjustPL::generatePLTablesFromErrorRates(std::string inputFileName, TParameters* params){
	logfile->startIndent("Generating PL tables from error rates:");

	//which model do we use?
	int model = params->getParameterIntWithDefault("errorModel", 1);
	if(model == 1)
		logfile->list("Will use a model with a single error rate.");
	else if(model == 2)
		logfile->list("Will use a model with two error rates: one for homozygous and one for heterozygous sites.");
	else
		throw "Unknown model '" + toString(model) + "'! Currently only models 1 & 2 are implemented.";

	//read file
	logfile->listFlush("Reading error rates from file '" + inputFileName + "' ...");
	std::ifstream file(inputFileName.c_str());
	if(!file) throw "Failed to open file '" + inputFileName + "'!";
	std::string paramLine;

	//parse input file
	std::vector<std::string> paramVector;
	std::vector<int> depthInterval;

	//header
	std::getline(file, paramLine);
	fillVectorFromString(paramLine, paramVector, "\t");
//	if(paramVector.size() != 7) throw "Wrong number of columns in " + inputFileName + ": " + toString(paramVector.size()) + " instead of 7 columns found!";


	//prepare object for conversion
	TLikelihoodTable* likTable;
	if(model==2)
		likTable = new TLikelihoodTableHet();
	else
		likTable = new TLikelihoodTable();

	//now read file
	int lineNum = 0;
	while(file.good() && !file.eof()){
		++lineNum;
		fillVectorFromLineWhiteSpaceSkipEmpty(file, paramVector);
		if(!paramVector.empty()){
			generatePLTablesFromErrorRates(likTable, paramVector, lineNum, inputFileName, model);
		}
	}
	logfile->done();
	logfile->endIndent();
};

void TAdjustPL::readBatchAssociationOfSamples(TParameters* myParameters){
	//read error class
	logfile->startIndent("Parsing sample sequencing batches:");
	std::string batchesString = myParameters->getParameterString("batches", false);
	if(batchesString == ""){
		//only one class for all samples
		logfile->list("Assuming a single sequencing batch 'all' for all samples.");
		batches.initializeSingleGroup(&vcfFile, logfile);
	} else if(batchesString[0] == '['){
		//parse as group string
		batches.initialize(&vcfFile, batchesString, logfile, false);
	} else {
		//parse as file
		int batchesColumn = myParameters->getParameterIntWithDefault("batchesCol", 3) - 1;
		batches.initialize(&vcfFile, batchesString, batchesColumn, logfile, false);
	}
	if(batches.numGroups < 1) throw "No groups defined!";
	logfile->endIndent();
}


void TAdjustPL::adjustPL(TParameters* params){
	logfile->startIndent("Adjusting PL field:");

	//create PL tables
	preparePLTables(params);

	//DEBUG
	//printPLTables();

	//open VCF file
	openVCF(params);

	//read error classes for each sample
	readBatchAssociationOfSamples(params);

	//parse VCF file
	logfile->startIndent("Parsing VCF file and adjusting PL fields:");
	int counter = 0;
	int genotype, depth;
	int errorClass;

	logfile->listFlush("Read 0 lines ... ");
	while(vcfFile.next()){
		++counter;
		for(int i=0; i < vcfFile.numSamples(); ++i){
			if(!vcfFile.sampleIsMissing(i)){
				if(batches.sampleVcfIndexInGroup(i)){
					genotype = vcfFile.sampleGenotype(i);
					depth = vcfFile.fieldContentAsInt("DP", i);
					errorClass = batches.getGroupOfSampleVcfIndex(i);
					if(PL_Tables[errorClass].PL_exists(genotype, depth)){
						vcfFile.updatePL(PL_Tables[errorClass].getPL(genotype, depth), i);
					}
				}
			}
		}
		if(counter % 1000 == 0)	logfile->overList("Read " +toString(counter) + " lines ... ");
	}
	logfile->done();

	logfile->conclude("Parsed a total of " + toString(counter) + " positions.");
	logfile->endIndent();

	logfile->endIndent();
}

void TAdjustPL::printPLTables(){
	for(plIt=PL_Tables.begin(); plIt!=PL_Tables.end(); ++plIt){
		plIt->print();
	}

}



