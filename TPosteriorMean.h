/*
 * TPosteriorMean.h
 *
 *  Created on: Mar 12, 2018
 *      Author: phaentu
 */

#ifndef TPOSTERIORMEAN_H_
#define TPOSTERIORMEAN_H_

#include <cstddef>

//////////////////////////////////////
// Posterior mean object
//////////////////////////////////////

class TPosteriorMean{
private:
	long numAdded;
	int dimension;
	double* posteriorMean;
	bool initialized;

	void init(int Dimension);
	void free();

public:
	TPosteriorMean();
	TPosteriorMean(int Dimension);
	~TPosteriorMean();
	void setDimension(int Dimension);
	void clear();
	void add(double* estimates);
	double getMean(int index);
	void setToMean(double* parameter);
};

//----------------------------------
class TPosteriorMean2D{
private:
	long numAdded;
	int dimension1;
	int* dimension2;
	bool dimension2Allocated;

	double** posteriorMean;
	bool initialized;

	void init(int Dimension1, int* Dimension2);
	void init(int Dimension1, int Dimension2);
	void initStorage();
	void free();

public:
	TPosteriorMean2D();
	TPosteriorMean2D(int Dimension1, int* Dimension2);
	TPosteriorMean2D(int Dimension1, int Dimension2);
	~TPosteriorMean2D();
	void setDimensions(int Dimension1, int* Dimension2);
	void setDimensions(int Dimension1, int Dimension2);
	void clear();
	void add(double** estimates);
	double getMean(int index1, int index2);
	void setToMean(double** parameters);
};




#endif /* TPOSTERIORMEAN_H_ */
