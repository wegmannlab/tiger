/*
 * TAtlasTesting.h
 *
 *  Created on: Dec 11, 2017
 *      Author: phaentu
 */

#ifndef TTESTING_H_
#define TTESTING_H_

#include "TTestList.h"

//-----------------------------------
//TTesting
//-----------------------------------
class TTesting{
private:
	TLog* logfile;
	std::string outputName;
	TAtlasTestList testList;
	TParameters* params;

	void parseTests(TParameters & params);

public:
	TTesting(TParameters & params, TLog* Logfile);
	~TTesting(){};
	void printTests();
	void addTest(std::string & name, TParameters & params);
	void runTests();
};


#endif /* TTESTING_H_ */
