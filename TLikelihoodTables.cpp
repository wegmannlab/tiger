/*
 * TLikelihoodTables.cpp
 *
 *  Created on: Feb 28, 2018
 *      Author: phaentu
 */


#include "TLikelihoodTables.h"


/////////////////////////////////////////////////////////////////////////////////////////
// TLikelihoodTables
/////////////////////////////////////////////////////////////////////////////////////////
TLikelihoodTablesOneError::TLikelihoodTablesOneError(std::string Name){
	init(Name);
}

TLikelihoodTablesOneError::TLikelihoodTablesOneError(std::string Name, std::string errorString){
	init(Name);
	parseErrorRates(errorString);
}

void TLikelihoodTablesOneError::init(std::string Name){
	storageInitialized = false;
	name = Name;

	//set genotype srrings
	genotypeStrings[0] = "0/0"; genotypeStrings[1] = "0/1"; genotypeStrings[2] = "1/1";
}

void TLikelihoodTablesOneError::initStorage(int NumErrorRates){
	freeStorage();
	numErrorRates = NumErrorRates;
	likTables = new TLikelihoodTable[numErrorRates];
	storageInitialized = true;
};

void TLikelihoodTablesOneError::freeStorage(){
	if(storageInitialized){
		delete[] likTables;
	}
};

void TLikelihoodTablesOneError::parseErrorRates(std::string & errorString){
	//read error rates
	fillVectorFromString(errorString, errorRates, ',');
	numErrorRates = errorRates.size();

	//prepare likelihood tables
	initStorage(numErrorRates);

	//fill likelihood
	for(int d=0; d<numErrorRates; ++d)
		likTables[d].update(errorRates[d]);
};

std::string TLikelihoodTablesOneError::getClassString(){
	//report
	std::string outString = name + ": ";
	if(numErrorRates == 1)
		outString += "a per allele genotyping error rate of " + toString(errorRates[0]) + ".";
	else {
		std::vector<double>::iterator it=errorRates.begin();
		outString += "error rates " + toString(*it); ++it;
		for(; it!=errorRates.end(); ++it)
			outString += ", " + toString(*it);
		outString += " for depths ";
		for(int d=0; d<numErrorRates; ++d)
			outString += toString(d+1) + ", ";
		outString += "respectively.";
	}
	return outString;
}

int TLikelihoodTablesOneError::simulateAndWriteGenotype(const int & trueGenotype, TSimulationVCF & vcf, TRandomGenerator & randomGenerator){
	int d = randomGenerator.pickOne(numErrorRates);
	return likTables[d].simulateAndWriteGenotype(trueGenotype, d+1, vcf, randomGenerator);
};

int TLikelihoodTablesOneError::simulateAndWriteGenotype(const int & trueGenotype, TSimulationVCF & vcf, std::ofstream & R_input, TRandomGenerator & randomGenerator){
	int d = randomGenerator.pickOne(numErrorRates);
	return likTables[d].simulateAndWriteGenotype(trueGenotype, d+1, vcf, R_input, randomGenerator);
};

//----------------------------------------------------------------
//TLikelihoodTablesTwoErrors
//----------------------------------------------------------------
TLikelihoodTablesTwoErrors::TLikelihoodTablesTwoErrors(std::string Name, std::string errorString, std::string errorStringHet):TLikelihoodTablesOneError(Name){
	parseErrorRates(errorString, errorStringHet);
};

void TLikelihoodTablesTwoErrors::initStorage(int NumErrorRates){
	freeStorage();
	numErrorRates = NumErrorRates;
	likTables = new TLikelihoodTableHet[numErrorRates];
	storageInitialized = true;
};

void TLikelihoodTablesTwoErrors::parseErrorRates(std::string & errorString, std::string & errorStringHet){
	//read error rates
	fillVectorFromString(errorString, errorRates, ',');
	numErrorRates = errorRates.size();

	//read errorHet rates
	fillVectorFromString(errorStringHet, errorRatesHet, ',');
	if(errorRatesHet.size() != numErrorRates)
		throw "Unequal number of error rates and errorHet rates provided!";

	//prepare likelihood tables
	initStorage(numErrorRates);

	//fill likelihood
	for(int d=0; d<numErrorRates; ++d)
		likTables[d].update(errorRates[d], errorRatesHet[d]);
};

std::string TLikelihoodTablesTwoErrors::getClassString(){
	//report
	std::string outString = name + ": ";
	if(numErrorRates == 1){
		outString += "a per allele genotyping error rate of " + toString(errorRates[0]);
		outString += " for homozygous and " + toString(errorRatesHet[0]) + " for heterozygous sites";
		outString += ".";
	} else {
		//homo
		std::vector<double>::iterator it=errorRates.begin();
		outString += "error rates " + toString(*it); ++it;
		for(; it!=errorRates.end(); ++it)
			outString += ", " + toString(*it);

		//hetero
		it=errorRatesHet.begin();
		outString += " for homozygous and " + toString(*it); ++it;
		for(; it!=errorRatesHet.end(); ++it)
			outString += ", " + toString(*it);
		outString += " for heterozygous sites,";

		//depth
		outString += " for depths ";
		for(int d=0; d<numErrorRates; ++d)
			outString += toString(d+1) + ", ";
		outString += "respectively.";
	}
	return outString;
};

//----------------------------------------------------------------
//TLikelihoodTables
//----------------------------------------------------------------
TLikelihoodTables::TLikelihoodTables(TParameters* myParameters, TLog* logfile, TRandomGenerator* RandomGenerator){
	randomGenerator = RandomGenerator;
	parseErrorRates(myParameters, logfile);
};

TLikelihoodTables::~TLikelihoodTables(){
	for(likClassIt=likTableErrorClasses.begin(); likClassIt!=likTableErrorClasses.end(); ++likClassIt)
		delete *likClassIt;
	likTableErrorClasses.clear();
};

void TLikelihoodTables::parseErrorRates(TParameters* myParameters, TLog* logfile){
	//read error rates
	logfile->startIndent("Setting error rates:");

	logfile->listFlush("Parsing error rates ...");
	std::string errorString = myParameters->getParameterString("error", false);
	trimString(errorString);
	std::vector<std::string> errorRates;
	if(errorString == ""){
		//no error rate provided: create a single group with a single error rate
		errorRates.push_back("0.05");
	} else if(errorString[0] == '['){
		//user specified (multiple) classes
		while(!errorString.empty()){
			if(errorString[0]!='[') throw "Unable to understand error set: missing [!";
			//find closing ]
			std::size_t pos = errorString.find_first_of(']');
			if(pos == std::string::npos) throw "Unable to understand error set: missing ]!";

			std::string tmp = errorString.substr(1, pos-1);
			trimString(tmp);
			if(tmp.empty()) throw "Unable to understand error classes: set is empty!";
			errorRates.push_back(tmp);
			errorString.erase(0,pos+2);
			trimString(errorString);
		}
	} else {
		//a single class is given
		errorRates.push_back(errorString);
	}
	logfile->done();
	logfile->conclude("Read " + toString(errorRates.size()) + " error rate sets.");

	//read errorHet rates
	int model = 1;
	std::vector<std::string> errorRatesHet;

	if(myParameters->parameterExists("errorHet")){
		logfile->listFlush("Parsing heterozygous error rates ...");
		model = 2;
		errorString = myParameters->getParameterString("errorHet", false);
		trimString(errorString);

		//now parse
		if(errorString[0] == '['){
			//user specified (multiple) classes
			while(!errorString.empty()){
				if(errorString[0]!='[') throw "Unable to understand error set: missing [!";
				//find closing ]
				std::size_t pos = errorString.find_first_of(']');
				if(pos == std::string::npos) throw "Unable to understand error set: missing ]!";
				std::string tmp = errorString.substr(1, pos-1);
				trimString(tmp);
				if(tmp.empty()) throw "Unable to understand error set: set is empty!";
				errorRatesHet.push_back(tmp);
				errorString.erase(0,pos+2);
				trimString(errorString);
			}
		} else {
			//a single class is given
			errorRatesHet.push_back(errorString);
		}
		logfile->done();
		logfile->conclude("Read " + toString(errorRates.size()) + " error rate sets.");

		//does number of classes match erro rates?
		if(errorRatesHet.size() != errorRates.size())
			throw "Unequal number of error rate sets provided by error and errorHet!";
	}

	//create likelihood tables
	std::vector<std::string>::iterator erIt = errorRates.begin();
	int classNum = 1;
	if(model == 1){
		for(; erIt!=errorRates.end(); ++erIt, ++classNum)
			likTableErrorClasses.push_back(new TLikelihoodTablesOneError("Batch_" + toString(classNum), *erIt));
	} else {
		std::vector<std::string>::iterator erHetIt = errorRatesHet.begin();
		for(; erIt!=errorRates.end(); ++erIt, ++erHetIt, ++classNum)
			likTableErrorClasses.push_back(new TLikelihoodTablesTwoErrors("Batch_" + toString(classNum), *erIt, *erHetIt));
	}
	numClasses = likTableErrorClasses.size();

	//report
	logfile->startIndent("Will simulate with the following " + toString(numClasses) + " error rate set(s):");
	for(likClassIt=likTableErrorClasses.begin(); likClassIt!=likTableErrorClasses.end(); ++likClassIt)
		logfile->list((*likClassIt)->getClassString());
	logfile->endIndent();
	logfile->endIndent();
};

void TLikelihoodTables::simulateErrorClassMemberships(int numSamples, std::vector<int> & errorClasses){
	//make sure each class is equally represented -> fill vector and shuffle
	errorClasses.clear();
	int n=0; int c=0;
	while(n<numSamples){
		errorClasses.push_back(c);
		++n;
		++c;
		c = c % numClasses;
	}

	//randomly shuffle vector
	randomGenerator->shuffle(errorClasses);
};

int TLikelihoodTables::simulateAndWriteGenotype(int trueGenotype, int errorClass, TSimulationVCF & vcf){
	return likTableErrorClasses[errorClass]->simulateAndWriteGenotype(trueGenotype, vcf, *randomGenerator);
};

int TLikelihoodTables::simulateAndWriteGenotype(int trueGenotype, int errorClass, TSimulationVCF & vcf, std::ofstream & R_input){
	return likTableErrorClasses[errorClass]->simulateAndWriteGenotype(trueGenotype, vcf, R_input, *randomGenerator);
};



