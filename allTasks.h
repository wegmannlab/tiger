/*
 * TTask.h
 *
 *  Created on: Mar 31, 2019
 *      Author: phaentu
 */

#ifndef ALLTASKS_H_
#define ALLTASKS_H_

#include "TTask.h"

//---------------------------------------------------------------------------
// all includes necessary for the application
//---------------------------------------------------------------------------
#include "TSimulator.h"
#include "TCompareToTruth.h"
#include "TGenoErrorEstimator.h"
#include "TAdjustPL.h"

//---------------------------------------------------------------------------
// TTask class specific to this application (optional)
//---------------------------------------------------------------------------
class TTask_tiger:public TTask{
public:
	TTask_tiger(){ _citations.push_back("Bresadola et al. (2019) Molecular Ecology Resources"); };
};

//---------------------------------------------------------------------------
// Create a class for each task, as shown in the example below
//---------------------------------------------------------------------------
//	class TTask_NAME:public TTask{ //or derive from class specific task
//		public:
//		TTask_NAME(){
//			_explanation = "SAY WHAT THIS TASK IS DOING";
//			_citations.push_back("THE CITATION YOU WANT TO ADD");
//		};

//		void run(TParameters & parameters, TLog* logfile){
//			SPECIFY HOW TO EXECUTE THIS TASK
//		};
//	};
//---------------------------------------------------------------------------


class TTask_simulate:public TTask_tiger{
public:
	TTask_simulate(){
		_explanation = "Simulating data";
	};

	void run(TParameters & parameters, TLog* logfile){
		TSimulator simulator(parameters, logfile, randomGenerator);
		std::string model = parameters.getParameterString("model");
		simulator.runSimulations(model);
	};
};

class TTask_truthSet:public TTask_tiger{
public:
	TTask_truthSet(){
		_explanation = "Estimating genotyping error rates using a  set";
	};

	void run(TParameters & parameters, TLog* logfile){
			TCompareToTruth compareToTruth(parameters, logfile);
			compareToTruth.compareCallsIndividuals();
	};
};

class TTask_sampleReps:public TTask_tiger{
public:
	TTask_sampleReps(){
		_explanation = "Estimating genotyping error rates from individual replicates";
	};

	void run(TParameters & parameters, TLog* logfile){
		TGenoErrorEstimator estimator(randomGenerator, logfile);
		estimator.inferGenoError(&parameters, "indReps");
	};
};

class TTask_hardyWeinberg:public TTask_tiger{
public:
	TTask_hardyWeinberg(){
		_explanation = "Estimating genotyping error rates assuming Hardy-Weinberg equilibirum";
	};

	void run(TParameters & parameters, TLog* logfile){
		TGenoErrorEstimator estimator(randomGenerator, logfile);
		estimator.inferGenoError(&parameters, "hardyWeinberg");
	};
};

class TTask_adjustPL:public TTask_tiger{
public:
		TTask_adjustPL(){
		_explanation = "Adjusting genotype likelihoods in VCF files";
	};

	void run(TParameters & parameters, TLog* logfile){
		TAdjustPL adjuster(logfile);
		adjuster.adjustPL(&parameters);
	};
};

class TTask_surface:public TTask_tiger{
public:
	TTask_surface(){
		_explanation = "Calculating posterior surface of Hardy-Weinberg model)";
	};

	void run(TParameters & parameters, TLog* logfile){
		TGenoErrorEstimator estimator(randomGenerator, logfile);
		estimator.calculatePosteriorSurface(&parameters);
	};
};

class TTask_truthSetErrors:public TTask_tiger{
public:
	TTask_truthSetErrors(){
		_explanation = "Assessing the impact of errors in the truth set";
	};

	void run(TParameters & parameters, TLog* logfile){
			TCompareToTruth compareToTruth(parameters, logfile);
			compareToTruth.assessErrorsInTruthSet(randomGenerator);
	};
};

#endif /* ALLTASKS_H_ */
