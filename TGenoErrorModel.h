/*
 * TGenoErrorModel.h
 *
 *  Created on: Feb 26, 2018
 *      Author: wegmannd
 */

#ifndef TGENOERRORMODEL_H_
#define TGENOERRORMODEL_H_

#include "TLikelihoodModel.h"

//////////////////////////////////////
// EM and MCMC parameters
//////////////////////////////////////
class TGenoErrorEmParams{
public:
	double minDelta;
	int maxNumEmIterations;
	int minEpsIter;
	int maxEpsIter;

	TGenoErrorEmParams(TParameters* myParameters, TLog* logfile);
};

class TGenoErrorMcmcParams{
public:
	int numIterations;
	int burninLength;
	int numBurnins;
	long totNumIterations;
	int thinning;

	//priors
	double epsLambda;

	/*
	//update SD
	double epsPropWidth;
	double epsPropWidthHalf;
	double otherProbWidth;
	*/

	TGenoErrorMcmcParams(TParameters* myParameters, TLog* logfile);
};

//////////////////////////////////////
// TGenoErrorOneErrorModel
//////////////////////////////////////
class TGenoErrorOneErrorModel{
protected:
	//storage for EM
	int* numDepthWithData;
	int numGroups;
	int numErrorRateClasses;
	int numSnps;

	//storage for MCMC
	double** errorHastingsTermsNew;
	double** errorHastingsTermsOld;
	bool MCMCStorageInitialized;
	TPosteriorMean2D posteriorMeanEpsilon;
	long** acceptanceRatesEpsilon;
	double** proposalWidthEpsilon;

	//storage for both
	double** epsilon;
	double** newEpsilon;
	bool emissionProbsInitialized;
	bool errorRateStorageInitialized;
	double**** emissionProbs;

	//tmp storage for numerical optimization
	double** curQ;
	double** oldQ;
	double** initialQ;
	double** step;
	bool tmpOptimizationStorageInitialized;

	//model
	TLikelihoodModel* likelihoodModel;

	//initialization / storage
	void initEstimation(TGenoErrorStorage & Data, TLikelihoodModel* LikelihoodModel, TLog* logfiles);
	void initializeEmissionProbabilityMatrix();
	void freeEmissionProbabilityMatrix();
	void initializeEMStorage();
	virtual void initializeErrorRateStorage();
	void freeEMStorage();
	virtual void freeErrorRateStorage();
	void initializeTmpOptimizationStorageForEM();
	void freeTmpOptimizationStorageForEM();
	virtual void initializeMCMCStorage();
	virtual void freeMCMCStorage();

	//common
	void estimateInitialParameters(TGenoErrorStorage & Data, TLog* logfile);
	virtual void setInitialEpsilon(const double & eps);
	virtual void fillEmissionProbabilitiesCurErrorRates();
	virtual void fillEmissionProbabilitiesNewErrorRates();
	void fillEmissionProbabilities(double** thisEpsilon);
	void makeEmissionProbsInLog();
	double calculateLLCurErrorRates(TGenoErrorStorage & Data);
	virtual void calculateQ_epsilon(TGenoErrorStorage & Data, double** Q, double** thisEpsilon);

	//EM
	void initEpsilonOptimization(double** thisEpsilon, double** thisNewEpsilon);
	void proposeNewEpsilonForOptimization(double** thisEpsilon);
	bool adjustStepSizeAndCheckImprovement();
	virtual void estimateEpsilonNumerically(TGenoErrorStorage & Data, int minNumIter, int maxNumIter);
	virtual void saveEstimatesToOld();
	void runEM(TGenoErrorStorage & Data, TGenoErrorEmParams & EM_params, TLog* logfile);

	//MCMC
	virtual void initPosteriorMeanStorage();
	virtual void addCurEstimatesToPosteriorMeanStorage();
	virtual void setEpsilonToPosteriorMean();
	void setProposalWidthErrorRates(double** thisEpsilon, double** thisProposalWidth, long** thisAcceptanceRate);
	void updateProposalWidthErrorRates(double** thisProposalWidth, long** thisAcceptanceRate, long len);
	virtual void setInitialProposalWidthErrorRates();
	double calculateAcceptanceRateErrorRates(long len);
	void proposeNewErrorRatesMCMC(double** thisEpsilon, double** thisNewEpsilon, double** proposalWidth, TRandomGenerator & randomGenerator);
	void acceptOrRejectErrorRatesMCMC(double** thisEpsilon, double** thisNewEpsilon, double & priorLambda, long** thisAcceptanceRate, TRandomGenerator & randomGenerator);
	virtual void updateErrorRatesMCMC(TGenoErrorStorage & Data, TGenoErrorMcmcParams & MCMC_params, TRandomGenerator & randomGenerator);
	virtual int writeEpsilonHeaderToMCMCTraceFile(gz::ogzstream & out);
	virtual void writeCurEpsilonEstimatesToTraceFile(gz::ogzstream & out);
	double calcCurPosteriorDensity(TGenoErrorStorage & Data, TGenoErrorMcmcParams & MCMC_params, TRandomGenerator* randomGenerator);
	virtual void adjustAfterBurnin(TGenoErrorMcmcParams & MCMC_params);
	virtual void writeAcceptanceRateErrorRates(const int & length, TLog* logfile);
	virtual std::string getNameOfMCMCTraceFile(std::string & outname);
	void runMCMC(TGenoErrorStorage & Data, TGenoErrorMcmcParams & MCMC_params, std::string & outname, TRandomGenerator* randomGenerator, TLog* logfile);

public:
	TGenoErrorOneErrorModel();
	virtual ~TGenoErrorOneErrorModel();
	void estimateErrorRatesMLE(TGenoErrorStorage & Data,  TLikelihoodModel* LikelihoodModel, TGenoErrorEmParams & EM_params, TLog* logfile);
	void estimateErrorRatesBayesian(TGenoErrorStorage & Data,  TLikelihoodModel* LikelihoodModel, TGenoErrorMcmcParams & MCMC_params, std::string & outname,  TRandomGenerator* randomGenerator, TLog* logfile);
	void posteriorSurface(TGenoErrorStorage & Data, std::string frequencyFile, std::string & outname, TRandomGenerator* randomGenerator, TLog* logfile);
	double getErrorRate(int errorClass, int depthBin){ return epsilon[errorClass][depthBin]; };
};

//////////////////////////////////////
// TGenoErrorTwoErrorModel
//////////////////////////////////////
class TGenoErrorTwoErrorModel:public TGenoErrorOneErrorModel{
protected:
	double** epsilonHet;
	double** newEpsilonHet;
	bool errorRateStorageInitializedHet;
	TPosteriorMean2D posteriorMeanEpsilonHet;
	long** acceptanceRatesEpsilonHet;
	double** proposalWidthEpsilonHet;
	bool MCMCStorageHetInitialized;

	//init / storage
	void initializeErrorRateStorage();
	void freeErrorRateStorage();
	void initializeMCMCStorage();
	void freeMCMCStorage();

	//common
	void setInitialEpsilon(const double & eps);
	void fillEmissionProbabilitiesCurErrorRates();
	void fillEmissionProbabilitiesNewErrorRates();
	void fillEmissionProbabilities(double** thisEpsilon, double** thisEpsilonHet);

	//EM
	void calculateQ_epsilon(TGenoErrorStorage & Data, double** Q, double** thisEpsilon);
	void calculateQ_epsilonHet(TGenoErrorStorage & Data, double** Q, double** thisEpsilonHet);
	void estimateEpsilonNumerically(TGenoErrorStorage & Data, int minNumIter, int maxNumIter);
	void saveEstimatesToOld();

	//MCMC
	void initPosteriorMeanStorage();
	void addCurEstimatesToPosteriorMeanStorage();
	void setEpsilonToPosteriorMean();
	void setInitialProposalWidthErrorRates();
	void updateErrorRatesMCMC(TGenoErrorStorage & Data, TGenoErrorMcmcParams & MCMC_params, TRandomGenerator & randomGenerator);
	int writeEpsilonHeaderToMCMCTraceFile(std::ofstream & out);
	void writeCurEpsilonEstimatesToTraceFile(std::ofstream & out);
	void adjustAfterBurnin(TGenoErrorMcmcParams & MCMC_params);
	double calculateAcceptanceRateErrorRatesHet(long len);
	void writeAcceptanceRateErrorRates(const int & length, TLog* logfile);
	std::string getNameOfMCMCTraceFile(std::string & outname);

public:

	TGenoErrorTwoErrorModel();
	~TGenoErrorTwoErrorModel();

	double getErrorRateHom(int errorClass, int depthBin){ return epsilon[errorClass][depthBin]; };
	double getErrorRateHet(int errorClass, int depthBin){ return epsilonHet[errorClass][depthBin]; };

};


#endif /* TGENOERRORMODEL_H_ */
