/*
 * TGenoError.h
 *
 *  Created on: May 11, 2017
 *      Author: wegmannd
 */

#ifndef TGENOERRORESTIMATOR_H_
#define TGENOERRORESTIMATOR_H_

#include "TParameters.h"
#include "stringFunctions.h"
#include "TGenoErrorStorage.h"
#include "TGenoErrorModel.h"
#include "TRandomGenerator.h"
#include "TLog.h"

////////////////////////////////////
// TGenoErrorEstimator
////////////////////////////////////
class TGenoErrorEstimator{
protected:
	TLog* logfile;
	std::string outname;
	std::string defaultInferenceAlgo;
	TRandomGenerator* randomGenerator;

	//storage for data
	TGenoErrorStorage data;
	TGenoErrorOneErrorModel model1;
	TGenoErrorTwoErrorModel model2;

	//likelihood model
	TLikelihoodModel* likelihoodModel;
	bool likelihoodModelInitialized;

	void createLikelihoodModel(std::string model);
	void inferGenoErrorMLE(TParameters* myParameters);
	void inferGenoErrorBayes(TParameters* myParameters);
	void writeEstimates();

public:
	TGenoErrorEstimator(TRandomGenerator* RandomGenerator, TLog* Logfile);
	virtual ~TGenoErrorEstimator(){
		if(likelihoodModelInitialized)
			delete likelihoodModel;
	};

	void inferGenoError(TParameters* myParameters, std::string model);
	void calculatePosteriorSurface(TParameters* myParameters);
};




#endif /* TGENOERRORESTIMATOR_H_ */
