/*
 * TSampleGroup.cpp
 *
 *  Created on: Aug 9, 2011
 *      Author: wegmannd
 */

#include "TSampleGroup.h"

//--------------------------------------------------
//TSampleGroup
//--------------------------------------------------
TSampleGroup::TSampleGroup(std::string Name, TVcfFileSingleLine* VcfFile){
	name = Name;
	vcfFile=VcfFile;
	fastAccessInitialized = false;
	numSamples = 0;
	samples = NULL;
	fastAccessNeedsUpdate = false;
}

TSampleGroup::TSampleGroup(std::string Name, TVcfFileSingleLine* VcfFile, std::string & Samples){
	name = Name;
	vcfFile=VcfFile;
	fastAccessInitialized = false;
	addSamplesFromSampleString(Samples);
}

TSampleGroup::~TSampleGroup(){
	if(fastAccessInitialized)
		delete[] samples;
}

void TSampleGroup::addSample(std::string sampleName){
	sampleNames.push_back(sampleName);

	fastAccessNeedsUpdate = true;
};

void TSampleGroup::addSamplesFromSampleString(std::string & Samples){
	std::string temp;
	while(!Samples.empty()){
		temp=extractBefore(Samples, ',');
		trimString(temp);
		Samples.erase(0,1);
		if(!temp.empty()){
			trimString(temp);
			sampleNames.push_back(temp);
		}
	}

	fastAccessNeedsUpdate = true;
	updateFastAccess();
};

void TSampleGroup::updateFastAccess(){
	if(fastAccessNeedsUpdate){
		//delete fast access if it already exists
		if(fastAccessInitialized)
			delete[] samples;

		//store numbers as array for faster parsing
		numSamples = sampleNames.size();
		samples = new unsigned int[numSamples];
		int i=0;
		for(std::vector<std::string>::iterator it = sampleNames.begin(); it != sampleNames.end(); ++it, ++i){
			samples[i] = vcfFile->sampleNumber(*it);
		}
		fastAccessInitialized = true;
		fastAccessNeedsUpdate = false;
	}
}

std::string TSampleGroup::getListOfSampleNames(){
	std::string s;
	bool first=true;
	for(std::vector<std::string>::iterator it = sampleNames.begin(); it != sampleNames.end(); ++it){
		if(first) first=false;
		else s += ", ";
		s += *it;
	}
	return s;
}

void TSampleGroup::fillAlleleCountsFromGT(int & numRef, int & numAlt){
	numRef = 0; numAlt = 0;
	for(int i=0; i<numSamples; ++i){
		if(!vcfFile->sampleIsMissing(samples[i])){
			if(vcfFile->sampleIsHomoRef(samples[i])) numRef +=2;
			else if(vcfFile->sampleIsHeteroRefNonref(samples[i])){
				++numRef; ++numAlt;
			} else numAlt += 2;
		}
	}
}

void TSampleGroup::fillAlleleCountsFromGP(int & numRef, int & numAlt, double & cutOff){
	numRef = 0; numAlt = 0;
	std::vector<double> gp;
	int g;
	for(int i=0; i<numSamples; ++i){
		fillVectorFromString(vcfFile->getSampleContentAt("GP", samples[i]), gp, ',');
		for(g=0; g<3; ++g){
			if(gp[g] > cutOff){
				numRef += 2 - g;
				numAlt += g;
			}
		}
	}
}

void TSampleGroup::addSamplesToVector(std::vector<int> & vec){
	for(int i=0; i<numSamples; ++i)
		vec.push_back(samples[i]);
}

void TSampleGroup::addSamplesToVector(std::vector<std::string> & vec){
	for(std::vector<std::string>::iterator it = sampleNames.begin(); it != sampleNames.end(); ++it)
		vec.push_back(*it);
}

void TSampleGroup::addSamplesToArray(unsigned int* array, int & index){
	for(int i=0; i<numSamples; ++i, ++index)
		array[index] = samples[i];
}

bool TSampleGroup::sampleInGroup(const int & sample){
	for(int i=0; i<numSamples; ++i){
		if(samples[i] == sample)
			return true;
	}
	return false;
}

//--------------------------------------------------
//TSampleGroups
//--------------------------------------------------
TSampleGroups::TSampleGroups(){
	vcfFile = NULL;
	numSamples = 0;
	numGroups = 0;
	numSamplesPerGroup = NULL;
	pointerToSampleVectors = NULL;
	groupOfSample = NULL;
	groupOfSampleVcfIndex = NULL;
	initialized = false;

	for(it=groups.begin(); it!=groups.end(); ++it)
		delete (*it);
}

TSampleGroups::TSampleGroups(TVcfFileSingleLine* VcfFile, std::string Samples, TLog* logfile){
	initialize(VcfFile, Samples, logfile, true);
}

void TSampleGroups::initializeSingleGroup(TVcfFileSingleLine* VcfFile, TLog* logfile){
	//groups should have format [Sample1,Sampl5],[Sample6,Sample7]
	vcfFile = VcfFile;

	groups.push_back(new TSampleGroup("all", vcfFile));
	it = groups.end() - 1;
	for(int s=0; s<vcfFile->numSamples(); ++s)
		(*it)->addSample(vcfFile->sampleName(s));

	//fill maps and print
	fillMap();
	print(logfile);
	logfile->endIndent();
}

void TSampleGroups::initializeSingleGroup(TVcfFileSingleLine* VcfFile, std::vector<std::string> Samples, TLog* logfile){
	//groups should have format [Sample1,Sampl5],[Sample6,Sample7]
	vcfFile = VcfFile;

	groups.push_back(new TSampleGroup("all", vcfFile));
	it = groups.end() - 1;
	for(std::vector<std::string>::iterator sIt=Samples.begin(); sIt!=Samples.end(); ++sIt)
		(*it)->addSample(*sIt);

	//fill maps and print
	fillMap();
	print(logfile);
	logfile->endIndent();
}

void TSampleGroups::initialize(TVcfFileSingleLine* VcfFile, std::string Samples, TLog* logfile, bool _removeGroupsSizeOne){
	//groups should have format [Sample1,Sampl5],[Sample6,Sample7]
	vcfFile = VcfFile;
	logfile->listFlush("Parsing from string ...");
	//parse samples enclosed in []
	trimString(Samples);
	numSamples = 0;
	int g=1;
	while(!Samples.empty()){
		if(Samples[0]!='[') throw "Unable to understand sample groups/error classes: missing [!";
		//find closing ]
		std::size_t pos = Samples.find_first_of(']');
		if(pos == std::string::npos) throw "Unable to understand sample groups/error classes: missing ]!";
		std::string tmp = Samples.substr(1, pos-1);
		trimString(tmp);
		if(tmp.empty()) throw "Unable to understand sample groups/error classes: group is empty!";
		groups.push_back(new TSampleGroup("Group" + toString(g), vcfFile, tmp));
		++g;
		Samples.erase(0,pos+2);
		trimString(Samples);
	}
	logfile->done();

	//check if there are groups that one have 1 sample and remove them
	if(_removeGroupsSizeOne)
		removeGroupsSizeOne(logfile);

	//fill maps and print
	fillMap();
	print(logfile);
}

void TSampleGroups::initialize(TVcfFileSingleLine* VcfFile, std::string filename, int column, TLog* logfile, bool _removeGroupsSizeOne){
	//file shoudl have at least two columns: first is always sample name, later is group assignment.
	//(int) column indicates which column to read assignments from.
	vcfFile = VcfFile;
	logfile->listFlush("Parsing from column " + toString(column+1) + " of file '" + filename + "' ...");
	if(column==0) throw "Can not read group/error class assignments from first column!";

	//open file
	std::ifstream file(filename.c_str());
	if(!file) throw "Failed to open file '" + filename + "'!";

	//read header
	std::vector<std::string> tmpVec;
	fillVectorFromLineWhiteSpaceSkipEmpty(file, tmpVec);
	int numHeaderCol = tmpVec.size();
	if(numHeaderCol < column) throw "Wrong number of columns in " + filename + ": " + toString(numHeaderCol) + " instead of at least " + toString(column+1) + "!";


	//read line by line and add samples to groups
	int lineNum = 0;
	while(file.good() && !file.eof()){
		++lineNum;
		fillVectorFromLineWhiteSpaceSkipEmpty(file, tmpVec);
		if(!tmpVec.empty()){
			if(tmpVec.size() != numHeaderCol) throw "Wrong number of entries on line " + toString(lineNum) + " in file '" + filename + "'! There should be " + toString(numHeaderCol) + "!";

			//check if group exists
			it = groups.begin();
			while(it != groups.end()){
				if((*it)->name == tmpVec[column])
					break;
				++it;
			}

			//if not, create
			if(it==groups.end()){
				groups.push_back(new TSampleGroup(tmpVec[column], vcfFile));
				it = groups.end() - 1;
			}

			//add sample
			(*it)->addSample(tmpVec[0]);
		}
	}
	file.close();
	logfile->done();

	//check if there are groups that one have 1 sample and remove them
	if(_removeGroupsSizeOne)
		removeGroupsSizeOne(logfile);

	//fill maps and print
	fillMap();
	print(logfile);
}

void TSampleGroups::removeGroupsSizeOne(TLog* logfile){
	std::vector<TSampleGroup*>::iterator it = groups.begin();
	std::vector<std::string> removedGroups;
	while(it!=groups.end()){
		if((*it)->sampleNames.size() == 1){
			removedGroups.push_back((*it)->name);
			groups.erase(it);
		}
		else
			++it;
	}

	if(removedGroups.size() > 0){
		logfile->startIndent("Ignoring the following groups due to sample size = 1:");
		for(unsigned int i=0; i<removedGroups.size(); ++i)
			logfile->list(removedGroups[i]);
		logfile->endIndent();
	}
}

void TSampleGroups::fillMap(){
	//fill map
	numGroups = groups.size();
	numSamples = 0;
	numSamplesPerGroup = new int[numGroups];
	pointerToSampleVectors = new unsigned int*[numGroups];

	int g=0; int s=0;
	for(it=groups.begin(); it!=groups.end(); ++it, ++g){
		(*it)->updateFastAccess();
		numSamples += (*it)->numSamples;
		numSamplesPerGroup[g] = (*it)->numSamples;
		pointerToSampleVectors[g] = (*it)->samples;
	}

	//boolean for each vcf sample whether or not it is in any group
	groupOfSampleVcfIndex = new int[vcfFile->numSamples()];
	for(s=0; s<vcfFile->numSamples(); ++s){
		//set to -1 (not in any group) as defaul
		groupOfSampleVcfIndex[s] = -1;
		//check if sample is in any group
		g = 0;
		for(it=groups.begin(); it!=groups.end(); ++it, ++g){
			if((*it)->sampleInGroup(s))
				groupOfSampleVcfIndex[s] = g;
		}
	}

	//also array indicating group index for each sample index (NOT number in VCF!!)
	groupOfSample = new int[numSamples];
	g=0; s=0;
	for(it=groups.begin(); it!=groups.end(); ++it, ++g){
		for(int i=0; i<(*it)->numSamples; ++i, ++s){
			groupOfSample[s] = g;
		}
	}

	initialized = true;
}

void TSampleGroups::print(TLog* logfile){
	//print groups
	logfile->startIndent("Will use the following groups in the error estimation:");
	for(it=groups.begin(); it!=groups.end(); ++it){
		logfile->list((*it)->name + ": " + (*it)->getListOfSampleNames());
	}
	logfile->endIndent();
}

std::string TSampleGroups::getName(int groupIndex){
	return groups[groupIndex]->name;
}

std::string TSampleGroups::getGroupString(int group){
	return groups[group]->getListOfSampleNames();
}

void TSampleGroups::fillVectorOfGroupNames(std::vector<std::string> & vec){
	vec.clear();
	for(it=groups.begin(); it!=groups.end(); ++it)
		vec.push_back((*it)->name);
}

void TSampleGroups::fillCurrentAlleleCountsGT(int & numRef, int & numAlt){
	(*it)->fillAlleleCountsFromGT(numRef, numAlt);
}

void TSampleGroups::fillCurrentAlleleCountsGP(int & numRef, int & numAlt, double & cutOff){
	(*it)->fillAlleleCountsFromGP(numRef, numAlt, cutOff);
}

void TSampleGroups::fillVectorOfAllSamples(std::vector<int> & vec){
	vec.clear();
	for(it=groups.begin(); it!=groups.end(); ++it){
		(*it)->addSamplesToVector(vec);
	}
}

void TSampleGroups::fillVectorOfAllSamples(std::vector<std::string> & vec){
	vec.clear();
	for(it=groups.begin(); it!=groups.end(); ++it){
		(*it)->addSamplesToVector(vec);
	}
}

void TSampleGroups::fillArrayOfAllSamples(unsigned int* & array){
	array = new unsigned int[numSamples];
	int index=0;
	for(it=groups.begin(); it!=groups.end(); ++it){
		(*it)->addSamplesToArray(array, index);
	}
}

void TSampleGroups::fill2DArrayOfSamples(unsigned int** & array){
	array = new unsigned int*[numGroups];
	int index=0; int g=0;
	for(it=groups.begin(); it!=groups.end(); ++it, ++g){
		index = 0;
		array[g] = new unsigned int[(*it)->numSamples];
		(*it)->addSamplesToArray(array[g], index);
	}
}

bool TSampleGroups::sampleVcfIndexInGroup(const int & sample){
	return groupOfSampleVcfIndex[sample] >= 0;
}

int TSampleGroups::getGroupOfSampleVcfIndex(const int & sample){
	if(groupOfSampleVcfIndex[sample] < 0)
		throw "Sample with VCF index " + toString(sample) + " is not in any group!";

	return groupOfSampleVcfIndex[sample];
}
