/*
 * TSimulator.h
 *
 *  Created on: Feb 27, 2018
 *      Author: wegmannd
 */

#ifndef TSIMULATOR_H_
#define TSIMULATOR_H_

#include "TLikelihoodTables.h"
#include "TParameters.h"
#include "TRandomGenerator.h"
#include <numeric>

/////////////////////////////////////////////////////////////////////////////////////////
// SIMULATOR                                                                           //
/////////////////////////////////////////////////////////////////////////////////////////

class TSimulator{
private:
	TParameters* parameters;
	TLog* logfile;
	TRandomGenerator* randomGenerator;

	//simulation parameters
	int totNumAlleles;
	int numSamples;
	int numSites;

	//likelihood table
	TLikelihoodTables* likelihoodTables;
	bool likelihoodTablesInitialized;

	//samples
	std::vector<std::string> sampleNames;
	std::vector<int> groupOfSample;
	std::vector<int> errorClassOfSample;

	//output
	std::string outname;
	TSimulationVCF vcf;

	void prepareSimulations();
	void readingCommonParameters();
	void prepareLikelihoodTables();
	void openVCF();
	void writeSampleGroupErrorClassFile();
	void clean();
	int simulateTrueGenotype(const int trueGenotype, const int meanDepth, const double seqError);

public:
	TSimulator(TParameters & Parameters, TLog* Logfile, TRandomGenerator* RandomGenerator);
	~TSimulator(){
		clean();
	};

	void runSimulations(std::string model);
	void simulateSampleReplicates();
	void simulateHardyWeinberg();
	void simulatePopulationReplicates();
	void simulateTruthSetData();

};
















#endif /* TSIMULATOR_H_ */
