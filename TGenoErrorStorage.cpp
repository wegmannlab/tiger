/*
 * TGenoErrorStorage.cpp
 *
 *  Created on: Feb 26, 2018
 *      Author: wegmannd
 */


#include "TGenoErrorStorage.h"


//////////////////////////////////////
// TGenoErrorReplicatesStorage
//////////////////////////////////////
TGenoErrorStorage::TGenoErrorStorage(){
	vcfRead = false;
	numSamples = 0;
	numSnps = 0;
	numGroups = 0;
	numErrorClasses = 0;
	maxDepthPlusOne = 0;
	numDepthsWithData = 0;
	depthsWithData = NULL;
	depthIndex = NULL;
	numDataPoints = NULL;
	allSamples = NULL;
	batchOfSample = NULL;
	groupOfSample = NULL;
	numSamplesPerGroup = NULL;
};

TGenoErrorStorage::TGenoErrorStorage(TParameters* myParameters, TLog* logfile){
	readVCF(myParameters, logfile);
};


void TGenoErrorStorage::openVCF(TVcfFileSingleLine & vcfFile, TLog* logfile){
	bool isZipped = false;
	if(vcfFilename.find(".gz") == std::string::npos){
		logfile->list("Reading vcf from file '" + vcfFilename + "'.");
	} else {
		logfile->list("Reading vcf from gzipped file '" + vcfFilename + "'.");
		isZipped = true;
	}

	vcfFile.openStream(vcfFilename, isZipped);

	//enable parsers
	vcfFile.enablePositionParsing();
	vcfFile.enableFormatParsing();
	vcfFile.enableSampleParsing();
	vcfFile.enableVariantParsing();
}

void TGenoErrorStorage::readSampleGroups(TParameters* myParameters, TLog* logfile, TVcfFileSingleLine & vcfFile){
	//read sample groups
	logfile->startIndent("Parsing sample groups:");
	std::string groupString = myParameters->getParameterString("groups");
	if(groupString[0] == '['){
		//parse as group string
		groups.initialize(&vcfFile, groupString, logfile, true);
	} else {
		//parse as file
		int groupColumn = myParameters->getParameterIntWithDefault("groupCol", 2) - 1;
		groups.initialize(&vcfFile, groupString, groupColumn, logfile, true);
	}
	if(groups.numGroups < 1) throw "No groups defined!";
	logfile->endIndent();

	numGroups = groups.numGroups;

	//fill array of all samples
	numSamples = groups.numSamples;
	groups.fillArrayOfAllSamples(allSamples);

	//store sample names in same order
	groups.fillVectorOfAllSamples(sampleNames);

	//store sample size per group
	numSamplesPerGroup = groups.numSamplesPerGroup;

	//store group index for each sample
	groupOfSample = groups.groupOfSample;
}

void TGenoErrorStorage::readErrorClassOfSamples(TParameters* myParameters, TLog* logfile, TVcfFileSingleLine & vcfFile){
	//read error class
	logfile->startIndent("Parsing sample sequencing batches:");
	std::string errorClassString = myParameters->getParameterString("batches", false);
	batchOfSample = new int[groups.numSamples];
	if(errorClassString == ""){
		//only one class for all samples
		logfile->list("Using a single batch 'all' for all samples.");
		for(int s=0; s<groups.numSamples; ++s)
			batchOfSample[s] = 0;
		errorClassNames.push_back("all");
	} else {
		//abuse sample groups to read / parse
		TSampleGroups batches;

		if(errorClassString[0] == '['){
			//parse as string
			batches.initialize(&vcfFile, errorClassString, logfile, false);
		} else {
			//parse as file
			int errorColumn = myParameters->getParameterIntWithDefault("batchesCol", 3) - 1;
			batches.initialize(&vcfFile, errorClassString, errorColumn, logfile, false);
		}
		if(batches.numGroups < 1) throw "No error classes defined!";

		//save error class names
		batches.fillVectorOfGroupNames(errorClassNames);

		//fill array of group membership that is IN THE SAME ORDER as samples in groups
		for(int s=0; s<groups.numSamples; ++s){
			if(!batches.sampleVcfIndexInGroup(allSamples[s]))
				throw "Sample " + sampleNames[s] + " has no error rate class!";
			batchOfSample[s] = batches.getGroupOfSampleVcfIndex(allSamples[s]);
		}

		//check if there are error classes with size = 1 and give warning
		for(int g=0; g<batches.numGroups; ++g){
			if(batches.numSamplesPerGroup[g] == 1)
				logfile->warning("Error class " + batches.getName(g) + " has only 1 sample! The error rate might not be estimated correctly!");
		}
	}
	numErrorClasses = errorClassNames.size();
	logfile->endIndent();
}

void TGenoErrorStorage::fillDepthIndexes(){
	//create list of depth with data so that we will only need to consider those
	//variables
	int c,d,i,s;
	bool** depthsHasData = new bool*[numErrorClasses];
	numDepthsWithData = new int[numErrorClasses];
	for(c=0; c<numErrorClasses; ++c){
		depthsHasData[c] = new bool[maxDepthPlusOne];
		numDepthsWithData[c] = 1; //add extra one for depth == 0
		for(d = 0; d<maxDepthPlusOne; ++d)
			depthsHasData[c][d] = false;
	}

	//loop across depths
	for(std::vector<int*>::iterator itD = depths.begin(); itD != depths.end(); ++itD){
		for(s=0; s<groups.numSamples; ++s){
			depthsHasData[batchOfSample[s]][(*itD)[s]] = true;
		}
	}

	//set num depths with data for each class, ignore depth=0
	for(c=0; c<numErrorClasses; ++c){
		for(d = 1; d<maxDepthPlusOne; ++d){
			if(depthsHasData[c][d])
				++numDepthsWithData[c];
		}
	}

	//create lookup table
	depthsWithData = new int*[numErrorClasses];
	depthIndex = new int*[numErrorClasses];
	for(c=0; c<numErrorClasses; ++c){
		depthsWithData[c] = new int[numDepthsWithData[c]];
		depthIndex[c] = new int[maxDepthPlusOne];

		//add extra for depth = 0
		depthsWithData[c][0] = 0;
		depthIndex[c][0] = 0;

		//fill in others
		i = 1;
		for(d = 1; d<maxDepthPlusOne; ++d){
			if(depthsHasData[c][d]){
				depthsWithData[c][i] = d;
				depthIndex[c][d] = i;
				++i;
			} else
				depthIndex[c][d] = -1;
		}
	}

	//now store depth according to new index and count data points
	//create index
	numDataPoints = new long*[numErrorClasses];
	for(c=0; c<numErrorClasses; ++c){
		numDataPoints[c] = new long[numDepthsWithData[c]];
		for(d = 0; d<numDepthsWithData[c]; ++d)
			numDataPoints[c][d] = 0;
	}

	//loop across depth and samples
	for(std::vector<int*>::iterator itD = depths.begin(); itD != depths.end(); ++itD){
		for(s=0; s<groups.numSamples; ++s){
			(*itD)[s] = depthIndex[batchOfSample[s]][(*itD)[s]];
			if(batchOfSample[s] < 0) throw "errorClassOfSample[s] index < 0";
			if((*itD)[s] >= 0)
				++numDataPoints[batchOfSample[s]][(*itD)[s]];
		}
	}

	//clean up
	for(c=0; c<numErrorClasses; ++c)
		delete[] depthsHasData[c];
	delete[] depthsHasData;
}

void TGenoErrorStorage::readVCF(TParameters* myParameters, TLog* logfile){
	if(vcfRead)
		throw "VCF already read!";

	//open vcf file
	vcfFilename = myParameters->getParameterString("vcf");
	TVcfFileSingleLine vcfFile;
	openVCF(vcfFile, logfile);

	//read groups and error classes
	readSampleGroups(myParameters, logfile, vcfFile);
	readErrorClassOfSamples(myParameters, logfile, vcfFile);

	//do we limit the lines to read?
	int limitLines = myParameters->getParameterIntWithDefault("limitLines", -1);
	if(limitLines > 0)
		logfile->list("Will limit analysis to the first " + toString(limitLines) + " lines of the VCF file.");

	//prepare storage: genotypes and depth
	short* genoPointer;
	int* depthsPointer;
	maxDepthPlusOne = 0;
	int s;

	//Loop through VCF file
	logfile->startIndent("Parsing VCF file:");
	numSnps = 0;
	int counter = 0;
	std::string curChrName = "";
	int curChr = -1;
	while(vcfFile.next()){
		++counter;
		//check if it is biallelic and ignore others
		if(vcfFile.getNumAlleles()==2){
			//save SNP
			++numSnps;
			if(vcfFile.chr() != curChrName){
				chromosomeNames.push_back(vcfFile.chr());
				curChrName = vcfFile.chr();
				++curChr;
			}
			snpChr.push_back(curChr);
			snpPosition.push_back(vcfFile.position());

			//save genotypes
			genoPointer = new short[groups.numSamples];
			genotypes.push_back(genoPointer);
			depthsPointer = new int[groups.numSamples];
			depths.push_back(depthsPointer);

			for(s=0; s<groups.numSamples; ++s){
				genoPointer[s] = vcfFile.sampleGenotype(allSamples[s]);
				depthsPointer[s] = vcfFile.sampleDepth(allSamples[s]);
				maxDepthPlusOne = std::max(maxDepthPlusOne, depthsPointer[s]);
			}
		}

		//report
		if(limitLines > 0 && counter > limitLines) break;
		if(counter % 10000 == 0)	logfile->list("Read " + toString(counter) + " lines ...");
	}
	maxDepthPlusOne += 1;
	logfile->conclude(toString(numSnps) + " bi-allelic markers read.");
	logfile->endIndent();

	//now create list of depth with data so that we will only need to consider those
	fillDepthIndexes();

	//set that vcf was read
	vcfRead = true;
};

std::string TGenoErrorStorage::getChrPosString(long snp){
	return chromosomeNames[snpChr[snp]] + "\t" + toString(snpPosition[snp]);
}
