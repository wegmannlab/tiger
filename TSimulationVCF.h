/*
 * TSimulationVCF.h
 *
 *  Created on: Feb 28, 2018
 *      Author: phaentu
 */

#ifndef TSIMULATIONVCF_H_
#define TSIMULATIONVCF_H_

#include "TLog.h"
#include <vector>
#include "gzstream.h"

/////////////////////////////////////////////////////////////////////////////////////////
// VCF OUTPUT                                                                          //
/////////////////////////////////////////////////////////////////////////////////////////

class TSimulationVCF{
private:
	std::string filename;
	gz::ogzstream vcfOutFileStream;
	bool vcfOpen;
	long site;
	std::string genotypeStrings[3];
	std::string truePLStrings[3];

	void init();

public:
	TSimulationVCF();
	TSimulationVCF(std::string Filename, TLog* logfile);

	~TSimulationVCF(){
		closeVcf();
	};

	void openVCF(std::string Filename, TLog* logfile);
	void closeVcf();
	void writeHeader(int numSamples);
	void writeHeader(std::vector<std::string> & sampleNames);
	void newSite();
	void writeTrueGenotype(int geno);
	void writeObservedGenotype(int geno, std::string PL, int depth);
	gz::ogzstream& stream(){return vcfOutFileStream;};
};



#endif /* TSIMULATIONVCF_H_ */
