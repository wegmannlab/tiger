/*
 * TPosteriorMean.cpp
 *
 *  Created on: Mar 12, 2018
 *      Author: phaentu
 */

#include "TPosteriorMean.h"

/////////////////////////////////////////////////////////////////////////
// TPosteriorMean                                                      //
/////////////////////////////////////////////////////////////////////////
//----------------------------------------------------------------------
//	TPosteriorMean
//----------------------------------------------------------------------

void TPosteriorMean::init(int Dimension){
	free();

	dimension = Dimension;
	posteriorMean = new double[dimension];
	initialized = true;

	clear();
};

void TPosteriorMean::free(){
	if(initialized){
		delete[] posteriorMean;
	}
};

void TPosteriorMean::clear(){
	for(int d=0; d<dimension; ++d)
		posteriorMean[d] = 0.0;
	numAdded = 0;
};


TPosteriorMean::TPosteriorMean(){
	dimension = 0;
	posteriorMean = NULL;
	initialized = false;
	numAdded = 0;
};

TPosteriorMean::TPosteriorMean(int Dimension){
	initialized = false;
	init(Dimension);
};

TPosteriorMean::~TPosteriorMean(){
	free();
};

void TPosteriorMean::setDimension(int Dimension){
	init(Dimension);
};

void TPosteriorMean::add(double* estimates){
	for(int d=0; d<dimension; ++d)
		posteriorMean[d] = estimates[d];
	++numAdded;
}

double TPosteriorMean::getMean(int index){
	return posteriorMean[index] / (double) numAdded;
};

void TPosteriorMean::setToMean(double* parameter){
	for(int d=0; d<dimension; ++d)
		parameter[d] = posteriorMean[d] / (double) numAdded;
};


//----------------------------------------------------------------------
//	TPosteriorMean2D
//----------------------------------------------------------------------
TPosteriorMean2D::TPosteriorMean2D(){
	numAdded = 0;
	posteriorMean = NULL;
	initialized = false;
	dimension1 = 0;
	dimension2 = NULL;
	dimension2Allocated = false;
};

TPosteriorMean2D::TPosteriorMean2D(int Dimension1, int* Dimension2){
	initialized = false;
	dimension2Allocated = false;
	init(Dimension1, Dimension2);
};

TPosteriorMean2D::TPosteriorMean2D(int Dimension1, int Dimension2){
	initialized = false;
	dimension2Allocated = false;
	init(Dimension1, Dimension2);
};

TPosteriorMean2D::~TPosteriorMean2D(){
	free();
};

void TPosteriorMean2D::init(int Dimension1, int* Dimension2){
	free();

	//set dimension
	dimension1 = Dimension1;
	dimension2 = Dimension2;

	initStorage();
};

void TPosteriorMean2D::init(int Dimension1, int Dimension2){
	free();

	//set dimension
	dimension1 = Dimension1;
	dimension2 = new int[dimension1];
	for(int d=0; d<dimension1; ++d)
		dimension2[d] = Dimension2;
	dimension2Allocated = true;

	initStorage();
};

void TPosteriorMean2D::initStorage(){
	//init storage
	posteriorMean = new double*[dimension1];
	for(int c=0; c<dimension1; ++c)
		posteriorMean[c] = new double[dimension2[c]];

	initialized = true;

	//set to zero
	clear();
}

void TPosteriorMean2D::setDimensions(int Dimension1, int* Dimension2){
	init(Dimension1, Dimension2);
};

void TPosteriorMean2D::setDimensions(int Dimension1, int Dimension2){
	init(Dimension1, Dimension2);
};

void TPosteriorMean2D::free(){
	if(initialized){
		for(int c=0; c<dimension1; ++c)
			delete[] posteriorMean[c];
		delete[] posteriorMean;

		if(dimension2Allocated)
			delete[] dimension2;

		initialized = false;
	}
};

void TPosteriorMean2D::clear(){
	for(int c=0; c<dimension1; ++c){
		for(int d=0; d<dimension2[c]; ++d)
			posteriorMean[c][d] = 0.0;
	}
	numAdded = 0;
};

void TPosteriorMean2D::add(double** estimates){
	if(!initialized)
		throw "Can not add to posterior mean: dimensions not set!";

	for(int c=0; c<dimension1; ++c){
		for(int d=0; d<dimension2[c]; ++d)
			posteriorMean[c][d] += estimates[c][d];
	}
	++numAdded;
};

double TPosteriorMean2D::getMean(int index1, int index2){
	return posteriorMean[index1][index2] / (double) numAdded;
};

void TPosteriorMean2D::setToMean(double** parameter){
	for(int c=0; c<dimension1; ++c){
		for(int d=0; d<dimension2[c]; ++d){
			parameter[c][d] = posteriorMean[c][d] / (double) numAdded;
		}
	}
};


