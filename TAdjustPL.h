/*
 * TAdjustPL.h
 *
 *  Created on: Feb 5, 2018
 *      Author: phaentu
 */

#ifndef TADJUSTPL_H_
#define TADJUSTPL_H_

#include "TLog.h"
#include "TParameters.h"
#include "TVcfFile.h"
#include <map>
#include "TLikelihoodTables.h"
#include "TSampleGroup.h"

class TPLField{
public:
	int genotype;
	int depth;
	std::string PL_string;

	TPLField(int & Genotype, int & Depth, std::string & PL){
		genotype = Genotype;
		depth = Depth;
		PL_string = PL;
	};

	void print(){
		std::cout << genotype << "\t" << depth << "\t" << PL_string << std::endl;
	};
};


class TPLTable{
private:
	std::vector< TPLField > PLs;
	std::vector< TPLField >::iterator it;

	std::string name;
	bool directoryInitialized;
	int maxDepth;
	bool** _PL_exists;
	TPLField*** _PL_pointer;

	void initDirectory();
	void freeDirectory();

public:
	TPLTable(std::string Name){
		name = Name;
		directoryInitialized = false;
		maxDepth = 0;
		_PL_exists = NULL;
		_PL_pointer = NULL;
	};
	~TPLTable(){
		freeDirectory();
	};
	std::string getName(){return name;};
	void addPL(int genotype, int depth, std::string PL);
	bool PL_exists(int & genotype, int & depth);
	std::string updatePL(std::string oldPL, int genotype, int depth);
	std::string getPL(int genotype, int depth);
	void print();
};

class TAdjustPL{
private:
	TLog* logfile;
	TVcfFileSingleLine vcfFile;
	std::string genotypeStrings[3];
	std::vector<TPLTable> PL_Tables;
	std::vector<TPLTable>::iterator plIt;
	TSampleGroups batches;

	int getGenotypeFromString(std::string & genoString);
	void openVCF(TParameters* params);
	void generatePLTablesFromErrorRates(TLikelihoodTable* likTable, std::vector<std::string> paramVector, const int lineNum, const std::string inputFileName, const int model);
	void preparePLTables(TParameters* params);
	void setIteratorToErrorClassCreateIfNotExists(std::string & name);
	void readPLTables(std::string inputFileName);
	void generatePLTablesFromErrorRates(std::string inputFileName, TParameters* params);
	void adjustPL();
	void printPLTables();
	void readBatchAssociationOfSamples(TParameters* myParameters);

public:
	TAdjustPL(TLog* logfile);
	void adjustPL(TParameters* params);
};



#endif /* TADJUSTPL_H_ */
