/*
 * TFastLog.h
 *
 *  Created on: May 14, 2018
 *      Author: wegmannd
 */

#ifndef TFASTLOG_H_
#define TFASTLOG_H_

//a class to quickly calculate the log(x) for x between 0 and 1 using a lookup table
//used for allele frequencies

#include <iostream>

class TFastLog{
private:
	int size;
	double sizeDouble;
	double step;
	double* at;
	double* lookup;
	double* diffToUpper;

public:
	TFastLog(int Size=10000){
		size = Size;
		sizeDouble = size;
		step = 1.0 / (double) size;
		lookup = new double[size+1];
		at = new double[size+1];
		diffToUpper = new double[size+1];

		//fill lookup table
		at[0] = 0.0;
		lookup[0] = log(1.0 / ((double) size+1.0));
		for(int i=1; i<size; ++i){
			at[i] = (double) i / (double) size;
			lookup[i] = log(at[i]);
			diffToUpper[i-1] = lookup[i] - lookup[i-1];
		}
		at[size] = 1.0;
		lookup[size] = 0.0;
		diffToUpper[size-1] = -lookup[size-1];
		diffToUpper[size] = 0.0;
	};

	~TFastLog(){
		delete[] at;
		delete[] lookup;
		delete[] diffToUpper;
	};

	double fastlog(double x){
		//do not attempt for very small x
		if(x < 0.005)
			return log(x);

		//use approximation
		int lower = floor(sizeDouble * x);
		return lookup[lower] + (x - at[lower]) / step * diffToUpper[lower];
	};

	void test(){
		std::cout << "TESTING FAST LOG" << std::endl << "----------------" << std::endl;

		std::ofstream out("fastlog.txt");

		int n = 100000;
		double s = 1.0 / (double) n;
		for(int i=1; i<=n; ++i){
			double x = i * s;
			out << x << "\t" << log(x) << "\t" << fastlog(x) << std::endl;
		}
		out.close();
	};
};



#endif /* TFASTLOG_H_ */
