/*
 * TSimulator.cpp
 *
 *  Created on: Feb 27, 2018
 *      Author: wegmannd
 */


#include "TSimulator.h"


/////////////////////////////////////////////////////////////////////////////////////////
// SIMULATOR                                                                           //
/////////////////////////////////////////////////////////////////////////////////////////

TSimulator::TSimulator(TParameters & Parameters, TLog* Logfile, TRandomGenerator* RandomGenerator){
	parameters = &Parameters;
	randomGenerator = RandomGenerator;
	logfile = Logfile;

	numSamples = 0;
	totNumAlleles = 0;
	numSites = 0;
	likelihoodTables = NULL;
	likelihoodTablesInitialized = false;
};

void TSimulator::prepareSimulations(){
	prepareLikelihoodTables();
	openVCF();
	vcf.writeHeader(sampleNames);
	writeSampleGroupErrorClassFile();
}

void TSimulator::readingCommonParameters(){
	logfile->startIndent("Reading simulation parameters:");
	numSites = parameters->getParameterIntWithDefault("sites", 10000);
	logfile->list("Will simulate " + toString(numSites) + " sites (argument 'sites').");
	outname = parameters->getParameterStringWithDefault("outname", "simulations");
	logfile->list("Will write output files with tag '" + outname + "'.");
	numSamples = parameters->getParameterIntWithDefault("samples", 5);
	logfile->list("Will simulate " + toString(numSamples) + " samples.");
	totNumAlleles = numSamples; //may be modified to account for replicates
};

void TSimulator::prepareLikelihoodTables(){
	//fill likelihood tables
	likelihoodTables = new TLikelihoodTables(parameters, logfile, randomGenerator);
	likelihoodTablesInitialized = true;

	//simulate error rate class for each sample
	likelihoodTables->simulateErrorClassMemberships(totNumAlleles, errorClassOfSample);
};

void TSimulator::openVCF(){
	std::string filename = outname + ".vcf.gz";
	vcf.openVCF(filename, logfile);
};

void TSimulator::writeSampleGroupErrorClassFile(){
	std::string filename = outname + "_sampleGroups.txt";
	logfile->listFlush("Writing groups and error classes of samples to file '" + filename + "' ...");

	//open file
	std::ofstream out(filename.c_str());
	if(!out)
		throw "Failed to open file '" + filename + "' for writing!";

	//write header
	out << "Sample\tGroup\tBatch\n";

	//write samples
	std::vector<std::string>::iterator nIt = sampleNames.begin();
	std::vector<int>::iterator gIt = groupOfSample.begin();
	std::vector<int>::iterator eIt = errorClassOfSample.begin();
	for(; nIt != sampleNames.end(); ++nIt, ++gIt, ++eIt){
		out << *nIt;
		out << "\tGroup_" << *gIt+1;
		out << "\t" << likelihoodTables->getClassName(*eIt) << "\n";
	}

	//close file
	out.close();
	logfile->done();
};

void TSimulator::clean(){
	if(likelihoodTablesInitialized){
		delete likelihoodTables;
		likelihoodTablesInitialized = false;
	}

	sampleNames.clear();
	errorClassOfSample.clear();
	groupOfSample.clear();
	vcf.closeVcf();
};

void TSimulator::runSimulations(std::string model){
	if(model =="indReps")
		simulateSampleReplicates();
	else if(model == "hardyWeinberg")
		simulateHardyWeinberg();
	else if(model == "popRep")
		simulatePopulationReplicates();
	else if(model == "truthSet")
		simulateTruthSetData();
	else
		throw "Simulation model '" + model + "' does not exist! Use either 'indRep', 'hardyWeinberg', or 'truthSet'.";
};


//**************************************************************************************
// Sample Replicates
//**************************************************************************************
void TSimulator::simulateSampleReplicates(){
	logfile->startIndent("Simulating data of sample replicates:");

	//read parameters
	readingCommonParameters();
	int numReplicates = parameters->getParameterIntWithDefault("replicates", 3);
	logfile->list("Will simulate " + toString(numReplicates) + " replicates per samples.");
	double het = parameters->getParameterDoubleWithDefault("het", 0.1);
	logfile->list("Will simulate a fraction " + toString(het) + " of heterozygous sites.");
	logfile->endIndent();

	//create sample names and add samples to groups
	int r,s;
	for(s=0; s<numSamples; ++s){
		for(r=0; r<numReplicates; ++r){
			sampleNames.push_back("Sample_" + toString(s) + "_" + toString(r));
			groupOfSample.push_back(s);
		}
	}

	//prepare simulations
	//Note: total number of samples is numSamples * numReplicates!
	totNumAlleles = numSamples * numReplicates;
	prepareSimulations();

	//loop over sites
	logfile->listFlush("Conducting simulations ...");
	int trueGenotype;
	int sampleIndex;
	for(long i=0; i<numSites; ++i){
		//write site info
		vcf.newSite();

		//loop over samples
		sampleIndex = 0;
		for(s=0; s<numSamples; ++s){
			//simulate genotype
			if(randomGenerator->getRand() < het)
				trueGenotype = 1;
			else trueGenotype = round(randomGenerator->getRand()) * 2;

			//now simulate data with different depths and print
			for(r=0; r<numReplicates; ++r){
				likelihoodTables->simulateAndWriteGenotype(trueGenotype, errorClassOfSample[sampleIndex], vcf);
				++sampleIndex;
			}
		}
	}

	//clean up
	clean();
	logfile->done();
}

//**************************************************************************************
// Hardy-Weinberg
//**************************************************************************************
void TSimulator::simulateHardyWeinberg(){
	logfile->startIndent("Simulating data under Hardy-Weinberg:");

	//read parameters
	readingCommonParameters();
	int numPops = parameters->getParameterIntWithDefault("populations", 3);
	logfile->list("Will simulate " + toString(numPops) + " populations with " + toString(numSamples) + " samples each.");
	double alpha_alleleFreqDist = parameters->getParameterDoubleWithDefault("alpha", 0.5);
	double beta_alleleFreqDist = parameters->getParameterDoubleWithDefault("beta", 0.5);
	logfile->list("Will simulate population allele frequencies distributed as Beta(" + toString(alpha_alleleFreqDist) + ", " + toString(beta_alleleFreqDist) + ").");
	double inbreedingF = parameters->getParameterDoubleWithDefault("F", 0.0);
	if(inbreedingF != 0.0){
		logfile->list("Will assuming inbreeding with F = " + toString(inbreedingF) + ".");
		if(inbreedingF < 0.0 || inbreedingF > 1.0)
			throw "F must be between 0 and 1!";
	}
	bool numSitesIsPolymorphic = parameters->parameterExists("numSitesPolymorphic");
	int minMAF = 0;
	if(numSitesIsPolymorphic){
		logfile->list("Will simulate until " + toString(numSites) + " sites are observed polymorphic.");
		minMAF = parameters->getParameterIntWithDefault("minMAF", 1);
		logfile->list("Sites are considered polymorphic with MAF >= " + toString(minMAF));
	}
	logfile->endIndent();

	//create sample names and add samples to groups
	int p,s;
	for(p=0; p<numPops; ++p){
		for(s=0; s<numSamples; ++s){
			sampleNames.push_back("Sample_" + toString(p) + "_" + toString(s));
			groupOfSample.push_back(p);
		}
	}

	//prepare simulations
	//Note: total number of samples is numSamples * numPops!
	totNumAlleles = 2 * numSamples * numPops;
	prepareSimulations();

	//open file to write allele frequencies to
	std::string filename = outname + "_trueAlleleFrequencies.txt";
	logfile->list("Will write true allele frequencies to file '" + filename + "'.");
	std::ofstream genoFreqOut(filename.c_str());
	if(!genoFreqOut) throw "Failed to open file '" + filename + "' for writing!";
	//write header
	//write header
	genoFreqOut << "locus";
	for(p=0; p<numPops; ++p)
		genoFreqOut << "\tPop" << p;
	genoFreqOut << "\n";

	//open R input file
	filename = outname + "_R_input.txt";
	std::ofstream R_input(filename.c_str());

	//loop over sites
	logfile->listFlush("Conducting simulations ...");
	int trueGenotype;
	int sampleIndex;
	int sumGenotypes;
	double cumulGenoProb[3];
	cumulGenoProb[2] = 1.0;
	long numHetGenotypes = 0;
	long numPolymorphicSites = 0;
	long i = 0;
	for(; numPolymorphicSites < numSites; ++i){
		//write site info
		vcf.newSite();
		genoFreqOut << i+1;

		R_input << i+1;

		//loop over population
		sampleIndex = 0;
		sumGenotypes = 0;
		for(p=0; p<numPops; ++p){
			//pick allele frequency
			double f = randomGenerator->getBetaRandom(alpha_alleleFreqDist, beta_alleleFreqDist);
			genoFreqOut << "\t" << f;

			//calculate Hardy-Weinberg proportions
			cumulGenoProb[0] = (1.0 - inbreedingF) * (1.0 - f) * (1.0 - f) + inbreedingF * (1.0 - f);
			cumulGenoProb[1] = (1.0 - inbreedingF) * 2.0 * f * (1.0 - f);
			cumulGenoProb[1] += cumulGenoProb[0];

			//loop over samples
			for(s=0; s<numSamples; ++s){
				//simulate genotype
				trueGenotype = randomGenerator->pickOne(3, cumulGenoProb);
				if(trueGenotype == 1) ++numHetGenotypes;

				//now simulate data with different depths and print
				sumGenotypes += likelihoodTables->simulateAndWriteGenotype(trueGenotype, errorClassOfSample[sampleIndex], vcf, R_input);

				++sampleIndex;
			}
		}

		//do we restrict to polymorphic?
		if(!numSitesIsPolymorphic || (sumGenotypes >= minMAF && sumGenotypes <= (totNumAlleles-minMAF)))
			++numPolymorphicSites;

		genoFreqOut << "\n";
		R_input << "\n";
	}

	//clean up
	genoFreqOut.close();
	R_input.close();
	clean();
	logfile->done();
	double percentHet = round((double) numHetGenotypes / (double) (numSamples*numPops*numSites) * 1000.0) / 10.0;
	logfile->conclude("Simulated " + toString(percentHet) + "% heterozygous genotypes.");
}

//**************************************************************************************
// Population Replicates
//**************************************************************************************
void TSimulator::simulatePopulationReplicates(){
	logfile->startIndent("Simulating data of population replicates:");

	//read parameters
	readingCommonParameters();
	int numPops = parameters->getParameterIntWithDefault("populations", 3);
	logfile->list("Will simulate " + toString(numPops) + " populations with " + toString(numSamples) + " samples each.");
	std::vector<double> dirchiletAlpha;
	parameters->fillParameterIntoVectorWithDefault("alpha", dirchiletAlpha, ',', "0.5,0.2,0.1");
	logfile->list("Will simulate population allele frequencies distributed as Dirichlet(" + concatenateString(dirchiletAlpha, ", ") + ").");
	if(dirchiletAlpha.size() != 3)
		throw "Wring number of alphas! Need 3 instead of " + toString(dirchiletAlpha.size()) + "!";
	logfile->endIndent();

	//create sample names and add samples to groups
	int p,s;
	for(p=0; p<numPops; ++p){
		for(s=0; s<numSamples; ++s){
			sampleNames.push_back("Sample_" + toString(p) + "_" + toString(s));
			groupOfSample.push_back(p);
		}
	}

	//prepare simulations
	//Note: total number of samples is numSamples * numPops!
	totNumAlleles = numSamples * numPops;
	prepareSimulations();

	//open file to write allele frequencies to
	std::string filename = outname + "_trueGenotypeFrequencies.txt";
	logfile->list("Will write true genotype frequencies to file '" + filename + "'.");
	std::ofstream genoFreqOut(filename.c_str());
	if(!genoFreqOut) throw "Failed to open file '" + filename + "' for writing!";
	//write header
	//write header
	genoFreqOut << "locus";
	for(p=0; p<numPops; ++p){
		for(int g=0; g<2; ++g)
			genoFreqOut << "\tPop" << p << "_" << g;
	}
	genoFreqOut << "\n";

	//loop over sites
	logfile->listFlush("Conducting simulations ...");
	int trueGenotype;
	int sampleIndex;
	std::vector<double> genoFreq(3);
	std::vector<double> genoFreqCumul(3);
	for(long i=0; i<numSites; ++i){
		//write site info
		vcf.newSite();
		genoFreqOut << i+1;

		//loop over population
		sampleIndex = 0;
		for(p=0; p<numPops; ++p){
			//pick allele frequency
			randomGenerator->fillDirichlet(dirchiletAlpha, genoFreq);
			std::partial_sum(genoFreq.begin(), genoFreq.end(), genoFreqCumul.begin());
			genoFreqOut << "\t" << genoFreq[0] << "\t" << genoFreq[1] << "\t" << genoFreq[2];

			//loop over samples
			for(s=0; s<numSamples; ++s){
				//simulate genotype
				trueGenotype = randomGenerator->pickOne(genoFreqCumul);

				//now simulate data with different depths and print
				likelihoodTables->simulateAndWriteGenotype(trueGenotype, errorClassOfSample[sampleIndex], vcf);
				++sampleIndex;
			}
		}

		genoFreqOut << "\n";
	}

	//clean up
	genoFreqOut.close();
	clean();
	logfile->done();
}

//**************************************************************************************
// Truth Set
//**************************************************************************************
int TSimulator::simulateTrueGenotype(const int trueGenotype, const int meanDepth, const double seqError){
	//pick depth
	double depth = randomGenerator->getPoissonRandom(meanDepth);

	//simulate number of reference reads
	int numRef = 0; // binomial random variable
	if (trueGenotype == 0)
		numRef = randomGenerator->getBiomialRand(1.0 - seqError, depth);
	else if (trueGenotype == 1)
		numRef = randomGenerator->getBiomialRand(0.5, depth);
	else
		numRef = randomGenerator->getBiomialRand(seqError, depth);

	//set num alternative alleles
	int numAlt = depth - numRef;

	//calculate genotype likelihoods and identify MLE genotype
	//ref/ref
	double maxLL = pow(1 - seqError, numRef) * pow(seqError, numAlt);
	int MLE_genotype = 0;

	//ref/alt
	double LL = pow(0.5, numRef + numAlt);
	if(LL > maxLL){
		maxLL = LL;
		MLE_genotype = 1;
	}

	//alt/alt
	LL = pow(seqError, numRef) * pow(1.0 - seqError, numAlt);
	if(LL > maxLL){
		maxLL = LL;
		MLE_genotype = 2;
	}

	//return MLE genotype
	return MLE_genotype;
};

void TSimulator::simulateTruthSetData(){
	logfile->startIndent("Simulating data with truth set:");

	//read parameters
	readingCommonParameters();
	double het = parameters->getParameterDoubleWithDefault("het", 0.1);
	logfile->list("Will simulate a fraction " + toString(het) + " of heterozygous sites.");
	double meanDepthOfTrue = 0.0;
	double seqError = 0.0;
	if(parameters->parameterExists("meanDepthOfTrue")){
		meanDepthOfTrue = parameters->getParameterDouble("meanDepthOfTrue");
		if(meanDepthOfTrue < 0.1)
			throw "meanDepthOfTrue must be >= 0.1!";
		seqError = parameters->getParameterDoubleWithDefault("seqError", 0.01);
		logfile->list("Will simulate 'true' genotypes with mean depth " + toString(meanDepthOfTrue) + " and sequencing error rate " + toString(seqError) + ".");
	} else {
		logfile->list("Will assume true genotypes are known.");
	}
	logfile->endIndent();

	//open file to write sample pairs
	std::string filename = outname + "_samplePairs.txt";
	logfile->listFlush("Writing sample pairs to file '" + filename + "' ...");
	std::ofstream samplePairOut(filename.c_str());
	if(!samplePairOut) throw "Failed to open file '" + filename + "' for writing!";
	samplePairOut << "observed\ttrue\n";

	//create sample names and add samples to a single group
	int s;
	for(s=0; s<numSamples; ++s){
		sampleNames.push_back("Sample_" + toString(s) + "_obs");
		sampleNames.push_back("Sample_" + toString(s) + "_true");
		samplePairOut << "Sample_" << s << "_obs\tSample_" << s <<"_true\n";

		//add obs and true sample to same group. Note: will never be used.
		groupOfSample.push_back(s);
		groupOfSample.push_back(s);
	}
	samplePairOut.close();
	logfile->done();

	//prepare simulations
	//Note: total number of samples is numSamples * 2!
	totNumAlleles = numSamples * 2;
	prepareSimulations();

	//loop over sites
	int trueGenotype;

	logfile->listFlush("Conducting simulations ...");
	for(long i=0; i<numSites; ++i){
		//write site info
		vcf.newSite();

		//loop over samples
		for(s=0; s<numSamples; ++s){
			//simulate genotype
			if(randomGenerator->getRand() < het)
				trueGenotype = 1;
			else trueGenotype = round(randomGenerator->getRand()) * 2;

			//now simulate data with errors and print
			likelihoodTables->simulateAndWriteGenotype(trueGenotype, errorClassOfSample[2*s], vcf);

			//estimate true genotype from sequencing data (if requested)
			if(meanDepthOfTrue > 0.0){
				trueGenotype = simulateTrueGenotype(trueGenotype, meanDepthOfTrue, seqError);
			}

			//and write true genotype for this sample
			vcf.writeTrueGenotype(trueGenotype);
		}
	}

	//clean up
	clean();
	logfile->done();
}

