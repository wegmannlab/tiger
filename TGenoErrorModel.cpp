/*
 * TgenoErrorModel.cpp
 *
 *  Created on: Feb 26, 2018
 *      Author: wegmannd
 */



#include "TGenoErrorModel.h"

//----------------------------------------------------
// TGenoErrorReplicatesEmParams
//----------------------------------------------------
TGenoErrorEmParams::TGenoErrorEmParams(TParameters* myParameters, TLog* logfile){
	logfile->startIndent("Reading EM parameters:");
	minDelta = myParameters->getParameterDoubleWithDefault("minDelta", 0.000001);
	logfile->list("Will stop EM algorithm if change in parameter estimates < " + toString(minDelta) + ".");
	maxNumEmIterations = myParameters->getParameterIntWithDefault("numIter", 100000);
	logfile->list("Will run EM for at max " + toString(maxNumEmIterations) + " iterations.");
	minEpsIter = myParameters->getParameterIntWithDefault("minEpsIter", 100);
	maxEpsIter = myParameters->getParameterIntWithDefault("maxEpsIter", 1000);
	logfile->list("Will optimize epsilon numerically for at least" + toString(minEpsIter) + " but no more than " + toString(maxEpsIter) + " iterations.");
	logfile->endIndent();
};

TGenoErrorMcmcParams::TGenoErrorMcmcParams(TParameters* myParameters, TLog* logfile){
	logfile->startIndent("Reading MCMC parameters:");
	numIterations = myParameters->getParameterIntWithDefault("numIter", 100000);
	logfile->list("Will run MCMC for " + toString(numIterations) + " iterations.");
	burninLength = myParameters->getParameterIntWithDefault("burnin", 5000);
	numBurnins = myParameters->getParameterIntWithDefault("numBurnins", 5);
	logfile->list("Will run " + toString(numBurnins) + " burnin(s) of " + toString(burninLength) + " iterations each.");
	totNumIterations = numIterations + numBurnins*burninLength;

	//priors
	epsLambda = myParameters->getParameterDoubleWithDefault("epsLambda", 5);
	logfile->list("Will put an exponential prior on error rates with lambda = " + toString(epsLambda) + ".");

	//printing
	thinning = myParameters->getParameterIntWithDefault("thinning", 10);
	if(thinning == 1)
		logfile->list("Will print every iteration.");
	else if(thinning == 2)
		logfile->list("Will print every second iteration.");
	else if(thinning == 3){
		logfile->list("Will print every third iteration.");
	} else
		logfile->list("Will print every " + toString(thinning) + "th iteration.");

	logfile->endIndent();
};

/////////////////////////////////////////////////////////////////////////
// ERROR MODELS                                                        //
/////////////////////////////////////////////////////////////////////////

//----------------------------------------------------
// TGenoErrorReplicatesOneErrorModel
//----------------------------------------------------
TGenoErrorOneErrorModel::TGenoErrorOneErrorModel(){
	emissionProbsInitialized = false;
	errorRateStorageInitialized = false;
	tmpOptimizationStorageInitialized = false;
	MCMCStorageInitialized = false;
	epsilon = NULL;
	newEpsilon = NULL;
	emissionProbs = NULL;
	curQ = NULL;
	oldQ = NULL;
	initialQ = NULL;
	step = NULL;
	errorHastingsTermsNew = NULL;
	errorHastingsTermsOld = NULL;

	proposalWidthEpsilon = NULL;
	acceptanceRatesEpsilon = NULL;

	numSnps = 0;
	numGroups = 0;
	numErrorRateClasses = 0;
	numDepthWithData = NULL;

	likelihoodModel = NULL;
};

TGenoErrorOneErrorModel::~TGenoErrorOneErrorModel(){
	freeEmissionProbabilityMatrix();
	freeErrorRateStorage();
	freeTmpOptimizationStorageForEM();
	freeMCMCStorage();
};

void TGenoErrorOneErrorModel::initializeEmissionProbabilityMatrix(){
	//error rates epsilon and emissions
	emissionProbs = new double***[numErrorRateClasses];
	for(int c=0; c<numErrorRateClasses; ++c){
		emissionProbs[c] = new double**[numDepthWithData[c]];
		for(int d = 0; d<numDepthWithData[c]; ++d){
			emissionProbs[c][d] = new double*[3];
			emissionProbs[c][d][0] = new double[4]; //also missing!
			emissionProbs[c][d][1] = new double[4]; //also missing!
			emissionProbs[c][d][2] = new double[4]; //also missing!

			for(int j=0; j<4; ++j){
				emissionProbs[c][d][0][j] = 1.0;
				emissionProbs[c][d][1][j] = 1.0;
				emissionProbs[c][d][2][j] = 1.0;
			}
		}
	}
	emissionProbsInitialized = true;
}

void TGenoErrorOneErrorModel::freeEmissionProbabilityMatrix(){
	if(emissionProbsInitialized){
		for(int c=0; c<numErrorRateClasses; ++c){
			for(int d = 0; d<numDepthWithData[c]; ++d){
				delete[] emissionProbs[c][d][0];
				delete[] emissionProbs[c][d][1];
				delete[] emissionProbs[c][d][2];
				delete[] emissionProbs[c][d];
			}
			delete[] emissionProbs[c];
		}
		delete[] emissionProbs;
	}
	emissionProbsInitialized = false;
}

void TGenoErrorOneErrorModel::initializeErrorRateStorage(){
	//error rates epsilon and emissions
	epsilon = new double*[numErrorRateClasses];
	newEpsilon = new double*[numErrorRateClasses];
	for(int c=0; c<numErrorRateClasses; ++c){
		epsilon[c] = new double[numDepthWithData[c]];
		newEpsilon[c] = new double[numDepthWithData[c]];
	}

	errorRateStorageInitialized = true;
}

void TGenoErrorOneErrorModel::freeErrorRateStorage(){
	if(errorRateStorageInitialized){
		for(int c=0; c<numErrorRateClasses; ++c){
			delete[] epsilon[c];
			delete[] newEpsilon[c];
		}
		delete[] epsilon;
		delete[] newEpsilon;

		errorRateStorageInitialized = false;
	}
};

void TGenoErrorOneErrorModel::initializeTmpOptimizationStorageForEM(){
	curQ = new double*[numErrorRateClasses];
	oldQ = new double*[numErrorRateClasses];
	initialQ = new double*[numErrorRateClasses];
	step = new double*[numErrorRateClasses];
	for(int c=0; c<numErrorRateClasses; ++c){
		curQ[c] = new double[numDepthWithData[c]];
		oldQ[c] = new double[numDepthWithData[c]];
		initialQ[c] = new double[numDepthWithData[c]];
		step[c] = new double[numDepthWithData[c]];
	}

	tmpOptimizationStorageInitialized = true;
};

void TGenoErrorOneErrorModel::freeTmpOptimizationStorageForEM(){
	if(tmpOptimizationStorageInitialized){
		for(int c=0; c<numErrorRateClasses; ++c){
			delete[] curQ[c];
			delete[] oldQ[c];
			delete[] initialQ[c];
			delete[] step[c];
		}
		delete[] curQ;
		delete[] oldQ;
		delete[] initialQ;
		delete[] step;

		tmpOptimizationStorageInitialized = false;
	}
};

void TGenoErrorOneErrorModel::initializeMCMCStorage(){
	errorHastingsTermsNew = new double*[numErrorRateClasses];
	errorHastingsTermsOld = new double*[numErrorRateClasses];
	acceptanceRatesEpsilon = new long*[numErrorRateClasses];
	proposalWidthEpsilon = new double*[numErrorRateClasses];

	for(int c=0; c<numErrorRateClasses; ++c){
		errorHastingsTermsNew[c] = new double[numDepthWithData[c]];
		errorHastingsTermsOld[c] = new double[numDepthWithData[c]];
		acceptanceRatesEpsilon[c] = new long[numDepthWithData[c]];
		proposalWidthEpsilon[c] = new double[numDepthWithData[c]];
	}

	MCMCStorageInitialized = true;
}

void TGenoErrorOneErrorModel::freeMCMCStorage(){
	if(MCMCStorageInitialized){
		for(int c=0; c<numErrorRateClasses; ++c){
			delete[] errorHastingsTermsNew[c];
			delete[] errorHastingsTermsOld[c];
			delete[] acceptanceRatesEpsilon[c];
			delete[] proposalWidthEpsilon[c];
		}

		delete[] errorHastingsTermsNew;
		delete[] errorHastingsTermsOld;
		delete[] acceptanceRatesEpsilon;
		delete[] proposalWidthEpsilon;

		MCMCStorageInitialized = false;
	}
};

void TGenoErrorOneErrorModel::initEstimation(TGenoErrorStorage & Data, TLikelihoodModel* LikelihoodModel, TLog* logfile){
	//create likelihood model
	likelihoodModel = LikelihoodModel;

	//get size
	numGroups = Data.getNumGroups();
	numDepthWithData = Data.numDepthsWithData;

	numSnps = Data.getNumSnps();
	numErrorRateClasses = Data.getNumErrorClasses();

	//initialize storage
	initializeEmissionProbabilityMatrix();
	initializeErrorRateStorage();
	estimateInitialParameters(Data, logfile);
}

void TGenoErrorOneErrorModel::estimateErrorRatesMLE(TGenoErrorStorage & Data, TLikelihoodModel* LikelihoodModel, TGenoErrorEmParams & EM_params, TLog* logfile){
	initEstimation(Data, LikelihoodModel, logfile);

	//runEM
	initializeTmpOptimizationStorageForEM();
	runEM(Data, EM_params, logfile);
}

void TGenoErrorOneErrorModel::estimateInitialParameters(TGenoErrorStorage & Data, TLog* logfile){
	logfile->listFlush("Estimating initial parameters ...");

	//frequencies
	likelihoodModel->estimateInitialParameters(Data);

	//epsilon: use same for all depth
	double eps = likelihoodModel->estimateInitialErrorRates(Data);
	setInitialEpsilon(eps);

	//report
	logfile->done();
	logfile->conclude("Initial error rate was set to ", eps);
}

void TGenoErrorOneErrorModel::setInitialEpsilon(const double & eps){
	for(int c=0; c<numErrorRateClasses; ++c){
		for(int d = 1; d<numDepthWithData[c]; ++d){
			epsilon[c][d] = eps;
		}
	}
	fillEmissionProbabilitiesCurErrorRates();
}

void TGenoErrorOneErrorModel::fillEmissionProbabilitiesCurErrorRates(){
	fillEmissionProbabilities(epsilon);
};

void TGenoErrorOneErrorModel::fillEmissionProbabilitiesNewErrorRates(){
	fillEmissionProbabilities(newEpsilon);
};

void TGenoErrorOneErrorModel::fillEmissionProbabilities(double** thisEpsilon){
	double** em;
	//em[i][j] = P(g=j|gamma=i)
	double Eps;
	double oneMinusEps;

	for(int c=0; c<numErrorRateClasses; ++c){
		//fill depth 0
		for(int j=0; j<4; ++j){
			emissionProbs[c][0][0][j] = 1.0;
			emissionProbs[c][0][1][j] = 1.0;
			emissionProbs[c][0][2][j] = 1.0;
		}

		for(int d=1; d<numDepthWithData[c]; ++d){
			em = emissionProbs[c][d];

			Eps = thisEpsilon[c][d];
			oneMinusEps = 1.0 - Eps;

			em[0][0] = oneMinusEps * oneMinusEps;
			em[0][1] = 2.0 * Eps * oneMinusEps;
			em[0][2] = Eps * Eps;
			em[0][3] = 1.0; //missing

			em[1][0] = Eps * oneMinusEps;
			em[1][1] = Eps * Eps + oneMinusEps * oneMinusEps;
			em[1][2] = Eps * oneMinusEps;
			em[1][3] = 1.0; //missing

			em[2][0] = Eps * Eps;
			em[2][1] = 2.0 * Eps * oneMinusEps;
			em[2][2] = oneMinusEps * oneMinusEps;
			em[2][3] = 1.0; //missing
		}
	}
};

void TGenoErrorOneErrorModel::makeEmissionProbsInLog(){
	for(int c=0; c<numErrorRateClasses; ++c){
		for(int d=0; d<numDepthWithData[c]; ++d){
			for(int i=0; i<4; ++i){
				emissionProbs[c][d][0][i] = log10(emissionProbs[c][d][0][i]);
				emissionProbs[c][d][1][i] = log10(emissionProbs[c][d][1][i]);
				emissionProbs[c][d][2][i] = log10(emissionProbs[c][d][2][i]);
			}
		}
	}
}

double TGenoErrorOneErrorModel::calculateLLCurErrorRates(TGenoErrorStorage & Data){
	//initialize emission probabilities
	fillEmissionProbabilitiesCurErrorRates();

	return likelihoodModel->calculateLL(Data, emissionProbs);
};

void TGenoErrorOneErrorModel::calculateQ_epsilon(TGenoErrorStorage & Data, double** Q, double** thisEpsilon){
	//initialize emission probabilities
	fillEmissionProbabilities(thisEpsilon);
	makeEmissionProbsInLog();

	//set all Q = 0
	for(int c=0; c<numErrorRateClasses; ++c){
		for(int d = 1; d<numDepthWithData[c]; ++d){
			Q[c][d] = 0.0;
		}
	}

	likelihoodModel->fillQ(Q, Data, emissionProbs);
};

void TGenoErrorOneErrorModel::initEpsilonOptimization(double** thisEpsilon, double** thisNewEpsilon){
	//set starting epsilon at old epsilon
	//init steps
	//store initial Q
	for(int c = 0; c<numErrorRateClasses; ++c){
		for( int d = 1; d<numDepthWithData[c]; ++d){
			thisNewEpsilon[c][d] = thisEpsilon[c][d];
			step[c][d] = 0.1;
			initialQ[c][d] = oldQ[c][d];
		}
	}
}

void TGenoErrorOneErrorModel::proposeNewEpsilonForOptimization(double** thisEpsilon){
	//update all epsilon
	for(int c = 0; c < numErrorRateClasses; ++c){
		for(int d = 1; d < numDepthWithData[c]; ++d){
			thisEpsilon[c][d] = exp(log(thisEpsilon[c][d]) + step[c][d]);
			//std::cout << std::setprecision(20) << d << ") new eps = " << newEpsilon[d] << std::endl;
			if(thisEpsilon[c][d] > 0.9999999999)
				thisEpsilon[c][d] = 0.9999999999;
			else if(thisEpsilon[c][d] < 0.0000000001)
				thisEpsilon[c][d] = 0.0000000001;
		}
	}
}

bool TGenoErrorOneErrorModel::adjustStepSizeAndCheckImprovement(){
	//adjust step & check it we break
	bool allQImproved = true;
	for(int c = 0; c < numErrorRateClasses; ++c){
		for(int d = 1; d < numDepthWithData[c]; ++d){
			if(curQ[c][d] < oldQ[c][d])
				step[c][d] = -step[c][d] / exp(1.0);

			//check if OK
			if(curQ[c][d] < initialQ[c][d])
				allQImproved = false;

			//save cur as old
			oldQ[c][d] = curQ[c][d];
		}
	}
	return allQImproved;
}

void TGenoErrorOneErrorModel::estimateEpsilonNumerically(TGenoErrorStorage & Data, int minNumIter, int maxNumIter){
	//estimate all epsilon using numerical optimization
	//do not estimate epsilon for depth=0!

	//calculate initial Q of current epsilons before optimization
	calculateQ_epsilon(Data, oldQ, epsilon);

	//init values
	initEpsilonOptimization(epsilon, newEpsilon);

	//now loop until convergence
	bool allQImproved;
	int iter = 0;
	for(; iter<maxNumIter; ++iter){
		//update all epsilon
		proposeNewEpsilonForOptimization(newEpsilon);

		//calculate new Q
		calculateQ_epsilon(Data, curQ, newEpsilon);

		//adjust step & check it we break
		allQImproved = adjustStepSizeAndCheckImprovement();

		//do we break?
		if(allQImproved && iter > minNumIter)
			break;
	}
}

void TGenoErrorOneErrorModel::saveEstimatesToOld(){
	double** tmpEpsilon = epsilon;
	epsilon = newEpsilon;
	newEpsilon = tmpEpsilon;
}

void TGenoErrorOneErrorModel::runEM(TGenoErrorStorage & Data, TGenoErrorEmParams & EM_params, TLog* logfile){
	//initialize storage & estimate initial parameters
	logfile->startIndent("Running EM algorithm:");

	//temporary variables
	double LL;
	double delta;
	double oldLL = calculateLLCurErrorRates(Data);
	logfile->list("Initial LL = " + toString(oldLL, 12));

	//---------------------------
	//run EM algo
	//---------------------------
	for(int i=0; i<EM_params.maxNumEmIterations; ++i){
		//fill emission probabilities
		fillEmissionProbabilitiesCurErrorRates();

		likelihoodModel->calculateWeights(Data, emissionProbs);
		likelihoodModel->estimateFrequencies(Data);

		//estimate epsilon by numerically optimizing Q
		estimateEpsilonNumerically(Data, EM_params.minEpsIter, EM_params.maxEpsIter);

		//save estimates
		saveEstimatesToOld();

		//calc LL
		LL = calculateLLCurErrorRates(Data);
		delta = LL - oldLL;
		oldLL = LL;

		//report
		logfile->list("EM iteration " + toString(i+1) + ": LL = " + toString(LL, 12) + ", deltaLL = " + toString(delta));

		//check if converged
		if(delta < EM_params.minDelta){
			logfile->conclude("EM converged!");
			break;
		}
	}

	logfile->endIndent();
}


//-----------------------------
// MCMC
//-----------------------------
void TGenoErrorOneErrorModel::initPosteriorMeanStorage(){
	posteriorMeanEpsilon.setDimensions(numErrorRateClasses, numDepthWithData);
};

void TGenoErrorOneErrorModel::addCurEstimatesToPosteriorMeanStorage(){
	posteriorMeanEpsilon.add(epsilon);
};

void TGenoErrorOneErrorModel::setEpsilonToPosteriorMean(){
	posteriorMeanEpsilon.setToMean(epsilon);
	fillEmissionProbabilitiesCurErrorRates();
};

void TGenoErrorOneErrorModel::estimateErrorRatesBayesian(TGenoErrorStorage & Data, TLikelihoodModel* LikelihoodModel, TGenoErrorMcmcParams & MCMC_params, std::string & outname, TRandomGenerator* randomGenerator, TLog* logfile){
	initEstimation(Data, LikelihoodModel, logfile);

	initPosteriorMeanStorage();

	//run MCMC
	initializeMCMCStorage();
	runMCMC(Data, MCMC_params, outname, randomGenerator, logfile);
};


void TGenoErrorOneErrorModel::setProposalWidthErrorRates(double** thisEpsilon, double** thisProposalWidth, long** thisAcceptanceRate){
	for(int c=0; c<numErrorRateClasses; ++c){
		for(int d = 1; d<numDepthWithData[c]; ++d){
			thisProposalWidth[c][d] = thisEpsilon[c][d];
			thisAcceptanceRate[c][d] = 0;
		}
	}
};

void TGenoErrorOneErrorModel::updateProposalWidthErrorRates(double** thisProposalWidth, long** thisAcceptanceRate, long len){
	for(int c=0; c<numErrorRateClasses; ++c){
		for(int d = 1; d<numDepthWithData[c]; ++d){
			thisProposalWidth[c][d] *= (double) thisAcceptanceRate[c][d] / (double) len * 3.0;
			if(thisProposalWidth[c][d] > 0.2)
				thisProposalWidth[c][d] = 0.2;
			thisAcceptanceRate[c][d] = 0;
		}
	}
};


void TGenoErrorOneErrorModel::setInitialProposalWidthErrorRates(){
	setProposalWidthErrorRates(epsilon, proposalWidthEpsilon, acceptanceRatesEpsilon);
};

double TGenoErrorOneErrorModel::calculateAcceptanceRateErrorRates(long len){
	double rate = 0.0;

	for(int c=0; c<numErrorRateClasses; ++c){
		for(int d = 1; d<numDepthWithData[c]; ++d){
			rate += acceptanceRatesEpsilon[c][d];
		}
	}

	return rate / (double) len;
};

void TGenoErrorOneErrorModel::proposeNewErrorRatesMCMC(double** thisEpsilon, double** thisNewEpsilon, double** proposalWidth, TRandomGenerator & randomGenerator){
	//propose new error rate
	for(int c=0; c<numErrorRateClasses; ++c){
		for(int d = 1; d<numDepthWithData[c]; ++d){
			thisNewEpsilon[c][d] = fabs(thisEpsilon[c][d] + (randomGenerator.getRand() -0.5) * proposalWidth[c][d]);

			if(thisNewEpsilon[c][d] > 0.5)
				thisNewEpsilon[c][d] = 0.5 - thisNewEpsilon[c][d];

			if(thisNewEpsilon[c][d] == 0.0) thisNewEpsilon[c][d] = 1E-10;
		}
	}
};

void TGenoErrorOneErrorModel::acceptOrRejectErrorRatesMCMC(double** thisEpsilon, double** thisNewEpsilon, double & priorLambda, long** thisAcceptanceRate, TRandomGenerator & randomGenerator){
	//now accept or reject for each class / depth
	for(int c=0; c<numErrorRateClasses; ++c){
		for(int d = 1; d<numDepthWithData[c]; ++d){
			double logHastings = errorHastingsTermsNew[c][d] - errorHastingsTermsOld[c][d];
			logHastings -= priorLambda * (thisNewEpsilon[c][d] - thisEpsilon[c][d]);

			if(log(randomGenerator.getRand()) < logHastings){
				thisEpsilon[c][d] = thisNewEpsilon[c][d];
				++thisAcceptanceRate[c][d];
			}
		}
	}
};

void TGenoErrorOneErrorModel::updateErrorRatesMCMC(TGenoErrorStorage & Data, TGenoErrorMcmcParams & MCMC_params, TRandomGenerator & randomGenerator){
	//calc old terms with current emission probs
	likelihoodModel->calculateLogHastingsTermErrorUpdate(errorHastingsTermsOld, Data, emissionProbs);

	//propose new error rates and update emission probs
	proposeNewErrorRatesMCMC(epsilon, newEpsilon, proposalWidthEpsilon, randomGenerator);
	fillEmissionProbabilitiesNewErrorRates();

	//calculate hasting terms with new emission probs
	likelihoodModel->calculateLogHastingsTermErrorUpdate(errorHastingsTermsNew, Data, emissionProbs);

	//accept reject or reject
	acceptOrRejectErrorRatesMCMC(epsilon, newEpsilon, MCMC_params.epsLambda, acceptanceRatesEpsilon, randomGenerator);
	fillEmissionProbabilitiesCurErrorRates();
}

int TGenoErrorOneErrorModel::writeEpsilonHeaderToMCMCTraceFile(gz::ogzstream & out){
	int numEpsilon = 0;
	for(int c=0; c<numErrorRateClasses; ++c){
		numEpsilon += numDepthWithData[c]; //move to class that assembles estimates
		for(int d=0; d<numDepthWithData[c]; ++d)
			out << "\teps_" << c << "_" << d;
	}
	return numEpsilon;
};

void TGenoErrorOneErrorModel::writeCurEpsilonEstimatesToTraceFile(gz::ogzstream & out){
	for(int c=0; c<numErrorRateClasses; ++c){
		for(int d=0; d<numDepthWithData[c]; ++d)
		out << "\t" << epsilon[c][d];
	}
};

double TGenoErrorOneErrorModel::calcCurPosteriorDensity(TGenoErrorStorage & Data, TGenoErrorMcmcParams & MCMC_params, TRandomGenerator* randomGenerator){
	//first prior of current error rate
	double postProb = 0.0;
	for(int c=0; c<numErrorRateClasses; ++c){
		for(int d=0; d<numDepthWithData[c]; ++d)
			postProb -= MCMC_params.epsLambda * epsilon[c][d];
	}

	//add from likelihood model
	return postProb + likelihoodModel->calculatePosteriorProb(Data, emissionProbs, *randomGenerator);
};

void TGenoErrorOneErrorModel::adjustAfterBurnin(TGenoErrorMcmcParams & MCMC_params){
	//adjust proposal width epsilon
	updateProposalWidthErrorRates(proposalWidthEpsilon, acceptanceRatesEpsilon, MCMC_params.burninLength);

	//also in likelihood model
	likelihoodModel->adjustAfterBurnin();
};

void TGenoErrorOneErrorModel::writeAcceptanceRateErrorRates(const int & length, TLog* logfile){
	logfile->conclude("Acceptance rate error rates was " + toString(calculateAcceptanceRateErrorRates(length),3) + ".");
};

std::string TGenoErrorOneErrorModel::getNameOfMCMCTraceFile(std::string & outname){
	return outname + "_oneError_MCMC_chain.txt.gz";
};

void TGenoErrorOneErrorModel::runMCMC(TGenoErrorStorage & Data, TGenoErrorMcmcParams & MCMC_params, std::string & outname, TRandomGenerator* randomGenerator, TLog* logfile){
	//initialize storage & estimate initial parameters
	logfile->startIndent("Running MCMC algorithm:");

	//temporary variables
	double postProb = calcCurPosteriorDensity(Data, MCMC_params, randomGenerator);
	logfile->list("Initial log posterior density = " + toString(postProb, 12));

	//open output file and write header
	std::string filename = getNameOfMCMCTraceFile(outname);
	gz::ogzstream out(filename.c_str());
	out << "postDens";
	writeEpsilonHeaderToMCMCTraceFile(out);
	likelihoodModel->writeHeaderHierarchicalParameters(out);
	out << "\n";

	//set epsilon proposal width
	setInitialProposalWidthErrorRates();

	//---------------------------
	//run Burnin(s)
	//---------------------------
	for(int b=0; b<MCMC_params.numBurnins; ++b){
		std::string reportString = "Running a burnin of " + toString(MCMC_params.burninLength) + " iterations (";
		logfile->listFlush(reportString + "0%) ...");
		int oldProg=0;
		for(long i=0; i<MCMC_params.burninLength; ++i){
			updateErrorRatesMCMC(Data, MCMC_params, *randomGenerator);

			//update model specific parameters (e.g. allele frequencies)
			likelihoodModel->updateModelSpecificParametersMCMC(Data, emissionProbs, *randomGenerator);

			//report
			int prog = floor((float) i / (float) MCMC_params.burninLength * 100);
			if(prog > oldProg){
				oldProg = prog;
				logfile->listOverFlush(reportString + toString(prog) + "%) ...");
			}
		}
		logfile->overList("Running a burnin of " + toString(MCMC_params.burninLength) + " iterations ... done!  ");
		writeAcceptanceRateErrorRates(MCMC_params.burninLength, logfile);
		likelihoodModel->writeAcceptanceRateModelSpecificParameters(logfile);

		//adjust things after burnin
		logfile->listFlush("Adjusting proposal widths ...");
		adjustAfterBurnin(MCMC_params);
		logfile->done();
	}

	//---------------------------
	//run MCMC algorithm
	//---------------------------
	std::string reportString = "Running an MCMC with " + toString(MCMC_params.numIterations) + " iterations (";
	logfile->listFlush(reportString + "0%) ...");
	int oldProg=0;
	for(long i=0; i<MCMC_params.numIterations; ++i){
		//update error rates(s)
		updateErrorRatesMCMC(Data, MCMC_params, *randomGenerator);

		//update model specific parameters (e.g. allele frequencies)
		likelihoodModel->updateModelSpecificParametersMCMC(Data, emissionProbs, *randomGenerator);

		//write cur estimates to file
		if(i % MCMC_params.thinning == 0){
			//add to posterior mean
			addCurEstimatesToPosteriorMeanStorage();
			likelihoodModel->addCurEstimatesToPosteriorMeanStorage();

			//print trace
			postProb = calcCurPosteriorDensity(Data, MCMC_params, randomGenerator);
			out << postProb;
			writeCurEpsilonEstimatesToTraceFile(out);
			likelihoodModel->writeCurHierarchicalParameters(out);
			out << "\n";
		}

		//report
		int prog = floor((float) i / (float) MCMC_params.totNumIterations * 1000);
		if(prog > oldProg){
			oldProg = prog;
			logfile->listOverFlush(reportString + toString(prog/10.0) + "%) ...");
		}
	}

	//report at end
	logfile->overList("Running an MCMC with " + toString(MCMC_params.numIterations) + " iterations ... done!  ");
	writeAcceptanceRateErrorRates(MCMC_params.numIterations, logfile);
	likelihoodModel->writeAcceptanceRateModelSpecificParameters(logfile);

	//-----------------------------------------
	// Saving posterior means as estimates
	//-----------------------------------------
	setEpsilonToPosteriorMean();

	logfile->endIndent();
}


void TGenoErrorOneErrorModel::posteriorSurface(TGenoErrorStorage & Data, std::string frequencyFile, std::string & outname, TRandomGenerator* randomGenerator, TLog* logfile){
	//initialize storage & estimate initial parameters
	logfile->startIndent("Calculating likelihood surface:");

	THardyWeinbergModel hardyWeinbergModel(Data);

	initEstimation(Data, &hardyWeinbergModel, logfile);

	hardyWeinbergModel.readFrequenciesfromFile(frequencyFile, logfile);

	//open output file and write header
	std::string filename = outname + "_posteriorSurface.txt";
	std::ofstream out(filename.c_str());
	out << "epsilon/alpha";
	hardyWeinbergModel.writePosteriorSurfaceHeader(out);

	//prepare steps
	int numEps = 250;
	double step = 0.2 / (numEps - 1.0);

	//calculate surface
	std::string reportString = "Calculating surface for " + toString(numEps) + " error rates ";
	logfile->listFlush(reportString + "(0%) ...");
	int prog, oldProg=0;
	for(long i=0; i<numEps; ++i){
		//set new error rates
		setInitialEpsilon(i*step);
		out << i*step;

		//double logPostProb = -5.0 * i * step; //asuming lambda = 5!
		double logPostProb = 0.0;


		//calculate surface
		hardyWeinbergModel.writePosteriorSurface(out, Data, emissionProbs, logPostProb, *randomGenerator);

		//report
		prog = floor((float) i / (float) numEps * 1000);
		if(prog > oldProg){
			oldProg = prog;
			logfile->listOverFlush(reportString + "(" + toString(prog/10.0) + "%) ...");
		}
	}
	logfile->overWrite(reportString + "done!  ");

	//close file
	out.close();

	logfile->endIndent();
};



////////////////////////////////////////////////
// TGenoErrorReplicatesTwoErrorModel
////////////////////////////////////////////////
TGenoErrorTwoErrorModel::TGenoErrorTwoErrorModel():TGenoErrorOneErrorModel(){
	epsilonHet = NULL;
	newEpsilonHet = NULL;
	errorRateStorageInitializedHet = false;

	acceptanceRatesEpsilonHet = NULL;
	proposalWidthEpsilonHet = NULL;
	MCMCStorageHetInitialized = false;
};

TGenoErrorTwoErrorModel::~TGenoErrorTwoErrorModel(){
	freeErrorRateStorage();
	freeMCMCStorage();
};

void TGenoErrorTwoErrorModel::setEpsilonToPosteriorMean(){
	posteriorMeanEpsilon.setToMean(epsilon);
	posteriorMeanEpsilonHet.setToMean(epsilonHet);
	fillEmissionProbabilitiesCurErrorRates();
};

void TGenoErrorTwoErrorModel::initializeErrorRateStorage(){
	//epsilon
	TGenoErrorOneErrorModel::initializeErrorRateStorage();

	//epsilonHet
	epsilonHet = new double*[numErrorRateClasses];
	newEpsilonHet = new double*[numErrorRateClasses];
	for(int c=0; c<numErrorRateClasses; ++c){
		epsilonHet[c] = new double[numDepthWithData[c]];
		newEpsilonHet[c] = new double[numDepthWithData[c]];
	}

	errorRateStorageInitializedHet = true;
};

void TGenoErrorTwoErrorModel::freeErrorRateStorage(){
	//epsilon
	TGenoErrorOneErrorModel::freeErrorRateStorage();

	//epsilonHet
	if(errorRateStorageInitializedHet){
		for(int c=0; c<numErrorRateClasses; ++c){
			delete[] epsilonHet[c];
			delete[] newEpsilonHet[c];
		}
		delete[] epsilonHet;
		delete[] newEpsilonHet;
	}

	errorRateStorageInitializedHet = false;
};


void TGenoErrorTwoErrorModel::initializeMCMCStorage(){

	TGenoErrorOneErrorModel::initializeMCMCStorage();

	acceptanceRatesEpsilonHet = new long*[numErrorRateClasses];
	proposalWidthEpsilonHet = new double*[numErrorRateClasses];

	for(int c=0; c<numErrorRateClasses; ++c){
		acceptanceRatesEpsilonHet[c] = new long[numDepthWithData[c]];
		proposalWidthEpsilonHet[c] = new double[numDepthWithData[c]];
	}

	MCMCStorageHetInitialized = true;
}

void TGenoErrorTwoErrorModel::freeMCMCStorage(){
	TGenoErrorOneErrorModel::freeMCMCStorage();

	if(MCMCStorageHetInitialized){
		for(int c=0; c<numErrorRateClasses; ++c){
			delete[] acceptanceRatesEpsilonHet[c];
			delete[] proposalWidthEpsilonHet[c];
		}

		delete[] acceptanceRatesEpsilonHet;
		delete[] proposalWidthEpsilonHet;

		MCMCStorageHetInitialized = false;
	}
};


void TGenoErrorTwoErrorModel::setInitialEpsilon(const double & eps){
	//epsilonHet
	for(int c = 0; c<numErrorRateClasses; ++c){
		for(int d = 1; d<numDepthWithData[c]; ++d){
			epsilonHet[c][d] = eps;
		}
	}

	//epsilon
	TGenoErrorOneErrorModel::setInitialEpsilon(eps);
};

void TGenoErrorTwoErrorModel::fillEmissionProbabilitiesCurErrorRates(){
	fillEmissionProbabilities(epsilon, epsilonHet);
};

void TGenoErrorTwoErrorModel::fillEmissionProbabilitiesNewErrorRates(){
	fillEmissionProbabilities(newEpsilon, epsilonHet);
};

void TGenoErrorTwoErrorModel::fillEmissionProbabilities(double** thisEpsilon, double** thisEpsilonHet){
	double** em;
	//em[i][j] = P(g=j|gamma=i)
	double Eps;
	double oneMinusEps;

	for(int c=0; c<numErrorRateClasses; ++c){
		for(int d=0; d<numDepthWithData[c]; ++d){
			em = emissionProbs[c][d];

			//homozygous
			Eps = thisEpsilon[c][d];
			oneMinusEps = 1.0 - Eps;

			em[0][0] = oneMinusEps * oneMinusEps;
			em[0][1] = 2.0 * Eps * oneMinusEps;
			em[0][2] = Eps * Eps;
			em[0][3] = 1.0; //missing

			em[2][0] = Eps * Eps;
			em[2][1] = 2.0 * Eps * oneMinusEps;
			em[2][2] = oneMinusEps * oneMinusEps;
			em[2][3] = 1.0; //missing

			//heterozygous
			Eps = thisEpsilonHet[c][d];
			oneMinusEps = 1.0 - Eps;

			em[1][0] = Eps * oneMinusEps;
			em[1][1] = Eps * Eps + oneMinusEps * oneMinusEps;
			em[1][2] = Eps * oneMinusEps;
			em[1][3] = 1.0; //missing
		}
	}
};

void TGenoErrorTwoErrorModel::calculateQ_epsilon(TGenoErrorStorage & Data, double** Q, double** thisEpsilon){
	//initialize emission probabilities
	fillEmissionProbabilities(thisEpsilon, epsilonHet);
	makeEmissionProbsInLog();

	//set all Q = 0
	for(int c = 0; c<numErrorRateClasses; ++c){
		for(int d = 1; d<numDepthWithData[c]; ++d){
			Q[c][d] = 0.0;
		}
	}

	likelihoodModel->fillQHomoOnly(Q, Data, emissionProbs);
};

void TGenoErrorTwoErrorModel::calculateQ_epsilonHet(TGenoErrorStorage & Data, double** Q, double** thisEpsilonHet){
	//initialize emission probabilities
	fillEmissionProbabilities(epsilon, thisEpsilonHet);
	makeEmissionProbsInLog();

	//set all Q = 0
	for(int c=0; c<numErrorRateClasses; ++c){
		for(int d = 1; d<numDepthWithData[c]; ++d){
			Q[c][d] = 0.0;
		}
	}

	likelihoodModel->fillQHetOnly(Q, Data, emissionProbs);
};


void TGenoErrorTwoErrorModel::estimateEpsilonNumerically(TGenoErrorStorage & Data, int minNumIter, int maxNumIter){
	//estimate all epsilon using numerical optimization
	//do not estimate epsilon for depth=0!

	//first estimate epsilon using base class method
	TGenoErrorOneErrorModel::estimateEpsilonNumerically(Data, minNumIter, maxNumIter);

	//calculate initial Q of current epsilons before optimization
	calculateQ_epsilonHet(Data, oldQ, epsilon);

	//init values
	initEpsilonOptimization(epsilonHet, newEpsilonHet);

	//now loop until convergence
	bool allQImproved;
	for(int iter=0; iter<maxNumIter; ++iter){
		//update all epsilon
		proposeNewEpsilonForOptimization(newEpsilonHet);

		//calculate new Q
		calculateQ_epsilonHet(Data, curQ, newEpsilonHet);

		//adjust step & check it we break
		allQImproved = adjustStepSizeAndCheckImprovement();

		//do we break?
		if(allQImproved && iter > minNumIter)
			break;
	}
}


void TGenoErrorTwoErrorModel::saveEstimatesToOld(){
	//freq and epsilon
	TGenoErrorOneErrorModel::saveEstimatesToOld();

	//epsilonHet
	double** tmpEpsilonHet = epsilonHet;
	epsilonHet = newEpsilonHet;
	newEpsilonHet = tmpEpsilonHet;
}


//-----------------------------
// MCMC
//-----------------------------
void TGenoErrorTwoErrorModel::initPosteriorMeanStorage(){
	posteriorMeanEpsilon.setDimensions(numErrorRateClasses, numDepthWithData);
	posteriorMeanEpsilonHet.setDimensions(numErrorRateClasses, numDepthWithData);
};

void TGenoErrorTwoErrorModel::addCurEstimatesToPosteriorMeanStorage(){
	posteriorMeanEpsilon.add(epsilon);
	posteriorMeanEpsilonHet.add(epsilonHet);
};

void TGenoErrorTwoErrorModel::setInitialProposalWidthErrorRates(){
	TGenoErrorOneErrorModel::setInitialProposalWidthErrorRates();
	setProposalWidthErrorRates(epsilonHet, proposalWidthEpsilonHet, acceptanceRatesEpsilonHet);
}

void TGenoErrorTwoErrorModel::updateErrorRatesMCMC(TGenoErrorStorage & Data, TGenoErrorMcmcParams & MCMC_params, TRandomGenerator & randomGenerator){
	//first update epsilon, but keep epsilonHet constant
	TGenoErrorOneErrorModel::updateErrorRatesMCMC(Data, MCMC_params, randomGenerator);

	//second, update epsilon het the same way.
	likelihoodModel->calculateLogHastingsTermErrorUpdate(errorHastingsTermsOld, Data, emissionProbs);

	//propose new error rates het and update emission probs
	proposeNewErrorRatesMCMC(epsilonHet, newEpsilonHet, proposalWidthEpsilonHet, randomGenerator);
	fillEmissionProbabilities(epsilon, newEpsilonHet);

	//calculate hasting terms with new emission probs
	likelihoodModel->calculateLogHastingsTermErrorUpdate(errorHastingsTermsNew, Data, emissionProbs);

	//accept reject or reject
	acceptOrRejectErrorRatesMCMC(epsilonHet, newEpsilonHet, MCMC_params.epsLambda, acceptanceRatesEpsilonHet, randomGenerator);
	fillEmissionProbabilitiesCurErrorRates();
}

void TGenoErrorTwoErrorModel::adjustAfterBurnin(TGenoErrorMcmcParams & MCMC_params){
    TGenoErrorOneErrorModel::adjustAfterBurnin(MCMC_params);

	//adjust proposal width epsilonHet
	updateProposalWidthErrorRates(proposalWidthEpsilonHet, acceptanceRatesEpsilonHet, MCMC_params.burninLength);
};

double TGenoErrorTwoErrorModel::calculateAcceptanceRateErrorRatesHet(long len){
	double rate = 0.0;

	for(int c=0; c<numErrorRateClasses; ++c){
		for(int d = 1; d<numDepthWithData[c]; ++d){
			rate += acceptanceRatesEpsilonHet[c][d];
		}
	}

	return rate / (double) len;
};


void TGenoErrorTwoErrorModel::writeAcceptanceRateErrorRates(const int & length, TLog* logfile){
	logfile->conclude("Acceptance rate error rates was " + toString(calculateAcceptanceRateErrorRates(length),3) + ".");
	logfile->conclude("Acceptance rate error rates het was " + toString(calculateAcceptanceRateErrorRatesHet(length),3) + ".");
};

std::string TGenoErrorTwoErrorModel::getNameOfMCMCTraceFile(std::string & outname){
	return outname + "_twoError_MCMC_chain.txt.gz";
};

int TGenoErrorTwoErrorModel::writeEpsilonHeaderToMCMCTraceFile(std::ofstream & out){
	int numEpsilon = 0;
	for(int c=0; c<numErrorRateClasses; ++c){
		numEpsilon += 2*numDepthWithData[c]; //move to class that assembles estimates
		for(int d=0; d<numDepthWithData[c]; ++d){
			out << "\teps_" << c << "_" << d;
			out << "\tepsHet_" << c << "_" << d;
		}
	}
	return numEpsilon;
};

void TGenoErrorTwoErrorModel::writeCurEpsilonEstimatesToTraceFile(std::ofstream & out){
	for(int c=0; c<numErrorRateClasses; ++c){
		for(int d=0; d<numDepthWithData[c]; ++d){
			out << "\t" << epsilon[c][d];
			out << "\t" << epsilonHet[c][d];
		}
	}
}



