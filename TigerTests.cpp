/*
 * TTigerTests.cpp
 *
 *  Created on: Feb 16, 2018
 *      Author: phaentu
 */


#include "TigerTests.h"



//------------------------------------------
//TTigerTest_truthSetInference
//------------------------------------------
TTigerTest_truthSetInference::TTigerTest_truthSetInference(TParameters & params, TLog* logfile):TTest(params, logfile){
	_name = "truthSetTes";
	parseParameters(params, logfile, _name);
}

TTigerTest_truthSetInference::TTigerTest_truthSetInference(TParameters & params, TLog* logfile, std::string testTag):TTest(params, logfile){
	_name = "truthSetTes";
	parseParameters(params, logfile, testTag);
}

void TTigerTest_truthSetInference::parseParameters(TParameters & params, TLog* logfile, std::string testTag){
	errorTolerance = params.getParameterIntWithDefault(testTag + "tolerance", 0.1);

	//read parameters for simulations
	numSamples = params.getParameterIntWithDefault(testTag + "_samples", 5);
	numSites = params.getParameterIntWithDefault(testTag + "_sites", 50000);
	het = params.getParameterIntWithDefault(testTag + "_het", 0.25);

	error = params.getParameterStringWithDefault(testTag + "_error", "0.2,0.1,0.05,0.01");
	fillVectorFromString(error, errorVec, ',');
	errorHet = params.getParameterStringWithDefault(testTag + "_het", "0.01,0.05,0.1,0.2");
	fillVectorFromString(errorHet, errorHetVec, ',');
	outname = params.getParameterStringWithDefault(testTag + "_outname", testTag);
}


void TTigerTest_truthSetInference::prepareSimulationParameters(){
	_testParams.clear();
	_testParams.addParameter("outname", outname);
	_testParams.addParameter("samples", toString(numSamples));
	_testParams.addParameter("sites", toString(numSites));
	_testParams.addParameter("het", toString(het));
	_testParams.addParameter("error", error);
	_testParams.addParameter("errorHet", errorHet);
}

bool TTigerTest_truthSetInference::run(){

	//1) Simulate replicates
	//-----------------------------
	prepareSimulationParameters();

	if(!runFromInputfile("simulateTruthSet"))
		return false;

	logfile->newLine();

	//2) Run inference
	//-----------------------------
	_testParams.clear();
	_testParams.addParameter("vcf", outname + ".vcf.gz");
	_testParams.addParameter("samplePairs", "");
	_testParams.addParameter("estimateError");

	if(!runFromInputfile("compareCalls"))
		return false;


	//3) check if results are OK
	//--------------------------
	return checkErrorRateEstimates(outname + "_genotypeErrorRates.txt");
};

bool TTigerTest_truthSetInference::checkErrorRateEstimates(std::string filename){
	logfile->startIndent("Checking error rate:");

	//open file with estimates file
	logfile->listFlush("Reading estimates from file '" + filename + "' ...");
	std::ifstream in(filename.c_str());
	if(!in){
		logfile->conclude("Failed to open output file '" + filename + "'!");
		return false;
	}
	logfile->done();

	//skip header
	std::string tmp;
	getline(in, tmp);
	std::vector<std::string> tmpVec;

	//compare depths class by depth class
	std::vector<double>::iterator it = errorVec.begin();
	std::vector<double>::iterator itHet = errorHetVec.begin();

	for(int i=0; i<errorVec.size(); ++i, ++it, ++itHet){
		//read estimates
		if(in.eof()){
			logfile->newLine();
			logfile->conclude("Missing estimates in file '" + filename + "'!");
			return false;
		}
		getline(in, tmp);
		fillVectorFromStringWhiteSpaceSkipEmpty(tmp, tmpVec);
		if(tmpVec.size() != 5){
			logfile->newLine();
			logfile->conclude("Wrong number of columns on line " + toString(i+1) + " in file '" + filename + "'!");
			return false;
		}

		//compare homozygous estimates
		if(fabs(stringToDouble(tmpVec[3]) - *it) / *it > errorTolerance){
			logfile->newLine();
			logfile->conclude("Relative error on homozygous error rate > " + toString(errorTolerance) + " (estimated " + tmpVec[3] + " instead of " + toString(*it) + ")!");
			return false;
		}

		//compare heterozygous estimates
		if(fabs(stringToDouble(tmpVec[4]) - *itHet) / *itHet > errorTolerance){
			logfile->newLine();
			logfile->conclude("Relative error on heterozygous error rate > " + toString(errorTolerance) + " (estimated " + tmpVec[4] + " instead of " + toString(*itHet) + ")!");
			return false;
		}
	}
	logfile->done();

	return true;
}

//------------------------------------------
//TTigerTest_replicateInference
//------------------------------------------
TTigerTest_replicateInference::TTigerTest_replicateInference(TParameters & params, TLog* logfile):TTigerTest_truthSetInference(params, logfile, "replicateInferenceTest"){
	_name = "replicateInferenceTest";

	numReplicates = params.getParameterIntWithDefault(_name + "_samples", 5);
}

bool TTigerTest_replicateInference::run(){

	//1) Simulate replicates
	//-----------------------------
	prepareSimulationParameters();
	_testParams.addParameter("replicates", toString(numReplicates));

	if(!runFromInputfile("simulateReplicates"))
		return false;

	logfile->newLine();

	//2) Run inference
	//-----------------------------
	_testParams.clear();
	_testParams.addParameter("vcf", outname + ".vcf.gz");

	//add group string
	int r,s;
	std::string sampleGroupString = "";
	for(s=0; s<numSamples; ++s){
		if(s>0) sampleGroupString += ',';
		sampleGroupString += "[";
		for(r=0; r<numReplicates; ++r){
			if(r>0) sampleGroupString += ',';
			sampleGroupString += "Sample_" + toString(s) + "_" + toString(r);
		}
		sampleGroupString += ']';
	}
	_testParams.addParameter("groups", sampleGroupString);

	if(!runFromInputfile("inferFromReplicates"))
		return false;


	//3) check if results are OK
	//--------------------------
	return checkErrorRateEstimates(outname + "_genotypeErrorRates.txt");
};

