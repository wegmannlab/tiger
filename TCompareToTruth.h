/*
 * TAnnotator.h
 *
 *  Created on: Jun 15, 2011
 *      Author: wegmannd
 */

#ifndef TCOMPARETOTRUTH_H_
#define TCOMPARETOTRUTH_H_

#include "stringFunctions.h"
#include <vector>
#include "TParameters.h"
#include <iostream>
#include <fstream>
#include "TVcfFile.h"
#include "TSampleGroup.h"
#include <math.h>
#include <stdlib.h>

//#include "TSampleGroup.h"
#include "TLog.h"
#include "TFile.h"
#include "TLikelihoodTables.h"
#include "TRandomGenerator.h"

//-------------------------------------------------
//TGenoComparisonTable
//-------------------------------------------------
class TGenoComparisonTable{
private:
	double** counts; //[true][observed]
	std::string* genoString;

	double calcDerivativeOfLogLikelihood(const double & eps, const double & a, const double & b, const double & c);
	double maximizeLikelihood(double initialEps, const double & a, const double & b, const double & c);

public:
	TGenoComparisonTable();
	~TGenoComparisonTable(){
		for(int g=0; g<4; ++g)
			delete[] counts[g];
		delete[] counts;
		delete[] genoString;
	};

	void clear();
	long size(bool includeMissing=false);
	void simulate(long n, double eps, double het, TRandomGenerator* randomGenerator);
	void add(const int & trueGeno, const int & observedGeno);
	void add(const int & trueGeno, const int & observedGeno, const double value);
	void write(const std::string filename);
	double estimateErrorRate();
	double estimateErrorRateHomozygousSites();
	double estimateErrorRateHeterozygousSites();
	std::string getNewPL(const int & genotype);
};


//-------------------------------------------------
//TDepthBins
//-------------------------------------------------
class TDepthBins{
private:
	TGenoComparisonTable* depthTables;
	int numdepthTables;
	std::map<int, int> rangesOfDepthTables;
	std::map<int, int>::iterator mapIt;
	int* depthToBinMap; //Mapping depth to bins
	int maxDepthConsideredPlusOne;
	bool initialized;

	void fillDepthToBinMap();

public:
	TDepthBins();
	~TDepthBins(){
		if(initialized){
			delete[] depthTables;
			delete[] depthToBinMap;
		}
	};
	void createBins(int maxDepth);
	void createBins(std::vector<std::string> & binVec);
	void createBins(TDepthBins & other);
	void reportBins(TLog* logfile);
	void copyBins(std::map<int, int> & rangeMap);
	void add(const int & depth, const int & trueGeno, const int & observedGeno);
	void write(const std::string & tag);
	void estimateErrorRates(std::ofstream & out, std::string & className, TLog* logfile);
	void printNewPLTable(std::ofstream & out, const std::string & className, const std::string* genotypeString);
};

//-------------------------------------------------
//TSampleMap
//-------------------------------------------------
class TSamplePairContainer{
public:
	int observedSampleVcfIndex;
	int trueSampleVcfIndex;
	std::string observedName;
	std::string trueName;
	int errorRateClass;

	TSamplePairContainer(int ObservedSampleVcfIndex, int TrueSampleVcfIndex, std::string ObservedName, std::string TrueName){
		observedSampleVcfIndex = ObservedSampleVcfIndex;
		trueSampleVcfIndex = TrueSampleVcfIndex;
		observedName = ObservedName;
		trueName = TrueName;
		errorRateClass = -1;
	};
};

class TSampleMap{
private:
	bool initialized;
	std::vector<TSamplePairContainer> sampleMap;
	std::vector<std::string> errorClassNames;
	std::vector<TSamplePairContainer>::iterator it;

	void readSampleMapFromFile(TParameters* myParameters, TVcfFileSingleLine & vcfFile, TLog* logfile);
	void readErrorClasses(TParameters* myParameters, TVcfFileSingleLine & vcfFile, TLog* logfile);

public:

	TSampleMap();
	TSampleMap(TParameters* myParameters, TVcfFileSingleLine & vcfFile, TLog* logfile);

	void initialize(TParameters* myParameters, TVcfFileSingleLine & vcfFile, TLog* logfile);
	int size(){ return sampleMap.size(); };
	int numErrorClasses(){ return errorClassNames.size(); };
	std::string& getErrorClassName(int index){ return errorClassNames[index]; };
	void begin();
	void next();
	bool end();
	TSamplePairContainer& cur(){ return *it; };
};


//-------------------------------------------------
//TCompareToTruth
//-------------------------------------------------
class TCompareToTruth{
private:
	TParameters* myParameters;
	TLog* logfile;
	TVcfFileSingleLine vcfFile;
	std::ifstream vcfFileStream;
	std::ofstream vcfOutFilestream;

	TSampleMap sampleMap;
	TGenoComparisonTable* individualTables;
	TDepthBins* depthBins;
	bool tablesInitialized;

	void prepareVcfInput();
	void createComparisonTables();
	void freeComparisonTables();
	void readDataFromVCF();
	void estimateErrorRates(std::string filename);
	void estimatePerIndividualErrorRates(std::string filename);
	void printComaprisonTables(std::string & outname);
	void printNewPLTables(std::string filename);
	void fillReportedTruthConfusionMatrix(double** matrix, int depth, double seqError, TRandomGenerator* randomGenerator);

public:

	TCompareToTruth(TParameters & Params, TLog* Logfile);
	~TCompareToTruth(){};

	//void filterForMinimumDepth();
	void compareCallsIndividuals();
	void assessErrorsInTruthSet(TRandomGenerator* randomGenerator);
};




#endif /* TCOMPARETOTRUTH_H_ */

