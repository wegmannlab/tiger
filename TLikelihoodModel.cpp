/*
 * TLikelihoodModel.cpp
 *
 *  Created on: Mar 1, 2018
 *      Author: phaentu
 */

#include "TLikelihoodModel.h"



//----------------------------------------------------
//LikelihoodModel
//----------------------------------------------------
TLikelihoodModel::TLikelihoodModel(){
	numSnps = 0;
	numSamples = 0;
	numGroups = 0;
	weights = NULL;
	weightsInitialized = false;

	testNum = 123456;
};

//--------------------------------------------------------
//TIndividualReplicatesModel
//--------------------------------------------------------
TIndividualReplicatesModel::TIndividualReplicatesModel(TGenoErrorStorage & Data):TLikelihoodModel(){
	initializeWeights(Data);
	initializeFrequencies();
}

void TIndividualReplicatesModel::initializeFrequencies(){
	//frequencies
	genotypeFrequencies = new double*[numGroups];
	newGenotypeFrequencies = new double*[numGroups];
	for(int G=0; G<numGroups; ++G){
		genotypeFrequencies[G] = new double[4]; //missing genotype = 3
		newGenotypeFrequencies[G] = new double[4];
	}

	frequenciesInitialized = true;
}

void TIndividualReplicatesModel::freeFrequencies(){
	if(frequenciesInitialized){
		for(int G=0; G<numGroups; ++G){
			delete[] genotypeFrequencies[G];
			delete[] newGenotypeFrequencies[G];
		}
		delete[] genotypeFrequencies;
		delete[] newGenotypeFrequencies;
	}

	frequenciesInitialized = false;
};

void TIndividualReplicatesModel::initializeWeights(TGenoErrorStorage & Data){
	numGroups = Data.numGroups;
	numSnps = Data.numSnps;

	//weights
	weights = new double**[numSnps];
	long l; int G;
	for(l=0; l<numSnps; ++l){
		weights[l] = new double*[numGroups];
		for(G=0; G<numGroups; ++G)
			weights[l][G] = new double[3];
	}

	weightsInitialized = true;
};

void TIndividualReplicatesModel::freeWeights(){
	if(weightsInitialized){
		for(long l=0; l<numSnps; ++l){
			for(int G=0; G<numGroups; ++G)
				delete[] weights[l][G];
			delete[] weights[l];
		}
		delete[] weights;
	}

	weightsInitialized = false;
}

void TIndividualReplicatesModel::estimateInitialParameters(TGenoErrorStorage & Data){
	//estimate initial genotype frequencies and error rates (independent of depths)
	for(int G=0; G<numGroups; ++G){
		genotypeFrequencies[G][0] = 0.0;
		genotypeFrequencies[G][1] = 0.0;
		genotypeFrequencies[G][2] = 0.0;
		genotypeFrequencies[G][3] = 1.0;
	}

	int s;
	for(std::vector<short*>::iterator itG = Data.genotypes.begin(); itG != Data.genotypes.end(); ++itG){
		for(s=0; s<Data.numSamples; ++s){
			++genotypeFrequencies[Data.groupOfSample[s]][(*itG)[s]];
		}
	}

	//normalize estimates
	double sum;
	for(int G=0; G<Data.numGroups; ++G){
		sum = genotypeFrequencies[G][0] + genotypeFrequencies[G][1] + genotypeFrequencies[G][2] + genotypeFrequencies[G][3];
		genotypeFrequencies[G][0] /= sum;
		genotypeFrequencies[G][1] /= sum;
		genotypeFrequencies[G][2] /= sum;
		genotypeFrequencies[G][3] = 1.0; //missing genotype has always probability 1.0
	}
};

double TIndividualReplicatesModel::estimateInitialErrorRates(TGenoErrorStorage & Data){
	//count mismatched between replicates
	int tmp = 0;
	double eps = 0.0;
	double numcomparisons = 0.0;
	int oldGroup; int s;
	for(std::vector<short*>::iterator itG = Data.genotypes.begin(); itG != Data.genotypes.end(); ++itG){
		oldGroup = -1;
		for(s=0; s<Data.numSamples; ++s){
			//compare genotypes within groups
			if(Data.groupOfSample[s] != oldGroup){
				tmp = (*itG)[s];
				oldGroup = Data.groupOfSample[s];
			} else {
				eps += abs(tmp - (*itG)[s]);
				++numcomparisons;
			}
		}
	}
	eps = (eps + 1) / (double) (4.0 * numcomparisons + 1);

	return eps;
};

double TIndividualReplicatesModel::calculateLL(TGenoErrorStorage & Data, double**** emissionProbs){
	//variables
	std::vector<short*>::iterator itG = Data.genotypes.begin();
	std::vector<int*>::iterator itD = Data.depths.begin();
	int s; int G;
	double LL = 0.0;
	double** tmp = new double*[numGroups];
	for(G=0; G<numGroups; ++G){
		tmp[G] = new double[3];
	}

	//Now run across all genotypes
	for(; itG != Data.genotypes.end(); ++itG, ++itD){
		for(G=0; G<numGroups; ++G){
			tmp[G][0] = 1.0; tmp[G][1] = 1.0; tmp[G][2] = 1.0;
		}
		for(s=0; s<Data.numSamples; ++s){
			tmp[Data.groupOfSample[s]][0] *= emissionProbs[Data.batchOfSample[s]][(*itD)[s]][0][(*itG)[s]];
			tmp[Data.groupOfSample[s]][1] *= emissionProbs[Data.batchOfSample[s]][(*itD)[s]][1][(*itG)[s]];
			tmp[Data.groupOfSample[s]][2] *= emissionProbs[Data.batchOfSample[s]][(*itD)[s]][2][(*itG)[s]];
		}
		for(G=0; G<Data.numGroups; ++G){
			LL += log(tmp[G][0] * genotypeFrequencies[G][0] + tmp[G][1] * genotypeFrequencies[G][1] + tmp[G][2] * genotypeFrequencies[G][2]);
		}
	}

	//clean up
	for(G=0; G<Data.numGroups; ++G)
		delete[] tmp[G];
	delete[] tmp;
	return LL;
}

void TIndividualReplicatesModel::calculateWeights(TGenoErrorStorage & Data, double**** emissionProbs){
	//prepare variables
	long l = 0;
	int G, s;
	double sum;

	std::vector<short*>::iterator itG = Data.genotypes.begin();
	std::vector<int*>::iterator itD = Data.depths.begin();

	//run across data
	for(; itG != Data.genotypes.end(); ++itG, ++itD, ++l){
		//add frequency
		for(G=0; G<numGroups; ++G){
			weights[l][G][0] = genotypeFrequencies[G][0]; weights[l][G][1] = genotypeFrequencies[G][1]; weights[l][G][2] = genotypeFrequencies[G][2];
		}

		//multiply by emission probabilities
		for(s=0; s<Data.numSamples; ++s){
			weights[l][Data.groupOfSample[s]][0] *= emissionProbs[Data.batchOfSample[s]][(*itD)[s]][0][(*itG)[s]];
			weights[l][Data.groupOfSample[s]][1] *= emissionProbs[Data.batchOfSample[s]][(*itD)[s]][1][(*itG)[s]];
			weights[l][Data.groupOfSample[s]][2] *= emissionProbs[Data.batchOfSample[s]][(*itD)[s]][2][(*itG)[s]];
		}

		//normalize and add to frequency estimates
		for(G=0; G<numGroups; ++G){
			sum = weights[l][G][0] + weights[l][G][1] + weights[l][G][2];
			weights[l][G][0] /= sum;
			weights[l][G][1] /= sum;
			weights[l][G][2] /= sum;
		}
	}
}

void TIndividualReplicatesModel::estimateFrequencies(TGenoErrorStorage & Data){
	//set new Freq = 0
	int G;
	for(G=0; G<numGroups; ++G){
		newGenotypeFrequencies[G][0] = 0.0;
		newGenotypeFrequencies[G][1] = 0.0;
		newGenotypeFrequencies[G][2] = 0.0;
	}

	//loop over sites and add to frequency estimates
	for(long l=0; l<numSnps; ++l){
		for(G=0; G<numGroups; ++G){
			newGenotypeFrequencies[G][0] += weights[l][G][0];
			newGenotypeFrequencies[G][1] += weights[l][G][1];
			newGenotypeFrequencies[G][2] += weights[l][G][2];
		}
	}

	//estimate all f
	for(G=0; G<numGroups; ++G){
		newGenotypeFrequencies[G][0] /= (double) numSnps;
		newGenotypeFrequencies[G][1] /= (double) numSnps;
		newGenotypeFrequencies[G][2] /= (double) numSnps;
	}


	//switch old and new
	double** tmpFreq = genotypeFrequencies;
	genotypeFrequencies = newGenotypeFrequencies;
	newGenotypeFrequencies = tmpFreq;
};

void TIndividualReplicatesModel::fillQ(double** Q, TGenoErrorStorage & Data, double**** emissionProbs){
	//variables
	std::vector<short*>::iterator itG = Data.genotypes.begin();
	std::vector<int*>::iterator itD = Data.depths.begin();
	long l = 0;
	int s;

	//Now add to appropriate Q
	for(; itG != Data.genotypes.end(); ++itG, ++itD, ++l){
		for(s=0; s<Data.numSamples; ++s){
			Q[Data.batchOfSample[s]][(*itD)[s]]
						 += weights[l][Data.groupOfSample[s]][0] * emissionProbs[Data.batchOfSample[s]][(*itD)[s]][0][(*itG)[s]]
					     +  weights[l][Data.groupOfSample[s]][1] * emissionProbs[Data.batchOfSample[s]][(*itD)[s]][1][(*itG)[s]]
						 +  weights[l][Data.groupOfSample[s]][2] * emissionProbs[Data.batchOfSample[s]][(*itD)[s]][2][(*itG)[s]];
		}
	}
};

void TIndividualReplicatesModel::fillQHomoOnly(double** Q, TGenoErrorStorage & Data, double**** emissionProbs){
	//variables
	std::vector<short*>::iterator itG = Data.genotypes.begin();
	std::vector<int*>::iterator itD = Data.depths.begin();
	long l = 0;
	int s;

	//Now add to appropriate Q
	for(; itG != Data.genotypes.end(); ++itG, ++itD, ++l){
		for(s=0; s<Data.numSamples; ++s){
			Q[Data.batchOfSample[s]][(*itD)[s]]
						 += weights[l][Data.groupOfSample[s]][0] * emissionProbs[Data.batchOfSample[s]][(*itD)[s]][0][(*itG)[s]]
						 +  weights[l][Data.groupOfSample[s]][2] * emissionProbs[Data.batchOfSample[s]][(*itD)[s]][2][(*itG)[s]];
		}
	}
};

void TIndividualReplicatesModel::fillQHetOnly(double** Q, TGenoErrorStorage & Data, double**** emissionProbs){
	//variables
	std::vector<short*>::iterator itG = Data.genotypes.begin();
	std::vector<int*>::iterator itD = Data.depths.begin();
	long l = 0;
	int s;

	//Now add to appropriate Q
	for(; itG != Data.genotypes.end(); ++itG, ++itD, ++l){
		for(s=0; s<Data.numSamples; ++s){
			Q[Data.batchOfSample[s]][(*itD)[s]] +=  weights[l][Data.groupOfSample[s]][1] * emissionProbs[Data.batchOfSample[s]][(*itD)[s]][1][(*itG)[s]];

		}
	}
};

void TIndividualReplicatesModel::writeModelSpecificParameterEstimates(std::string outname, TGenoErrorStorage & Data, TLog* logfile){
	std::string filename = outname + "_individualGenotypeFrequencies.txt";
	logfile->listFlush("Writing estimated genotype frequencies to file '" + filename + "' ...");
	std::ofstream out(filename.c_str());
	if(!out)
		throw "Failed to open file '" + filename + "' for writing!";
	out << "sample\tfreq_0\tfreq_1\tfreq_2\n";

	for(int G=0; G<numGroups ; ++G){
		out << Data.getGroupString(G);
		out << "\t" << genotypeFrequencies[G][0] << "\t" << genotypeFrequencies[G][1] << "\t" << genotypeFrequencies[G][2];
		out << "\n";
	}
	out.close();
	logfile->done();
}


//********************************************************
//THardyWeinbergModel
//********************************************************
THardyWeinbergModel::THardyWeinbergModel():TLikelihoodModel(){
	frequenciesInitialized = false;
	genotypeFrequencies = NULL;
	alleleFreq = NULL;
	newAlleleFreq = NULL;
	alpha = NULL;
	beta = NULL;
	logAlpha = NULL;
	logBeta = NULL;
	proposalWidthFrequencies = NULL;
	proposalWidthAlphaBeta = 0;

	numAlphaBetaAccepted = 0;
	numFreqAccepted = NULL;
	numUpdates = 0;

	//set prior
	//TODO: read from user input!
	sigma2AlphaBetaPrior = 0.25;
	muAlphaBetaPrior = log(0.5);
	OneOverTwoSigma2 = 1.0 / 2.0 / sigma2AlphaBetaPrior;
};

THardyWeinbergModel::THardyWeinbergModel(TGenoErrorStorage & Data):TLikelihoodModel(){
	numGroups = Data.numGroups;
	numSnps = Data.numSnps;
	numSamples = Data.numSamples;

	initializeWeights(Data);
	initializeFrequencies();

	alleleFreqPosteriorMean.setDimensions(numGroups, numSnps);
	logAlphaPosteriorMean.setDimension(numGroups);
	logBetaPosteriorMean.setDimension(numGroups);

	numAlphaBetaAccepted = 0;
	numUpdates = 0;
}

void THardyWeinbergModel::initializeFrequencies(){
	//frequencies
	alleleFreq = new double*[numGroups];
	newAlleleFreq = new double*[numGroups];
	genotypeFrequencies = new double**[numGroups];
	proposalWidthFrequencies = new double*[numGroups];

	numFreqAccepted = new long*[numGroups];

	alpha = new double[numGroups];
	beta = new double[numGroups];
	logAlpha = new double[numGroups];
	logBeta = new double[numGroups];

	for(int G=0; G<numGroups; ++G){
		alleleFreq[G] = new double[numSnps];
		newAlleleFreq[G] = new double[numSnps];
		genotypeFrequencies[G] = new double*[numSnps];
		proposalWidthFrequencies[G] = new double[numSnps];

		numFreqAccepted[G] = new long[numSnps];

		for(int s=0; s<numSnps; ++s){
			genotypeFrequencies[G][s] = new double[4]; //missing genotype = 3
			genotypeFrequencies[G][s][3] = 1.0; //missing is always 1.0
			numFreqAccepted[G][s] = 0;
		}
	}

	frequenciesInitialized = true;
}

void THardyWeinbergModel::freeFrequencies(){
	if(frequenciesInitialized){
		for(int G=0; G<numGroups; ++G){
			for(int s=0; s<numSnps; ++s)
				delete[] genotypeFrequencies[G][s];

			delete[] alleleFreq[G];
			delete[] newAlleleFreq[G];
			delete[] genotypeFrequencies[G];
			delete[] proposalWidthFrequencies[G];
			delete[] numFreqAccepted[G];
		}
		delete[] alleleFreq;
		delete[] newAlleleFreq;
		delete[] genotypeFrequencies;
		delete[] proposalWidthFrequencies;
		delete[] numFreqAccepted;
		delete[] alpha;
		delete[] beta;
		delete[] logAlpha;
		delete[] logBeta;
	}

	frequenciesInitialized = false;
};

void THardyWeinbergModel::initializeWeights(TGenoErrorStorage & Data){
	//weights
	weights = new double**[numSnps];
	long l; int s;
	for(l=0; l<numSnps; ++l){
		weights[l] = new double*[numSamples];
		for(s=0; s<numSamples; ++s)
			weights[l][s] = new double[3];
	}

	weightsInitialized = true;
};

void THardyWeinbergModel::freeWeights(){
	if(weightsInitialized){
		for(long l=0; l<numSnps; ++l){
			for(int s=0; s<numSamples; ++s)
				delete[] weights[l][s];
			delete[] weights[l];
		}
		delete[] weights;
	}

	weightsInitialized = false;
}

void THardyWeinbergModel::fillGenotypeFrequenciesOneLocus(long & locus, double** theseAlleleFrequencies){
	for(int G=0; G<numGroups; ++G){
		genotypeFrequencies[G][locus][0] = (1.0 - theseAlleleFrequencies[G][locus]) * (1.0 - theseAlleleFrequencies[G][locus]);
		genotypeFrequencies[G][locus][1] = 2.0 * theseAlleleFrequencies[G][locus] * (1.0 - theseAlleleFrequencies[G][locus]);
		genotypeFrequencies[G][locus][2] = theseAlleleFrequencies[G][locus] * theseAlleleFrequencies[G][locus];
	}
};

void THardyWeinbergModel::fillGenotypeFrequencies(double** theseAlleleFrequencies){
	//fill genotype frequencies according to HardyWeinberg
	for(long l=0; l<numSnps; ++l){
		for(int G=0; G<numGroups; ++G){
			genotypeFrequencies[G][l][0] = (1.0 - theseAlleleFrequencies[G][l]) * (1.0 - theseAlleleFrequencies[G][l]);
			genotypeFrequencies[G][l][1] = 2.0 * theseAlleleFrequencies[G][l] * (1.0 - theseAlleleFrequencies[G][l]);
			genotypeFrequencies[G][l][2] = theseAlleleFrequencies[G][l] * theseAlleleFrequencies[G][l];
		}
	}
};

void THardyWeinbergModel::estimateInitialParameters(TGenoErrorStorage & Data){
	//estimate initial allele frequencies and error rates (independent of depths)
	long l = 0;
	int* numGenotypesPerGroup = new int[numGroups];

	for(std::vector<short*>::iterator itG = Data.genotypes.begin(); itG != Data.genotypes.end(); ++itG, ++l){
		//set to zero
		for(int G=0; G<numGroups; ++G){
			alleleFreq[G][l] = 0.0;
			numGenotypesPerGroup[G] = 0;
		}

		//add data from samples
		for(int s=0; s<numSamples; ++s){
			if((*itG)[s] >= 0){
				++numGenotypesPerGroup[Data.groupOfSample[s]];
				alleleFreq[Data.groupOfSample[s]][l] += (*itG)[s];
			}
		}

		//normalize
		for(int G=0; G<numGroups; ++G){
			alleleFreq[G][l] /= 2.0 *  (double) numGenotypesPerGroup[G];
			if(alleleFreq[G][l] < 0.5)
				alleleFreq[G][l] += 0.1 / (double) numGenotypesPerGroup[G];
			else
				alleleFreq[G][l] -= 0.1 / (double) numGenotypesPerGroup[G];
		}

		//set initial proposal width
		for(int G=0; G<numGroups; ++G){
			double maf = alleleFreq[G][l];
			if(maf > 0.5) maf = 1.0 - maf;
			proposalWidthFrequencies[G][l] = -0.2 / log10(maf); //found to work well to predict acceptance rates. Will be updated after burnin
		}
	}
	delete[] numGenotypesPerGroup;

	//fill genotype frequencies
	fillGenotypeFrequencies(alleleFreq);

	//estimate corresponding alpha and beta
	for(int G=0; G<numGroups; ++G){
		//calc mean and variance of allele frequencies
		double mean = 0.0;
		double sumXSquare = 0.0;
		for(l=0; l<numSnps; ++l){
			mean += alleleFreq[G][l];
			sumXSquare += alleleFreq[G][l] * alleleFreq[G][l];
		}
		mean /= (double) numSnps;
		sumXSquare /= (double) numSnps;
		double var = sumXSquare - mean  * mean;

		//now estimate alpha and beta
		double tmp = ((mean * (1.0 - mean)) / var ) - 1.0;
		alpha[G] = mean * tmp;
		if(alpha[G] < 0.0) alpha[G] = 0.01;
		logAlpha[G] = log(alpha[G]);

		beta[G] = (1.0 - mean) * tmp;
		if(beta[G] < 0.0) beta[G] = 0.01;
		logBeta[G] = log(beta[G]);
	}

	proposalWidthAlphaBeta = 0.1;

	//set prior
	//TODO: read from user input!
	sigma2AlphaBetaPrior = 0.25;
	muAlphaBetaPrior = log(0.5);
	OneOverTwoSigma2 = 1.0 / 2.0 / sigma2AlphaBetaPrior;

	//clear posterior mean storage
	alleleFreqPosteriorMean.clear();
	logAlphaPosteriorMean.clear();
	logBetaPosteriorMean.clear();
};

void THardyWeinbergModel::readFrequenciesfromFile(std::string & filename, TLog* logfile){
	logfile->listFlush("Reading allele frequencies from file '" + filename + "' ...");

	//open file
	std::ifstream file(filename.c_str());
	if(!file) throw "failed to open file '" + filename + " for reading!";

	//read header
	std::vector<std::string> tmpVec;
	fillVectorFromLineWhiteSpaceSkipEmpty(file, tmpVec);
	int numHeaderCol = tmpVec.size();
	if(numHeaderCol < numGroups+1) throw "Wrong number of columns in " + filename + ": " + toString(numHeaderCol) + " instead of " + toString(numGroups + 1) + "!";

	//read line by line and add samples to groups
	int lineNum = 0;
	long l = 0;
	while(file.good() && !file.eof()){
		++lineNum;
		fillVectorFromLineWhiteSpaceSkipEmpty(file, tmpVec);
		if(!tmpVec.empty()){
			if(tmpVec.size() != numHeaderCol) throw "Wrong number of entries on line " + toString(lineNum) + " in file '" + filename + "'! There should be " + toString(numHeaderCol) + "!";

			//check if locus exists
			if(l == numSnps) throw "File contains too many sites!";

			//read frequency
			for(int G=0; G<numGroups; ++G){
				alleleFreq[G][l] = stringToDouble(tmpVec[G+1]);
				if(alleleFreq[G][l] <= 0.0) alleleFreq[G][l] = 0.00000000000001;
				if(alleleFreq[G][l] >= 1.0) alleleFreq[G][l] = 0.99999999999999;

			}
			++l;
		}
	}

	if(l < numSnps) throw "File contains too few sites!";

	//close file
	file.close();
	logfile->done();

	//fill genotype frequencies
	fillGenotypeFrequencies(alleleFreq);
};


double THardyWeinbergModel::estimateInitialErrorRates(TGenoErrorStorage & Data){
	//can not be estimated, so just return a low error rate
	return 0.05;
}

void THardyWeinbergModel::adjustAfterBurnin(){
	//adjust proposal with for f
	for(long l=0; l<numSnps; ++l){
		for(int G=0; G<numGroups; ++G){

			proposalWidthFrequencies[G][l] *=  (double) numFreqAccepted[G][l] / (double) numUpdates * 3.0;
			numFreqAccepted[G][l] = 0;
		}
	}

	//adjust proposal for alpha / beta
	proposalWidthAlphaBeta *= (double) numAlphaBetaAccepted / 2.0 / (double) numUpdates * 3.0;

	numAlphaBetaAccepted = 0;
	numUpdates = 0;
}

double THardyWeinbergModel::calculateLL(TGenoErrorStorage & Data, double**** emissionProbs){
	//variables
	std::vector<short*>::iterator itG = Data.genotypes.begin();
	std::vector<int*>::iterator itD = Data.depths.begin();
	int s;
	double LL = 0.0;
	double tmp;

	//Run across all samples
	long l = 0;
	for(; itG != Data.genotypes.end(); ++itG, ++itD, ++l){
		for(s=0; s<Data.numSamples; ++s){
			tmp = emissionProbs[Data.batchOfSample[s]][(*itD)[s]][0][(*itG)[s]] * genotypeFrequencies[Data.groupOfSample[s]][l][0]
			    + emissionProbs[Data.batchOfSample[s]][(*itD)[s]][1][(*itG)[s]] * genotypeFrequencies[Data.groupOfSample[s]][l][1]
			    + emissionProbs[Data.batchOfSample[s]][(*itD)[s]][2][(*itG)[s]] * genotypeFrequencies[Data.groupOfSample[s]][l][2];

			LL += log(tmp);
		}
	}

	return LL;
}

double THardyWeinbergModel::calculatePosteriorProb(TGenoErrorStorage & Data, double**** emissionProbs, TRandomGenerator & randomGenerator){
	//variables
	std::vector<short*>::iterator itG = Data.genotypes.begin();
	std::vector<int*>::iterator itD = Data.depths.begin();
	int s;
	double posteriorProbability = 0.0;
	double tmp;

	//Run across all samples to get Sum_gamma P(g|gamma)P(gamma|f)
	long l = 0;

	for(; itG != Data.genotypes.end(); ++itG, ++itD, ++l){
		for(s=0; s<Data.numSamples; ++s){
			tmp = emissionProbs[Data.batchOfSample[s]][(*itD)[s]][0][(*itG)[s]] * genotypeFrequencies[Data.groupOfSample[s]][l][0]
			    + emissionProbs[Data.batchOfSample[s]][(*itD)[s]][1][(*itG)[s]] * genotypeFrequencies[Data.groupOfSample[s]][l][1]
			    + emissionProbs[Data.batchOfSample[s]][(*itD)[s]][2][(*itG)[s]] * genotypeFrequencies[Data.groupOfSample[s]][l][2];

			posteriorProbability += log(tmp);
		}
	}

	//add P(f|alpha, beta).
	double sumLogFreq;
	double sumLogOneMinusFreq;
	for(int G=0; G<numGroups; ++G){
		//calc sum of log(f) and log(1-f)
		sumLogFreq = 0.0;
		sumLogOneMinusFreq = 0.0;
		for(l=0; l<numSnps; ++l){
			sumLogFreq += log(alleleFreq[G][l]);
			sumLogOneMinusFreq += log(1.0 - alleleFreq[G][l]);
		}

		//add beta density
		posteriorProbability += (alpha[G]-1.0) * sumLogFreq + (beta[G] - 1.0) * sumLogOneMinusFreq;
		posteriorProbability += numSnps * (randomGenerator.gammaln(alpha[G] + beta[G]) - randomGenerator.gammaln(alpha[G]) - randomGenerator.gammaln(beta[G]));
	}

	return posteriorProbability;
}


void THardyWeinbergModel::writePosteriorSurfaceHeader(std::ofstream & out){
	//alpha=beta between 0 and 2
	int numProbs = 100;
	double step = 10.0 / (numProbs - 1.0);
	for(int i=0; i<numProbs; ++i)
		out << "\t" << i*step;
	out << "\n";
}

void THardyWeinbergModel::writePosteriorSurface(std::ofstream & out, TGenoErrorStorage & Data, double**** emissionProbs, double postProbError, TRandomGenerator & randomGenerator){
	//calculate posterior probabilities for different alpha between 0 and 2
	int numProbs = 100;
	float step = 10.0 / (float) numProbs;

	for(int i=1; i<=numProbs; ++i){
		//set alpha and beta
		for(int G=0; G<numGroups; ++G){
			alpha[G] = i*step;
			beta[G] = i*step;
		}

		out << "\t" << calculatePosteriorProb(Data, emissionProbs, randomGenerator);
	}

	out << "\n";
}

void THardyWeinbergModel::calculateLogHastingsTermErrorUpdate(double** logHastingsTerm, TGenoErrorStorage & Data, double**** emissionProbs){
	//calculate per error class term for hastings ration when updating the error rate
	//variables
	std::vector<short*>::iterator itG = Data.genotypes.begin();
	std::vector<int*>::iterator itD = Data.depths.begin();
	int s;
	double tmp;

	//set to zero
	int d;
	for(int c=0; c<Data.numErrorClasses; ++c){
		for(d=0; d<Data.numDepthsWithData[c]; ++d)
		logHastingsTerm[c][d] = 0.0;
	}

	//Run across all samples
	long l = 0;
	for(; itG != Data.genotypes.end(); ++itG, ++itD, ++l){
		for(s=0; s<Data.numSamples; ++s){
			tmp = emissionProbs[Data.batchOfSample[s]][(*itD)[s]][0][(*itG)[s]] * genotypeFrequencies[Data.groupOfSample[s]][l][0]
			    + emissionProbs[Data.batchOfSample[s]][(*itD)[s]][1][(*itG)[s]] * genotypeFrequencies[Data.groupOfSample[s]][l][1]
			    + emissionProbs[Data.batchOfSample[s]][(*itD)[s]][2][(*itG)[s]] * genotypeFrequencies[Data.groupOfSample[s]][l][2];

			logHastingsTerm[Data.batchOfSample[s]][(*itD)[s]] += log(tmp);
		}
	}
}

void THardyWeinbergModel::calculateWeights(TGenoErrorStorage & Data, double**** emissionProbs){
	//prepare variables
	long l = 0;
	int s;
	double sum;

	std::vector<short*>::iterator itG = Data.genotypes.begin();
	std::vector<int*>::iterator itD = Data.depths.begin();

	//run accross data
	for(; itG != Data.genotypes.end(); ++itG, ++itD, ++l){
		for(s=0; s<Data.numSamples; ++s){
			weights[l][s][0] = emissionProbs[Data.batchOfSample[s]][(*itD)[s]][0][(*itG)[s]] * genotypeFrequencies[Data.groupOfSample[s]][l][0];
			weights[l][s][1] = emissionProbs[Data.batchOfSample[s]][(*itD)[s]][1][(*itG)[s]] * genotypeFrequencies[Data.groupOfSample[s]][l][1];
			weights[l][s][2] = emissionProbs[Data.batchOfSample[s]][(*itD)[s]][2][(*itG)[s]] * genotypeFrequencies[Data.groupOfSample[s]][l][2];

			sum =  weights[l][s][0] + weights[l][s][1] + weights[l][s][2];
			weights[l][s][0] /= sum;
			weights[l][s][1] /= sum;
			weights[l][s][2] /= sum;
		}
	}
}

void THardyWeinbergModel::estimateFrequencies(TGenoErrorStorage & Data){
	//set new Freq = 0
	int s, G;
	for(long l=0; l<numSnps; ++l){
		for(G=0; G<numGroups; ++G)
			newAlleleFreq[G][l] = 0.0;

		//loop over samples
		for(s=0; s<Data.numSamples; ++s)
			newAlleleFreq[Data.groupOfSample[s]][l] += weights[l][s][1] + 2.0 * weights[l][s][2];

		for(G=0; G<Data.numGroups; ++G)
			newAlleleFreq[G][l] /= 2.0 * Data.numSamplesPerGroup[G];
	}

	//switch old and new
	double** tmpFreq = alleleFreq;
	alleleFreq = newAlleleFreq;
	newAlleleFreq = tmpFreq;

	//update genotype frequencies
	fillGenotypeFrequencies(alleleFreq);
};


void THardyWeinbergModel::updateAlphaBeta(double & _this, double & _thisLog, double & _other, double & sumLogFreq, TRandomGenerator & randomGenerator){
	//propose new alpha
	double newValLog = _thisLog + (randomGenerator.getRand()-0.5) * proposalWidthAlphaBeta;
	double newVal = exp(newValLog);

	//calculate log hastings
	double logHastings = (newVal - _this) * sumLogFreq;
	logHastings += (double) numSnps
			    * (randomGenerator.gammaln(newVal + _other) + randomGenerator.gammaln(_this) - randomGenerator.gammaln(_this + _other) - randomGenerator.gammaln(newVal));

	//add prior: normal on log
	double tmp1 = _thisLog - muAlphaBetaPrior;
	double tmp2 = newValLog - muAlphaBetaPrior;
	logHastings += OneOverTwoSigma2 * (tmp1*tmp1 - tmp2*tmp2);

	//accept / reject
	if(log(randomGenerator.getRand()) < logHastings){
		_this = newVal;
		_thisLog = newValLog;
		++numAlphaBetaAccepted;
	}
};


void THardyWeinbergModel::updateModelSpecificParametersMCMC(TGenoErrorStorage & Data, double**** emissionProbs, TRandomGenerator & randomGenerator){
	//variables
	++numUpdates;

	//update hierarchical parameters: alpha and beta for each population
	for(int G=0; G<numGroups; ++G){
		//calculate sum of log allele frequencies
		double sumLogFreq = 0.0;
		double sumLogOneMinusFreq = 0.0;

		for(int l=0; l<numSnps; ++l){
			sumLogFreq += log(alleleFreq[G][l]);
			sumLogOneMinusFreq += log(1.0 - alleleFreq[G][l]);
		}

		//update alpha
		updateAlphaBeta(alpha[G], logAlpha[G], beta[G], sumLogFreq, randomGenerator);

		//update beta
		updateAlphaBeta(beta[G], logBeta[G], alpha[G], sumLogOneMinusFreq, randomGenerator);
	}

	//update all frequencies
	double* logHastingsPerGroup = new double[numGroups];
	std::vector<short*>::iterator itG = Data.genotypes.begin();
	std::vector<int*>::iterator itD = Data.depths.begin();
	for(long l=0; itG != Data.genotypes.end(); ++itG, ++itD, ++l){
		//propose new frequencies and cal log hastings prior
		for(int G=0; G<numGroups; ++G){
			newAlleleFreq[G][l] = fabs(alleleFreq[G][l] + (randomGenerator.getRand()-0.5) * proposalWidthFrequencies[G][l]);
			if(newAlleleFreq[G][l] > 1.0) newAlleleFreq[G][l] = 2.0 - newAlleleFreq[G][l];
			if(newAlleleFreq[G][l] == 0.0) newAlleleFreq[G][l] = 0.000000000000001;
			if(newAlleleFreq[G][l] == 1.0) newAlleleFreq[G][l] = 0.999999999999999;

			logHastingsPerGroup[G] = (alpha[G]-1.0) * log(newAlleleFreq[G][l] / alleleFreq[G][l]) + (beta[G] - 1.0) * log((1.0-newAlleleFreq[G][l])/(1.0 - alleleFreq[G][l]));
		}

		//add term per sample: first with old frequencies
		for(int s=0; s<Data.numSamples; ++s){
			double tmp = emissionProbs[Data.batchOfSample[s]][(*itD)[s]][0][(*itG)[s]] * genotypeFrequencies[Data.groupOfSample[s]][l][0]
				+ emissionProbs[Data.batchOfSample[s]][(*itD)[s]][1][(*itG)[s]] * genotypeFrequencies[Data.groupOfSample[s]][l][1]
				+ emissionProbs[Data.batchOfSample[s]][(*itD)[s]][2][(*itG)[s]] * genotypeFrequencies[Data.groupOfSample[s]][l][2];

			logHastingsPerGroup[Data.groupOfSample[s]] -= log(tmp);
		}

		//add term per sample: now with new frequencies
		fillGenotypeFrequenciesOneLocus(l, newAlleleFreq);
		for(int s=0; s<Data.numSamples; ++s){
			double tmp = emissionProbs[Data.batchOfSample[s]][(*itD)[s]][0][(*itG)[s]] * genotypeFrequencies[Data.groupOfSample[s]][l][0]
				+ emissionProbs[Data.batchOfSample[s]][(*itD)[s]][1][(*itG)[s]] * genotypeFrequencies[Data.groupOfSample[s]][l][1]
				+ emissionProbs[Data.batchOfSample[s]][(*itD)[s]][2][(*itG)[s]] * genotypeFrequencies[Data.groupOfSample[s]][l][2];

			logHastingsPerGroup[Data.groupOfSample[s]] += log(tmp);
		}

		//now accept or reject
		for(int G=0; G<numGroups; ++G){
			if(log(randomGenerator.getRand()) < logHastingsPerGroup[G]){
				alleleFreq[G][l] = newAlleleFreq[G][l];
				++numFreqAccepted[G][l];
			}
		}
		fillGenotypeFrequenciesOneLocus(l, alleleFreq);
	}

	//clean up
	delete[] logHastingsPerGroup;
}

void THardyWeinbergModel::writeAcceptanceRateModelSpecificParameters(TLog* logfile){
	//alpha / beta
	logfile->conclude("Acceptance rate alpha / beta was " + toString(numAlphaBetaAccepted / 2.0 / (double) numUpdates, 3) + ".");

	//frequencies
	double sum = 0.0;
	for(long l=0; l<numSnps; ++l){
		for(int G=0; G<numGroups; ++G)
			sum += numFreqAccepted[G][l];
	}

	logfile->conclude("Acceptance rate allele frequencies was " + toString(sum / (double) numSnps / (double) numGroups / (double) numUpdates, 3) + ".");
}

void THardyWeinbergModel::addCurEstimatesToPosteriorMeanStorage(){
	alleleFreqPosteriorMean.add(alleleFreq);
	logAlphaPosteriorMean.add(logAlpha);
	logBetaPosteriorMean.add(logBeta);
};

void THardyWeinbergModel::setParametersToPosteriorMean(){
	alleleFreqPosteriorMean.setToMean(alleleFreq);
	logAlphaPosteriorMean.setToMean(logAlpha);
	logBetaPosteriorMean.setToMean(logBeta);
}

void THardyWeinbergModel::fillQ(double** Q, TGenoErrorStorage & Data, double**** logEmissionProbs){
	//variables
	std::vector<short*>::iterator itG = Data.genotypes.begin();
	std::vector<int*>::iterator itD = Data.depths.begin();
	long l = 0;
	int s;

	//Now add to appropriate Q
	for(; itG != Data.genotypes.end(); ++itG, ++itD, ++l){
		for(s=0; s<Data.numSamples; ++s){

			Q[Data.batchOfSample[s]][(*itD)[s]]
						 += weights[l][s][0] * logEmissionProbs[Data.batchOfSample[s]][(*itD)[s]][0][(*itG)[s]]
						 +  weights[l][s][2] * logEmissionProbs[Data.batchOfSample[s]][(*itD)[s]][2][(*itG)[s]];
		}
	}
};

void THardyWeinbergModel::fillQHomoOnly(double** Q, TGenoErrorStorage & Data, double**** logEmissionProbs){
	//variables
	std::vector<short*>::iterator itG = Data.genotypes.begin();
	std::vector<int*>::iterator itD = Data.depths.begin();
	long l = 0;
	int s;

	//Now add to appropriate Q
	for(; itG != Data.genotypes.end(); ++itG, ++itD, ++l){
		for(s=0; s<Data.numSamples; ++s){

			Q[Data.batchOfSample[s]][(*itD)[s]]
						 += weights[l][s][0] * logEmissionProbs[Data.batchOfSample[s]][(*itD)[s]][0][(*itG)[s]]
					      +  weights[l][s][1] * logEmissionProbs[Data.batchOfSample[s]][(*itD)[s]][1][(*itG)[s]]
						 +  weights[l][s][2] * logEmissionProbs[Data.batchOfSample[s]][(*itD)[s]][2][(*itG)[s]];
		}
	}
};

void THardyWeinbergModel::fillQHetOnly(double** Q, TGenoErrorStorage & Data, double**** logEmissionProbs){
	//variables
	std::vector<short*>::iterator itG = Data.genotypes.begin();
	std::vector<int*>::iterator itD = Data.depths.begin();
	long l = 0;
	int s;

	//Now add to appropriate Q
	for(; itG != Data.genotypes.end(); ++itG, ++itD, ++l){
		for(s=0; s<Data.numSamples; ++s){
			Q[Data.batchOfSample[s]][(*itD)[s]] += weights[l][s][1] * logEmissionProbs[Data.batchOfSample[s]][(*itD)[s]][1][(*itG)[s]];
		}
	}
};

void THardyWeinbergModel::writeModelSpecificParameterEstimates(std::string outname, TGenoErrorStorage & Data, TLog* logfile){
	//write allele frequencies
	std::string filename = outname + "_populationAlleleFrequencies.txt";
	logfile->listFlush("Writing estimated allele frequencies to file '" + filename + "' ...");
	std::ofstream out(filename.c_str());
	if(!out)
		throw "Failed to open file '" + filename + "' for writing!";

	//write header
	out << "Chr\tLocus";
	for(int G=0; G<numGroups; ++G)
		out << "\t" << Data.getGroupName(G) << "_freq";
	out << "\n";

	//write frequencies
	for(long l=0; l<numSnps; ++l){
		out << Data.getChrPosString(l);
		for(int G=0; G<numGroups; ++G)
			out << "\t" << alleleFreq[G][l];
		out << "\n";
	}
	out.close();
	logfile->done();

	//write alpha and beta
	filename = outname + "_alphaBeta.txt";
	logfile->listFlush("Writing estimated alpha and beta to file '" + filename + "' ...");
	out.open(filename.c_str());
	if(!out)
		throw "Failed to open file '" + filename + "' for writing!";

	//write header
	for(int G=0; G<numGroups; ++G){
		if(G > 0) out << "\t";
		out << Data.getGroupName(G) << "_logAlpha";
		out << "\t" << Data.getGroupName(G) << "_logBeta";
	}
	out << "\n";

	//write estimates
	for(int G=0; G<numGroups; ++G){
		if(G > 0) out << "\t";
		out << logAlpha[G];
		out << "\t" << logBeta[G];
	}
	out << "\n";
	out.close();
	logfile->done();
}

void THardyWeinbergModel::writeHeaderHierarchicalParameters(gz::ogzstream & out){
	for(int G=0; G<numGroups; ++G)
		out << "\tlogAlpha_" << G << "\tlogBeta_" << G;

	/*
	for(int G=0; G<numGroups; ++G){
		out << "\tlogAlpha_" << G << "\tlogBeta_" << G;
		for(int l=0; l<numSnps; ++l)
			out << "\tfreq_" << G << "_" << l;

	}
	*/

}

void THardyWeinbergModel::writeCurHierarchicalParameters(gz::ogzstream & out){
	for(int G=0; G<numGroups; ++G)
		out << "\t" << logAlpha[G] << "\t" << logBeta[G];

/*
	for(int G=0; G<numGroups; ++G){
		out << "\t" << logAlpha[G] << "\t" << logBeta[G];
		for(int l=0; l<numSnps; ++l)
			out << "\t" << alleleFreq[G][l];
	}
	*/
}

/*
// ********************************************************
//TPopulationReplicateModel
// ********************************************************
TPopulationReplicateModel::TPopulationReplicateModel(TGenoErrorStorage & Data):THardyWeinbergModel(){
	initializeWeights(Data);
	initializeFrequencies();
}

void TPopulationReplicateModel::initializeFrequencies(){
	//frequencies
	genotypeFrequencies = new double**[numGroups];
	newFreq = new double**[numGroups];
	for(int G=0; G<numGroups; ++G){
		genotypeFrequencies[G] = new double*[numSnps];
		newFreq[G] = new double*[numSnps];
		for(int s=0; s<numSnps; ++s){
			genotypeFrequencies[G][s] = new double[4]; //missing genotype = 3
			newFreq[G][s] = new double[4];
		}
	}

	frequenciesInitialized = true;
}

void TPopulationReplicateModel::freeFrequencies(){
	if(frequenciesInitialized){
		for(int G=0; G<numGroups; ++G){
			for(int s=0; s<numSnps; ++s){
				delete[] genotypeFrequencies[G][s];
				delete[] newFreq[G][s];
			}
			delete[] genotypeFrequencies[G];
			delete[] newFreq[G];
		}
		delete[] genotypeFrequencies;
		delete[] newFreq;
	}

	frequenciesInitialized = false;
};

void TPopulationReplicateModel::estimateInitialFrequencies(TGenoErrorStorage & Data){
	//estimate initial genotype frequencies and error rates (independent of depths)
	double sum;
	long l = 0;
	int s;
	for(std::vector<short*>::iterator itG = Data.genotypes.begin(); itG != Data.genotypes.end(); ++itG, ++l){
		//set to zero
		for(int G=0; G<numGroups; ++G){
			genotypeFrequencies[G][l][0] = 0.0;
			genotypeFrequencies[G][l][1] = 0.0;
			genotypeFrequencies[G][l][2] = 0.0;
			genotypeFrequencies[G][l][3] = 1.0;
		}

		//add data from samples
		for(s=0; s<numSamples; ++s){
			++genotypeFrequencies[Data.groupOfSample[s]][l][(*itG)[s]];
		}

		//normalize
		for(int G=0; G<Data.numGroups; ++G){
			sum = genotypeFrequencies[G][l][0] + genotypeFrequencies[G][l][1] + genotypeFrequencies[G][l][2] + genotypeFrequencies[G][l][3];
			genotypeFrequencies[G][l][0] /= sum;
			genotypeFrequencies[G][l][1] /= sum;
			genotypeFrequencies[G][l][2] /= sum;
			genotypeFrequencies[G][l][3] = 1.0; //missing genotype has always probability 1.0
		}
	}
};


void TPopulationReplicateModel::estimateFrequencies(TGenoErrorStorage & Data){
	//set new Freq = 0
	int s, G;
	for(long l=0; l<numSnps; ++l){
		//set to zero
		for(G=0; G<numGroups; ++G){
			newFreq[G][l][0] = 0.0;
			newFreq[G][l][1] = 0.0;
			newFreq[G][l][2] = 0.0;
		}

		//loop over samples and add weights
		for(s=0; s<Data.numSamples; ++s){
			newFreq[Data.groupOfSample[s]][l][0] += weights[l][s][0];
			newFreq[Data.groupOfSample[s]][l][1] += weights[l][s][1];
			newFreq[Data.groupOfSample[s]][l][2] += weights[l][s][2];
		}

		//estimate f
		for(G=0; G<numGroups; ++G){
			newFreq[G][l][0] /= (double) Data.numSamplesPerGroup[G];
			newFreq[G][l][1] /= (double) Data.numSamplesPerGroup[G];
			newFreq[G][l][2] /= (double) Data.numSamplesPerGroup[G];
		}
	}

	//switch old and new
	double*** tmpFreq = genotypeFrequencies;
	genotypeFrequencies = newFreq;
	newFreq = tmpFreq;
};

void TPopulationReplicateModel::writeEstimatedFrequencies(std::string outname, TGenoErrorStorage & Data, TLog* logfile){
	std::string filename = outname + "_populationGenotypeFrequencies.txt";
	logfile->listFlush("Writing estimated genotype frequencies to file '" + filename + "' ...");
	std::ofstream out(filename.c_str());
	if(!out)
		throw "Failed to open file '" + filename + "' for writing!";
	out << "Chr\tLocus";
	int G;
	for(G=0; G<numGroups; ++G)
		out << "\t" << Data.getGroupName(G) << "_freq_0\t" << Data.getGroupName(G) << "_freq_1\t" << Data.getGroupName(G) << "_freq_2";
	out << "\n";

	//write frequencies
	for(long l=0; l<numSnps; ++l){
		out << Data.getChrPosString(l);
		for(G=0; G<numGroups; ++G)
			out << "\t" << genotypeFrequencies[G][l][0] << "\t" << genotypeFrequencies[G][l][1] << "\t" << genotypeFrequencies[G][l][2];
		out << "\n";
	}
	out.close();
}
*/
