#make file for genoError
SRC = $(wildcard *.cpp) $(wildcard *.C) $(wildcard commonutilities/*.cpp) $(wildcard commonutilities/*.C)
GIT_HEADER = commonutilities/gitversion.cpp

OBJ = $(SRC:%.cpp=%.o)
BIN = tiger

.PHONY : all
all : $(BIN)


$(BIN): $(GIT_HEADER) $(OBJ)
	$(CXX) -O3 -o $(BIN) $(OBJ) -lz

$(GIT_HEADER): .git/HEAD .git/COMMIT_EDITMSG
	echo "#include \"gitversion.h\"" > $@
	echo "std::string getGitVersion(){" >> $@
	echo "return \"$(shell git rev-parse HEAD)\";" >> $@
	echo "}" >> $@

.git/COMMIT_EDITMSG :
	touch $@

%.o: %.cpp
	$(CXX) -O3 -c -std=c++1y -I. -Icommonutilities $< -o $@

.PHONY : clean
clean:
	rm -rf $(BIN) $(OBJ)

