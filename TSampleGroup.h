/*
 * TSampleGroup.h
 *
 *  Created on: Aug 9, 2011
 *      Author: wegmannd
 */

#ifndef TSAMPLEGROUP_H_
#define TSAMPLEGROUP_H_

#include <vector>
#include "TVcfFile.h"
#include "TRandomGenerator.h"
#include "TLog.h"

class TSampleGroup{
private:
	bool fastAccessInitialized;
	bool fastAccessNeedsUpdate;

public:
	std::string name;
	int numSamples;
	unsigned int* samples;
	TVcfFileSingleLine* vcfFile;
	std::vector<std::string> sampleNames;

	TSampleGroup(){
		numSamples = 0;
		vcfFile = NULL;
		samples = NULL;
		fastAccessInitialized = false;
		fastAccessNeedsUpdate = false;
	};
	TSampleGroup(std::string Name, TVcfFileSingleLine* VcfFile);
	TSampleGroup(std::string Name, TVcfFileSingleLine* VcfFile, std::string & Samples);
	~TSampleGroup();

	void updateFastAccess();
	void addSample(std::string sampleName);
	void addSamplesFromSampleString(std::string & Samples);
	std::string getListOfSampleNames();
	void fillAlleleCountsFromGT(int & numRef, int & numAlt);
	void fillAlleleCountsFromGP(int & numRef, int & numAlt, double & cutOff);
	void addSamplesToVector(std::vector<int> & vec);
	void addSamplesToVector(std::vector<std::string> & vec);
	void addSamplesToArray(unsigned int* array, int & index);
	bool sampleInGroup(const int & sample);
};

class TSampleGroups{
private:
	bool initialized;

	void removeGroupsSizeOne(TLog* logfile);
	void fillMap();

public:
	std::vector<TSampleGroup*> groups;
	std::vector<TSampleGroup*>::iterator it;
	TVcfFileSingleLine* vcfFile;
	int numGroups;
	int numSamples;
	int* numSamplesPerGroup;
	int* groupOfSample;
	unsigned int** pointerToSampleVectors;
	int* groupOfSampleVcfIndex;


	TSampleGroups();
	TSampleGroups(TVcfFileSingleLine* VcfFile, std::string Samples, TLog* logfile);
	~TSampleGroups(){
		if(initialized){
			for(it = groups.begin(); it!=groups.end(); ++it) delete *it;
			delete[] numSamplesPerGroup;
			delete[] pointerToSampleVectors;
			delete[] groupOfSample;
			delete[] groupOfSampleVcfIndex;
		}
	};

	void initializeSingleGroup(TVcfFileSingleLine* VcfFile, TLog* logfile);
	void initializeSingleGroup(TVcfFileSingleLine* VcfFile, std::vector<std::string> Samples, TLog* logfile);
	void initialize(TVcfFileSingleLine* VcfFile, std::string Samples, TLog* logfile, bool removeGroupsSizeOne);
	void initialize(TVcfFileSingleLine* VcfFile, std::string filename, int column, TLog* logfile, bool removeGroupsSizeOne);
	void print(TLog* logfile);
	std::string getName(int groupIndex);
	void getNumSamples();
	std::string getGroupString(int group);
	void fillVectorOfGroupNames(std::vector<std::string> & vec);
	void begin(){it = groups.begin();};
	bool next(){++it; if(it==groups.end()) return false; else return true;};
	std::string firstSampleInCurrentGroup(){ return (*it)->sampleNames[0]; };
	void fillCurrentAlleleCountsGT(int & numRef, int & numAlt);
	void fillCurrentAlleleCountsGP(int & numRef, int & numAlt, double & cutOff);
	void fillVectorOfAllSamples(std::vector<int> & vec);
	void fillVectorOfAllSamples(std::vector<std::string> & vec);
	void fill2DArrayOfSamples(unsigned int** & array);
	void fillArrayOfAllSamples(unsigned int* & array);
	bool sampleVcfIndexInGroup(const int & sample);
	int getGroupOfSampleVcfIndex(const int & sample);
};


#endif /* TSAMPLEGROUP_H_ */
