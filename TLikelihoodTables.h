/*
 * TLikelihoodTables.h
 *
 *  Created on: Feb 28, 2018
 *      Author: phaentu
 */

#ifndef TLIKELIHOODTABLES_H_
#define TLIKELIHOODTABLES_H_

#include "TParameters.h"
#include "TRandomGenerator.h"
#include "TMatrix.h"
#include "TSimulationVCF.h"

/////////////////////////////////////////////////////////////////////////////////////////
// LIKELIHOOD TABLES                                                                   //
/////////////////////////////////////////////////////////////////////////////////////////

class TLikelihoodTable{
private:
	void fill(const double & error){
		likelihood.resize(3); //row=true, col=observed

		double oneMinusError = 1.0 - error;
		likelihood(0,0) = oneMinusError * oneMinusError;
		likelihood(0,1) = 2.0 * oneMinusError * error;
		likelihood(0,2) = error * error;
		likelihood(1,0) = oneMinusError * error;
		likelihood(1,1) = oneMinusError * oneMinusError + error * error;
		likelihood(1,2) = oneMinusError * error;
		likelihood(2,0) = error * error;
		likelihood(2,1) = 2.0 * oneMinusError * error;
		likelihood(2,2) = oneMinusError * oneMinusError;

		//now fill cumulative and phred scaled tables
		fillCumulAndPhred();
	};

protected:
	void fillCumulAndPhred(){
		//fill cumulative
		cumul.fillAsCumulative(likelihood);

		//fill as normalized phred scaled
		phred.resize(3);
		double min;
		for(int i=0; i<3; ++i){
			//calc phred
			for(int j=0; j<3; ++j){
				phred(i,j) = -10.0 * log10(likelihood(i,j));
			}

			//find min
			min = phred(i,0);
			if(phred(i,1) < min)
				min = phred(i,1);
			if(phred(i,2) < min)
				min = phred(i,2);

			//normalize
			for(int j=0; j<3; ++j){
				phred(i,j) -= min;
			}
		}
	};

public:
	TSquareMatrixStorage likelihood;
	TSquareMatrixStorage cumul;
	TSquareMatrixStorage phred;

	TLikelihoodTable(){};
	TLikelihoodTable(const double & error){
		fill(error);
	};
	virtual ~TLikelihoodTable(){};

	virtual void update(const double & error){
		fill(error);
	};
	virtual void update(const double & error, const double & errorHet){
		fill(error);
	};

	int simulateAndWriteGenotype(const int & trueGenotype, const int depth, TSimulationVCF & vcf, TRandomGenerator & randomGenerator){
		int observedGenotype = randomGenerator.pickOne(3, cumul.Row(trueGenotype));
		vcf.writeObservedGenotype(observedGenotype, toString(phred(0,observedGenotype)) + "," + toString(phred(1,observedGenotype)) + "," + toString(phred(2,observedGenotype)), depth);
		return observedGenotype;
	};

	int simulateAndWriteGenotype(const int & trueGenotype, const int depth, TSimulationVCF & vcf, std::ofstream & R_input, TRandomGenerator & randomGenerator){
		int observedGenotype = randomGenerator.pickOne(3, cumul.Row(trueGenotype));
		vcf.writeObservedGenotype(observedGenotype, toString(phred(0,observedGenotype)) + "," + toString(phred(1,observedGenotype)) + "," + toString(phred(2,observedGenotype)), depth);
		R_input << "\t" << observedGenotype + 1;
		return observedGenotype;
	};

	std::string getPLstring(int observedGeno){
		//return normalized phred for this true genotype as string
		return toString(phred(0, observedGeno)) + "," + toString(phred(1, observedGeno)) + "," + toString(phred(2, observedGeno));
	};

	double getLikelihood(const int trueGenotype, const int observedGenotype){
		return likelihood(trueGenotype, observedGenotype);
	};
};

class TLikelihoodTableHet:public TLikelihoodTable{
private:
	void fill(const double & error, const double & errorHet){
		likelihood.resize(3); //row=true, col=observed

		//homozygous
		double oneMinusError = 1.0 - error;
		likelihood(0,0) = oneMinusError * oneMinusError;
		likelihood(0,1) = 2.0 * oneMinusError * error;
		likelihood(0,2) = error * error;

		likelihood(2,0) = error * error;
		likelihood(2,1) = 2.0 * oneMinusError * error;
		likelihood(2,2) = oneMinusError * oneMinusError;

		//heterozygous
		oneMinusError = 1.0 - errorHet;
		likelihood(1,0) = oneMinusError * errorHet;
		likelihood(1,1) = oneMinusError * oneMinusError + errorHet * errorHet;
		likelihood(1,2) = oneMinusError * errorHet;

		//now fill cumulative and phred scaled tables
		fillCumulAndPhred();
	};

public:
	TLikelihoodTableHet(){};
	TLikelihoodTableHet(const double & error, const double & errorHet){
		fill(error, errorHet);
	};

	void update(const double & error){
		fill(error, error);
	};
	void update(const double & error, const double & errorHet){
		fill(error, errorHet);
	};
};

class TLikelihoodTablesOneError{
private:
	void parseErrorRates(std::string & errorString);
	void fillEmissionLikelihoodMatrixSimulations(TSquareMatrixStorage & likelihood, const double & error);

protected:
	std::string name;
	std::vector<double> errorRates;
	bool storageInitialized;
	std::string genotypeStrings[3];

	TLikelihoodTable* likTables;

	void init(std::string Name);
	virtual void initStorage(int NumErrorRates);
	void freeStorage();

public:
	int numErrorRates;

	TLikelihoodTablesOneError(){ init(""); };
	TLikelihoodTablesOneError(std::string Name);
	TLikelihoodTablesOneError(std::string Name, std::string errorString);
	virtual ~TLikelihoodTablesOneError(){
		freeStorage();
	};

	void update(double error);
	void update(double error, double errorHet);
	std::string getName(){ return name; };
	virtual std::string getClassString();
	int simulateAndWriteGenotype(const int & trueGenotype, TSimulationVCF & vcf, TRandomGenerator & RandomGenerator);
	int simulateAndWriteGenotype(const int & trueGenotype, TSimulationVCF & vcf, std::ofstream & R_input, TRandomGenerator & RandomGenerator);
};

class TLikelihoodTablesTwoErrors:public TLikelihoodTablesOneError{
private:
	std::vector<double> errorRatesHet;

	void initStorage(int NumErrorRates);
	void parseErrorRates(std::string & errorString, std::string & errorStringHet);
	void fillEmissionLikelihoodMatrixSimulations(TSquareMatrixStorage & likelihood, const double & error, const double & errorHet);

public:

	TLikelihoodTablesTwoErrors():TLikelihoodTablesOneError(){};
	TLikelihoodTablesTwoErrors(std::string Name):TLikelihoodTablesOneError(Name){};
	TLikelihoodTablesTwoErrors(std::string Name, std::string errorString, std::string errorStringHet);
	~TLikelihoodTablesTwoErrors(){};

	void update(std::string & errorString, std::string & errorStringHet);
	std::string getClassString();
};

class TLikelihoodTables{
private:
	TRandomGenerator* randomGenerator;
	std::vector< TLikelihoodTablesOneError* > likTableErrorClasses;
	std::vector< TLikelihoodTablesOneError* >::iterator likClassIt;
	int numClasses;

	void parseErrorRates(TParameters* myParameters, TLog* logfile);

public:
	TLikelihoodTables(TParameters* myParameters, TLog* logfile, TRandomGenerator* RandomGenerator);
	~TLikelihoodTables();

	int getNumClasses(){ return numClasses; };
	std::string getClassName(int index){ return likTableErrorClasses[index]->getName(); };
	void simulateErrorClassMemberships(int numSamples, std::vector<int> & errorClasses);
	int simulateAndWriteGenotype(int trueGenotype, int errorClass, TSimulationVCF & vcf);
	int simulateAndWriteGenotype(int trueGenotype, int errorClass, TSimulationVCF & vcf, std::ofstream & R_input);
};



#endif /* TLIKELIHOODTABLES_H_ */
