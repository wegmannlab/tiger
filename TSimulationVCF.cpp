/*
 * TSimulationVCF.cpp
 *
 *  Created on: Feb 28, 2018
 *      Author: phaentu
 */


#include "TSimulationVCF.h"


/////////////////////////////////////////////////////////////////////////////////////////
// TSimulationVCF
/////////////////////////////////////////////////////////////////////////////////////////
TSimulationVCF::TSimulationVCF(){
	init();
}

TSimulationVCF::TSimulationVCF(std::string Filename, TLog* logfile){
	init();
	openVCF(Filename, logfile);
};

void TSimulationVCF::init(){
	vcfOpen = false;
	site = 0;

	//set genotype strings
	genotypeStrings[0] = "0/0"; genotypeStrings[1] = "0/1"; genotypeStrings[2] = "1/1";
	truePLStrings[0] = "0,100,100"; truePLStrings[1] = "100,0,100"; truePLStrings[2] = "100,100,0";
}

void TSimulationVCF::openVCF(std::string Filename, TLog* logfile){
	//close if it was open before;
	closeVcf();

	//now open new file
	filename = Filename;
	logfile->list("Writing resulting vcf to file '" + filename + "'.");
	vcfOutFileStream.open(filename.c_str());
	if(!vcfOutFileStream) throw "Failed to open file '" + filename + "'!";

	//write info
	vcfOutFileStream << "##fileformat=VCFv4.2\n";
	vcfOutFileStream << "##source=SegDistort\n";
	vcfOutFileStream << "##FORMAT=<ID=GT,Number=1,Type=String,Description=\"Genotype\">\n";
	vcfOutFileStream << "##FORMAT=<ID=GQ,Number=1,Type=Integer,Description=\"Genotype Quality\">\n";
	vcfOutFileStream << "##FORMAT=<ID=PL,Number=G,Type=Integer,Description=\"Phred-scaled genotype likelihoods\">\n";
	vcfOutFileStream << "##FORMAT=<ID=DP,Number=G,Type=Integer,Description=\"Depth at site\">\n";

	vcfOpen = true;
};

void TSimulationVCF::closeVcf(){
	if(vcfOpen){
		vcfOutFileStream << "\n";
		vcfOutFileStream.close();
	}
};

void TSimulationVCF::writeHeader(int numSamples){
	vcfOutFileStream << "#CHROM\tPOS\tID\tREF\tALT\tQUAL\tFILTER\tINFO\tFORMAT";
	for(int i=0; i<numSamples; ++i)
		vcfOutFileStream << "\tSample_" << i;
};


void TSimulationVCF::writeHeader(std::vector<std::string> & sampleNames){
	vcfOutFileStream << "#CHROM\tPOS\tID\tREF\tALT\tQUAL\tFILTER\tINFO\tFORMAT";
	for(std::vector<std::string>::iterator it=sampleNames.begin(); it!=sampleNames.end(); ++it)
		vcfOutFileStream << "\t" << *it;
};

void TSimulationVCF::newSite(){
	vcfOutFileStream << "\n";
	++site;
	vcfOutFileStream << "1\t" << site << "\t.\tA\tC\t50\t.\t.\tGT:GQ:PL:DP";
};

void TSimulationVCF::writeTrueGenotype(int geno){
	vcfOutFileStream << "\t" << genotypeStrings[geno] << ":30:";
	vcfOutFileStream << truePLStrings[geno];
	vcfOutFileStream << ":" << 100;
};

void TSimulationVCF::writeObservedGenotype(int geno, std::string PL, int depth){
	vcfOutFileStream << "\t" << genotypeStrings[geno] << ":30:";
	vcfOutFileStream << PL;
	vcfOutFileStream << ":" << depth;
};

