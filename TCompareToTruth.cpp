/*
 * Tannotator.cpp
 *
 *  Created on: Jun 15, 2011
 *      Author: wegmannd
 */
#include "TCompareToTruth.h"

#include <algorithm>
#include <typeinfo>
#include <sstream>
#include "gzstream.h"

//---------------------------------------------------
//TGenoComparisonTable
//---------------------------------------------------
TGenoComparisonTable::TGenoComparisonTable(){
	genoString = new std::string[4];
	genoString[0] = "0/0";
	genoString[1] = "0/1";
	genoString[2] = "1/1";
	genoString[3] = "./.";

	counts = new double*[4];
	for(int g=0; g<4; ++g)
		counts[g] = new double[4];

	clear();
};

void TGenoComparisonTable::clear(){
	for(int g=0; g<4; ++g){
		for(int h=0; h<4; ++h)
			counts[g][h] = 0.0;
	}
};

long TGenoComparisonTable::size(bool includeMissing){
	long tot = 0;
	int g,h;
	int upTo = 3;
	if(includeMissing)
		upTo = 4;

	//now count
	for(g=0; g<upTo; ++g){
		for(h=0; h<upTo; ++h)
			tot += counts[g][h];
	}

	return tot;
};

void TGenoComparisonTable::simulate(long n, double eps, double het, TRandomGenerator* randomGenerator){
	//TODO: think of how to combine with simulations in TGenoErrorReplicates. Should use same code!
	//fill error rate tables: [true][observed]
	double** err = new double*[3];
	double** errCumul = new double*[3];
	for(int g=0; g<3; ++g){
		err[g] = new double[2];
		errCumul[g] = new double[3];
	}

	double oneMinusEps = 1.0 - eps;
	err[0][0] = oneMinusEps*oneMinusEps;
	err[0][1] = 2.0 * eps * oneMinusEps;
	err[1][0] = eps * oneMinusEps;
	err[1][1] = eps*eps + oneMinusEps*oneMinusEps;
	err[2][0] = eps * eps;
	err[2][1] = 2.0 * eps * oneMinusEps;

	//fill cumulative
	for(int g=0; g<3; ++g){
		errCumul[g][0] = err[g][0];
		errCumul[g][1] = err[g][0] + err[g][1];
		errCumul[g][2] = 1.0;
	}

	//fill genotype probabilities
	double genoProbCumul[3];
	genoProbCumul[0] = (1.0 - het) / 2.0;
	genoProbCumul[1] = genoProbCumul[0] + het;
	genoProbCumul[2] = 1.0;

	//now simulate
	int gTrue, gObs;
	for(long l=0; l<n; ++l){
		gTrue = randomGenerator->pickOne(3, genoProbCumul);
		gObs = randomGenerator->pickOne(3, errCumul[gTrue]);
		add(gTrue, gObs);
	}

	//delete
	for(int g=0; g<3; ++g){
		delete[] err[g];
		delete[] errCumul[g];
	}
	delete[] err;
	delete[] errCumul;
};

void TGenoComparisonTable::add(const int & trueGeno, const int & observedGeno){
	counts[trueGeno][observedGeno] += 1.0;
};

void TGenoComparisonTable::add(const int & trueGeno, const int & observedGeno, const double value){
	counts[trueGeno][observedGeno] += value;
};

void TGenoComparisonTable::write(const std::string filename){
	//open output file
	std::ofstream out(filename.c_str());
	if(!out) throw "Failed to open file '" + filename + "' for writing!";

	//write header
    out << "true/observed\t0/0\t0/1\t1/1\t./.\n";
    for(int g=0; g<4; ++g){
    	out << genoString[g];
    	for(int h=0; h<4; ++h)
    		out << "\t" << counts[g][h];
    	out << "\n";
    }

	//close
	out.close();
}

double TGenoComparisonTable::calcDerivativeOfLogLikelihood(const double & eps, const double & a, const double & b, const double & c){
	return -(2.0*a + 2.0*b + 4.0*c) * eps * eps * eps + (4.0*a + 2.0*b + 6.0*c) * eps * eps - (3.0*a + b + 2.0*c) * eps + a;
}

double TGenoComparisonTable::maximizeLikelihood(double initialEps, const double & a, const double & b, const double & c){
	//Initialize error a fraction of wrong genotypes
	double eps = initialEps;
	double delta = -0.01;
	double curDeriv = calcDerivativeOfLogLikelihood(eps, a, b, c);

	//now search
	double oldDeriv;
	double e = exp(1.0);
	for(int i=0; i<1000; ++i){
		oldDeriv = curDeriv;
		eps += delta;
		if(eps < 0.0) eps = 0.000001;
		if(eps > 0.5) eps = 0.499999;
		curDeriv = calcDerivativeOfLogLikelihood(eps, a, b, c);

		//switch direction if derivative changes sign
		if(oldDeriv < 0){
			if(curDeriv > 0 || curDeriv < oldDeriv)
				delta = -delta/e;
		} else {
			if(curDeriv < 0 || curDeriv > oldDeriv)
				 delta = -delta/e;
		}

		//std::cout << "eps=" << eps << "\tderiv=" << curDeriv << "\tdelta=" << delta << std::endl;

		if(fabs(delta) < 0.00000001) break;
	  }

	//return
	return eps;
}


double TGenoComparisonTable::estimateErrorRate(){
	//estimate per allele genotyping error rates as describes in Bresadola et al. (2018)

	//prepare variables a, b and c
	double a = counts[1][0] + 2.0 * counts[2][0] + counts[0][1] + counts[2][1] + 2.0 * counts[0][2] + counts[1][2];
	double b = 2.0 * counts[0][0] + counts[1][0] + counts[0][1] + counts[2][1] + counts[1][2] + 2.0 * counts[2][2];
	double c = counts[1][1];

	//Initialize error as fraction of wrong genotypes
	double initialEps = (a + counts[1][0] + counts[0][1] + counts[2][1] + counts[1][2]) / (a + b + c) / 2.0;
	return maximizeLikelihood(initialEps, a, b, c);
};

double TGenoComparisonTable::estimateErrorRateHomozygousSites(){
	//estimate per allele genotyping error rates as describes in Bresadola et al. (2018)

	//prepare variables a, b
	double a = 2.0 * counts[2][0] + counts[0][1] + counts[2][1] + 2.0 * counts[0][2];
	double b = 2.0 * counts[0][0] + counts[0][1] + counts[2][1] + 2.0 * counts[2][2];

	return a / (a + b);
};

double TGenoComparisonTable::estimateErrorRateHeterozygousSites(){
	//estimate per allele genotyping error rates as describes in Bresadola et al. (2018)

	//prepare variables a, b and c
	double a = counts[1][0] + counts[1][2];
	double b = counts[1][0] + counts[1][2];
	double c = counts[1][1];

	//Initialize error a fraction of wrong genotypes
	double initialEps = (a + b) / (a + b + c) / 2.0;
	return maximizeLikelihood(initialEps, a, b, c);
};

std::string TGenoComparisonTable::getNewPL(const int & genotype){
	//calc frequency of observed genotype | true genotype
	double freq[3];
	double rowSum;
	for(int g=0; g<3; ++g){
		rowSum = counts[g][0] + counts[g][1] + counts[g][2] + 3.0;
		freq[g] = -10.0 * log10((counts[g][genotype] + 1.0)/ rowSum);
	}

	//normalize
	double min = freq[0];
	if(freq[1] < min)
		min = freq[1];
	if(freq[2] < min)
		min = freq[2];

	for(int g=0; g<3; ++g)
		freq[g] -= min;

	//turn into string
	return toString(round(freq[0]*10.0)/10.0) + "," + toString(round(freq[1]*10.0)/10.0) + "," + toString(round(freq[2]*10.0)/10.0);
}


//---------------------------------------------------
//TDepthBins
//---------------------------------------------------

TDepthBins::TDepthBins(){
	initialized = false;
	numdepthTables = 0;
	depthTables = NULL;
	depthToBinMap = NULL;
	maxDepthConsideredPlusOne = 0;
};

void TDepthBins::createBins(int maxDepth){
	if(maxDepth < 0) throw "maxDepth must be > 0!";

	numdepthTables = maxDepth;
	depthTables = new TGenoComparisonTable[numdepthTables];

	//create ranges: min = max for all bins
	for(int d=1; d<=maxDepth; ++d)
		rangesOfDepthTables.insert(std::pair<int,int>(d,d));

	//make map
	fillDepthToBinMap();
	initialized = true;
};

void TDepthBins::createBins(std::vector<std::string> & binVec){
	//bins are specified as follows: 1-3,4,5,6-10,...
	if(binVec.size() < 1) throw "No depth bin specified!";

	int Min, Max;
	std::string tmp;

	for(std::vector<std::string>::iterator it=binVec.begin(); it!=binVec.end(); ++it){
		//is it a single number or a range?
		int pos = (*it).find('-');
		if(pos != std::string::npos){
			//is a range
			if(!stringContainsOnly(*it, "0123456789-"))
				throw "Unable to understand depth bin '" + *it  + "'!";

			//extract min and max
			tmp = *it;
			Min = stringToInt(tmp.substr(0, pos));
			tmp.erase(0, pos+1);
			Max = stringToInt(tmp);
			if(Min > Max) throw "Unable to understand depth bin '" + *it + "', min > max!";
			if(Min <= 0) throw "Unable to understand depth bin '" + *it + "', min <= 0!";
		} else {
			//is a single number
			if(!stringContainsOnly(*it, "0123456789"))
				throw "Unable to understand depth bin '" + *it  + "'!";

			Min = stringToInt(*it);
			Max = Min;
		}

		//check if range overlaps
		for(mapIt=rangesOfDepthTables.begin(); mapIt != rangesOfDepthTables.end(); ++mapIt){
			if((Min >= mapIt->first && Min <= mapIt->second) || (Max >= mapIt->first && Max <= mapIt->second))
				throw "Depth bin '" + *it + "' overlaps with another bin!";
		}

		//add to map
		rangesOfDepthTables.insert(std::pair<int,int>(Min, Max));
	}

	//create bins tables and map
	numdepthTables = rangesOfDepthTables.size();
	depthTables = new TGenoComparisonTable[numdepthTables];
	fillDepthToBinMap();
	initialized = true;
};

void TDepthBins::createBins(TDepthBins & other){
	//copy bins from other
	other.copyBins(rangesOfDepthTables);

	//create bins tables and map
	numdepthTables = rangesOfDepthTables.size();
	depthTables = new TGenoComparisonTable[numdepthTables];
	fillDepthToBinMap();
	initialized = true;
}

void TDepthBins::copyBins(std::map<int, int> & rangeMap){
	for(mapIt=rangesOfDepthTables.begin(); mapIt!=rangesOfDepthTables.end(); ++mapIt)
		rangeMap.insert(std::pair<int,int>(mapIt->first, mapIt->second));
}

void TDepthBins::reportBins(TLog* logfile){
	for(mapIt=rangesOfDepthTables.begin(); mapIt!=rangesOfDepthTables.end(); ++mapIt){
		logfile->list("[" + toString(mapIt->first), "," + toString(mapIt->second) + "]");
	}
}

void TDepthBins::fillDepthToBinMap(){
	//find max depth
	maxDepthConsideredPlusOne = 0;
	for(mapIt=rangesOfDepthTables.begin(); mapIt != rangesOfDepthTables.end(); ++mapIt){
		if(mapIt->second > maxDepthConsideredPlusOne)
			maxDepthConsideredPlusOne = mapIt->second;
	}
	maxDepthConsideredPlusOne += 1;

	//create map
	depthToBinMap = new int[maxDepthConsideredPlusOne];
	int i;
	for(i=0; i<maxDepthConsideredPlusOne; ++i)
		depthToBinMap[i] = -1; //means no bin

	//now fill map
	int bin = 0;
	for(mapIt=rangesOfDepthTables.begin(); mapIt != rangesOfDepthTables.end(); ++mapIt, ++bin){
		for(i=mapIt->first; i<=mapIt->second; ++i)
			depthToBinMap[i] = bin;
	}
};

void TDepthBins::add(const int & depth, const int & trueGeno, const int & observedGeno){
	//find corresponding bin and add
	if(depth > 0 && depth < maxDepthConsideredPlusOne){
		if(depthToBinMap[depth] >= 0){
			depthTables[depthToBinMap[depth]].add(trueGeno, observedGeno);
		}
	}
};

void TDepthBins::write(const std::string & tag){
	int bin = 0;
	for(mapIt=rangesOfDepthTables.begin(); mapIt != rangesOfDepthTables.end(); ++mapIt, ++bin){
		depthTables[bin].write(tag + "_depth_" + toString(mapIt->first) + "_" + toString(mapIt->second) + "_genoComparisons.txt");
	}
};

void TDepthBins::estimateErrorRates(std::ofstream & out, std::string & className, TLog* logfile){
	//now estimate for each bin
	double eps, epsHom, epsHet;
	int bin = 0;
	for(mapIt=rangesOfDepthTables.begin(); mapIt != rangesOfDepthTables.end(); ++mapIt, ++bin){
		if(depthTables[bin].size() > 0){
			//estimate rates
			eps = depthTables[bin].estimateErrorRate();
			epsHom = depthTables[bin].estimateErrorRateHomozygousSites();
			epsHet = depthTables[bin].estimateErrorRateHeterozygousSites();

			//write to file
			out << className << "\t" << mapIt->first << "\t" << mapIt->second << "\t" << depthTables[bin].size();
			out << "\t" <<  eps;
			out << "\t" <<  epsHom;
			out << "\t" <<  epsHet;
			out << "\n";

			//print to screen
			logfile->list("Error rates (total, hom, het) for depth bin [" + toString(mapIt->first) + "," + toString(mapIt->second) + "]: " + toString(eps) + ", " + toString(epsHom) + ", " + toString(epsHet));
		}
		/*
		else {
			out << className << "\t" << mapIt->first << "\t" << mapIt->second << "\t0\t-\n";
		}
		*/
	}
};

void TDepthBins::printNewPLTable(std::ofstream & out, const std::string & className, const std::string* genotypeString){
	//now estimate for each bin
	int g;
	int bin = 0;
	for(mapIt=rangesOfDepthTables.begin(); mapIt != rangesOfDepthTables.end(); ++mapIt, ++bin){
		if(depthTables[bin].size() > 0){
			//write to file
			out << className << "\t" << mapIt->first << "\t" << mapIt->second << "\t" << depthTables[bin].size();
			for(g=0; g<3; ++g){
				out << "\t" << depthTables[bin].getNewPL(g);
			}
			out << "\n";
		}
		/*
		else {
			out << mapIt->first << "\t" << mapIt->second << "\t0\t-\t-\t-\n";
		}
		*/
	}
}


//---------------------------------------------------
//TSampleMap
//---------------------------------------------------
TSampleMap::TSampleMap(){
	initialized = false;
};

TSampleMap::TSampleMap(TParameters* myParameters, TVcfFileSingleLine & vcfFile, TLog* logfile){
	initialized = false;
	initialize(myParameters, vcfFile, logfile);
};

void TSampleMap::initialize(TParameters* myParameters, TVcfFileSingleLine & vcfFile, TLog* logfile){
	if(initialized)
		throw "Sample map already initialized!";
	readSampleMapFromFile(myParameters, vcfFile, logfile);
	readErrorClasses(myParameters, vcfFile, logfile);
	initialized = true;
}

void TSampleMap::readSampleMapFromFile(TParameters* myParameters, TVcfFileSingleLine & vcfFile, TLog* logfile){
	//file contains on each line the names of an "observed" sample and the sample with the corresponding true genotypes
	std::string filename = myParameters->getParameterString("samplePairs");
	logfile->startIndent("Reading sample pairs (observed -> true) from file '" + filename + "':");

	//open file
	std::ifstream in(filename.c_str());
	if(!in) throw "Failed to open file '" + filename + "' for reading!";

	//skip header
	std::string line;
	std::vector<std::string> tmpVec;
	getline(in, line);


	//now read each line and add samples to map
	int lineNum = 1;
	int observedSample, trueSample;
	while(in.good() && !in.eof()){
		//read line
		getline(in, line);
		trimString(line);
		++lineNum;

		//skip empty lines
		if(line.size() > 0){
			fillVectorFromStringWhiteSpaceSkipEmpty(line, tmpVec);
			if(tmpVec.size() != 2) throw "Unexpected number of entries on line " + toString(lineNum) + ": found " + toString(tmpVec.size()) + " instead of 2 columns!";

			//find samples in vcf file
			observedSample = vcfFile.sampleNumber(tmpVec[0]);
			if(observedSample < 0) throw "Sample '" + tmpVec[0] + "' not present in vcf file!";
			trueSample = vcfFile.sampleNumber(tmpVec[1]);
			if(trueSample < 0) throw "Sample '" + tmpVec[1] + "' not present in vcf file!";

			//add to map and report
			sampleMap.emplace_back(observedSample, trueSample, tmpVec[0], tmpVec[1]);
			logfile->list(tmpVec[0] + " -> " + tmpVec[1]);
		}
	}
	logfile->endIndent();
	logfile->conclude("Will use a total of " + toString(sampleMap.size()) + " comparisons.");
};

void TSampleMap::readErrorClasses(TParameters* myParameters, TVcfFileSingleLine & vcfFile, TLog* logfile){
	//read error class
	logfile->startIndent("Parsing sample sequencing batches:");
	std::string errorClassString = myParameters->getParameterString("batches", false);
	if(errorClassString == ""){
		//only one class for all samples
		logfile->list("Using a single batch 'all' for all samples.");
		for(it=sampleMap.begin(); it!=sampleMap.end(); ++it)
			it->errorRateClass = 0;
		errorClassNames.push_back("all");
	} else {
		//abuse sample groups to read / parse
		TSampleGroups errorClasses;

		if(errorClassString[0] == '['){
			//parse as string
			errorClasses.initialize(&vcfFile, errorClassString, logfile, false);
		} else {
			//parse as file
			int errorColumn = myParameters->getParameterIntWithDefault("batchesCol", 3) - 1;
			errorClasses.initialize(&vcfFile, errorClassString, errorColumn, logfile, false);
		}
		if(errorClasses.numGroups < 1) throw "No error classes defined!";

		//update names and class memberships in map	IN THE CORRECT ORDER
		errorClasses.fillVectorOfGroupNames(errorClassNames);
		for(it=sampleMap.begin(); it!=sampleMap.end(); ++it){
			if(!errorClasses.sampleVcfIndexInGroup(it->observedSampleVcfIndex))
					throw "Sample " + it->observedName + " has no batch assigned!";
			it->errorRateClass = errorClasses.getGroupOfSampleVcfIndex(it->observedSampleVcfIndex);
		}
	}

	logfile->endIndent();
}

void TSampleMap::begin(){
	it = sampleMap.begin();
}

void TSampleMap::next(){
	if(it!=sampleMap.end())
		++it;
}

bool TSampleMap::end(){
	return it == sampleMap.end();
}

//---------------------------------------------------
//TCompareToTruth
//---------------------------------------------------
TCompareToTruth::TCompareToTruth(TParameters & Params, TLog* Logfile){
	myParameters = &Params;
	logfile = Logfile;

	individualTables = NULL;
	depthBins = NULL;
	tablesInitialized = false;
}

//open input stream
void TCompareToTruth::prepareVcfInput(){
	std::string filename = myParameters->getParameterString("vcf");
	if(filename.find(".gz") == std::string::npos){
		logfile->list("Reading vcf from file '" + filename + "'.");
		vcfFile.openStream(filename, false);
	} else {
		logfile->list("Reading vcf from gzipped file '" + filename + "'.");
		vcfFile.openStream(filename, true);
	}

	//enable parsers
	vcfFile.enablePositionParsing();
	vcfFile.enableFormatParsing();
	vcfFile.enableSampleParsing();
	vcfFile.enableVariantParsing();
}

void TCompareToTruth::createComparisonTables(){
	//create comparison tables for each individual
	individualTables = new TGenoComparisonTable[sampleMap.size()];

	//create comparison tables for each error class and depth
	depthBins = new TDepthBins[sampleMap.numErrorClasses()];

	if(myParameters->parameterExists("depthBins")){ //bins
		logfile->startIndent("Will assemble genotype comparison tables for the following depth bins:");
		std::vector<std::string> tmpVec;
		myParameters->fillParameterIntoVector("depthBins", tmpVec, ',');
		depthBins[0].createBins(tmpVec);
		depthBins[0].reportBins(logfile);
		logfile->endIndent();
	} else { //up to maxDepth
		int maxDepth = myParameters->getParameterIntWithDefault("maxDepth", 50);
		logfile->list("Will assemble genotype comparison tables for each depth, but put depth >= " + toString(maxDepth) + " into a single bin.");
		depthBins[0].createBins(maxDepth);
	}

	//copy to other bins
	for(int c=1; c<sampleMap.numErrorClasses(); ++c)
				depthBins[c].createBins(depthBins[0]);

	tablesInitialized = true;
};

void TCompareToTruth::freeComparisonTables(){
	if(tablesInitialized){
		delete[] individualTables;
		delete[] depthBins;
		tablesInitialized = false;
	}
};

void TCompareToTruth::readDataFromVCF(){
	//apply depth filter to true genotypes?
	int trueMindepth = myParameters->getParameterIntWithDefault("trueMinDepth", 0) - 1 ;
	logfile->list("Will filter true calls to a minimum depth of " + toString(trueMindepth+1) + ".");

	//do we limit the lines to read?
	int limitLines = myParameters->getParameterIntWithDefault("limitLines", -1);
	if(limitLines > 0)
		logfile->list("Will limit analysis to the first " + toString(limitLines) + " lines of the VCF file.");

	//other variables
	int pair;
	long counter = 0;

	//now run through VCF file
	logfile->startIndent("Parsing VCF file:");
	short trueGeno;
	int obsVecfIndex, tueVcfIndex;
	while(vcfFile.next()){
		++counter;

		//skip sites with != 2 alleles
		if(vcfFile.getNumAlleles() == 2){
			//go over all sample pairs
			pair = 0;
			for(sampleMap.begin(); !sampleMap.end(); sampleMap.next(), ++pair){
				obsVecfIndex = sampleMap.cur().observedSampleVcfIndex;
				tueVcfIndex = sampleMap.cur().trueSampleVcfIndex;

				//do we filter true genotype on min depth?
				if(vcfFile.sampleDepth(tueVcfIndex) > trueMindepth)
					trueGeno = vcfFile.sampleGenotype(tueVcfIndex);
				else trueGeno = 3;

				//add to depth tables: add(depth, true, observed);
				depthBins[sampleMap.cur().errorRateClass].add(vcfFile.sampleDepth(obsVecfIndex), trueGeno, vcfFile.sampleGenotype(obsVecfIndex));

				//add to individual tables
				individualTables[pair].add(trueGeno, vcfFile.sampleGenotype(obsVecfIndex));


				//DEBUG
				//if(!vcfFile.sampleIsMissing(obsVecfIndex) && trueGeno==1 && vcfFile.sampleGenotype(obsVecfIndex) != 2){
				//	std::cout << "XXX\t" << vcfFile.sampleDepth(obsVecfIndex) << "\t" << vcfFile.sampleGenotypeQuality(obsVecfIndex) << "\t" << vcfFile.sampleGenotype(obsVecfIndex) << "\n";
				//}
			}
		}

		//show progress
		if(limitLines > 0 && counter > limitLines) break;
		if(counter % 10000 == 0)	logfile->list("Read " +toString(counter) + " lines.");
	}
	logfile->conclude("Parsed a total of " + toString(counter) + " positions.");
	logfile->endIndent();
}

void TCompareToTruth::estimateErrorRates(std::string filename){
	logfile->startIndent("Estimating error rates for each sequencing batch:");

	//open output file
	logfile->list("Writing error rates to file '" + filename + "'.");
	std::ofstream out(filename.c_str());
	if(!out) throw "Failed to open file '" + filename + "' for writing!";

	//write header
	out << "batch\tminDepth\tmaxDepth\tsize\terrorRate\terrorRateHom\terrorRateHet\n";

	for(int c=0; c<sampleMap.numErrorClasses(); ++c){
		logfile->startIndent("Batch '" + sampleMap.getErrorClassName(c) + "':");
		depthBins[c].estimateErrorRates(out, sampleMap.getErrorClassName(c), logfile);
		logfile->endIndent();
	}
	logfile->endIndent();

}

void TCompareToTruth::estimatePerIndividualErrorRates(std::string filename){
	//and now per individual
	logfile->startIndent("Estimating error rates for each individual:");

	//open output file
	logfile->list("Writing error rates to file '" + filename + "'.");
	std::ofstream out(filename.c_str());
	if(!out) throw "Failed to open file '" + filename + "' for writing!";

	//write header
	out << "observedSample\ttruthSample\terrorRate\terrorRateHom\terrorRateHet\n";

	//now estimate for each sample pair
	double eps, epsHom, epsHet;
	int pair = 0;
	for(sampleMap.begin(); !sampleMap.end(); sampleMap.next(), ++pair){
		eps = individualTables[pair].estimateErrorRate();
		epsHom = individualTables[pair].estimateErrorRateHomozygousSites();
		epsHet = individualTables[pair].estimateErrorRateHeterozygousSites();

		//write to file
		out << sampleMap.cur().observedName << "\t" << sampleMap.cur().trueName << "\t" << eps << "\t" << epsHom << "\t" << epsHet << "\n";

		//print to screen
		logfile->list("Error rates (total, hom, het) for '" + sampleMap.cur().observedName + " vs " + sampleMap.cur().trueName + "' = " +  toString(eps) + ", " + toString(epsHom) + ", " + toString(epsHet));
	}

	//close
	out.close();
	logfile->endIndent();
}

void TCompareToTruth::printComaprisonTables(std::string & outname){
	//write individual files
	logfile->listFlush("Writing comparison tables ...");
	int pair = 0;
	for(sampleMap.begin(); !sampleMap.end(); sampleMap.next(), ++pair)
		individualTables[pair].write(outname + sampleMap.cur().observedName + "_vs_" + sampleMap.cur().trueName + "_genoComparisons.txt");

	//write depth tables
	for(int c=0; c<sampleMap.numErrorClasses(); ++c)
		depthBins[c].write(outname + "_" + sampleMap.getErrorClassName(c));

	logfile->done();
}

void TCompareToTruth::printNewPLTables(std::string filename){
	logfile->listFlush("Writing new PL tables to file '" + filename + "' ...");

	//open output file
	std::ofstream out(filename.c_str());
	if(!out) throw "Failed to open file '" + filename + "' for writing!";

	//write header
	out << "Batch\tMinDepth\tMaxDepth\tSize\tPL_0/0\tPL_0/1\tPL_1/1\n";

	//genotype string
	std::string genoString[3];
	genoString[0] = "0/0";
	genoString[1] = "0/1";
	genoString[2] = "1/1";

	//loop over classes
	for(int c=0; c<sampleMap.numErrorClasses(); ++c){
		std::cout << "Printing for class " << c << " -> " << sampleMap.getErrorClassName(c) << std::endl;
		depthBins[c].printNewPLTable(out, sampleMap.getErrorClassName(c), genoString);
	}

	logfile->done();
}

void TCompareToTruth::compareCallsIndividuals(){
	logfile->startIndent("Comparing calls to truth set:");

	//open vcf file
	prepareVcfInput();

	//create sample map: observed -> truth
	sampleMap.initialize(myParameters, vcfFile, logfile);

	//output name
	std::string outname = myParameters->getParameterStringWithDefault("outname", "");
	if(outname == ""){
		outname = vcfFile.filename;
		outname = extractBefore(outname, ".vcf");
	}
	logfile->list("Writing files with tag '" + outname + "'.");

	//create comparison tables
	createComparisonTables();

	//read data
	readDataFromVCF();

	//estimate error rates
	estimateErrorRates(outname + "_errorRates.txt");

	//and now per individual
	estimatePerIndividualErrorRates(outname + "_errorRates_perIndividual.txt");

	//print tables
	if(myParameters->parameterExists("printTables"))
		printComaprisonTables(outname);

	//print file to update PLs
	if(myParameters->parameterExists("makePLTables")){
		printNewPLTables(outname + "_updatedPL.txt");
	}

    //clean up
	delete[] individualTables;
    logfile->endIndent();
}

//---------------------------------------------
// Assessing impact of errors in truth set
//---------------------------------------------
void TCompareToTruth::fillReportedTruthConfusionMatrix(double** matrix, int depth, double seqError, TRandomGenerator* randomGenerator){
	//fill 3x3 table [t][r] reflecting the frequency with which a true call t is reported as call r based on sequencing
	//fill probability of alternative read
	std::vector<double> probAlt = {seqError, 0.5, 1.0 - seqError};

	//prepare costly variables
	double logSeqError = log(seqError);
	double logOneMinusSeqError = log(1.0 - seqError);
	double logHalf = log(0.5);

	//identify MLE call for each number of alt alleles at this depth
	std::vector<int> MLEGenotype(depth + 1);
	for(int numAlt=0; numAlt<=depth; ++numAlt){
		int numRef = depth - numAlt;

		double maxLL = numRef * logOneMinusSeqError + numAlt * logSeqError;
		MLEGenotype[numAlt] = 0;

		//ref/alt
		double LL = depth * logHalf;
		std::cout << " " << LL;
		if(LL > maxLL){
			maxLL = LL;
			MLEGenotype[numAlt] = 1;
		}

		//alt/alt
		LL = numRef * logSeqError + numAlt * logOneMinusSeqError;
		if(LL > maxLL){
			maxLL = LL;
			MLEGenotype[numAlt] = 2;
		}
	}

	//now fill confusion matrix
	for(int t=0; t<3; ++t){ //loop over all true calls
		matrix[t][0] = 0.0; matrix[t][1] = 0.0; matrix[t][2] = 0.0;
		for(int numAlt=0; numAlt<=depth; ++numAlt){ //loop over all possible number of alternative alleles
			//add binomial frequency
			matrix[t][MLEGenotype[numAlt]] += randomGenerator->binomDensity(depth, numAlt, probAlt[t]);
		}
	}

	//print
	//std::cout << std::endl << "CONFUSION at depth = " << depth << " with seqError = " << seqError << ":" << std::endl;
	//for(int t=0; t<3; ++t){ //loop over all true calls
	//	std::cout << matrix[t][0] << "\t" << matrix[t][1] << "\t" << matrix[t][2] << std::endl;
	//}

};

void TCompareToTruth::assessErrorsInTruthSet(TRandomGenerator* randomGenerator){
	//read arguments
	int maxDepth = myParameters->getParameterIntWithDefault("maxDepth", 30);

	TLikelihoodTable* likelihoodTable;
	double error = myParameters->getParameterDouble("error");
	if(myParameters->parameterExists("errorHet")){
		double errorHet = myParameters->getParameterDouble("errorHet");
		logfile->list("Using genotyping error rates " + toString(error) + " and " + toString(errorHet) + " for homozygous and heterozygous genotypes, respectively.");
		likelihoodTable = new TLikelihoodTableHet(error, errorHet);
	} else {
		logfile->list("Using genotyping error rate " + toString(error) + " for both homozygous and heterozygous genotypes.");
		likelihoodTable = new TLikelihoodTable(error);
	}

	double het = myParameters->getParameterDoubleWithDefault("het", 0.1);
	logfile->list("Will simulate a fraction " + toString(het) + " of heterozygous sites.");
	double pTrueGeno[3]; pTrueGeno[0] = (1.0 - het) / 2.0; pTrueGeno[1] = het; pTrueGeno[2] = pTrueGeno[0];

	double seqError = myParameters->getParameterDoubleWithDefault("seqError", 0.01);
	logfile->list("Will assume 'true' genotypes were sequenced with a per base error rate of " + toString(seqError) + ".");

	//open output file
	std::string outname = myParameters->getParameterStringWithDefault("outname", "Tiger_");
	logfile->list("Writing files with tag '" + outname + "'.");
	TOutputFilePlain out(outname + "truthSetErrors.txt");
	out.writeHeader({"error", "Depth", "ErrorRate", "ErrorRateHom", "ErrorRateHet"});

	//some variables
	double* confusionMatrix[3];
	confusionMatrix[0] = new double[3];
	confusionMatrix[1] = new double[3];
	confusionMatrix[2] = new double[3];

	TGenoComparisonTable counts;

	//now run estimates
	logfile->listFlush("Assess up to a depth of " + toString(maxDepth) + " ...");
	for(int d=1; d<=maxDepth; ++d){
		out << error << d;

		//get confusion matrix of true genotype
		fillReportedTruthConfusionMatrix(confusionMatrix, d, seqError, randomGenerator);

		//add to counts
		counts.clear();
		for(int t=0; t<3; t++){ //loop over true genotypes
			for(int r=0; r<3; r++){ //loop over genotypes reported as true
				for(int g=0; g<3; g++){ //loop over observed genotypes
					counts.add(r, g, confusionMatrix[t][r] * likelihoodTable->getLikelihood(t, g) * pTrueGeno[t]);
				}
			}
		}

		//now estimate MLE of error rates
		out << counts.estimateErrorRate() << counts.estimateErrorRateHomozygousSites() << counts.estimateErrorRateHeterozygousSites() << std::endl;
	}
	logfile->done();
};




























